<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AboutUs extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();


        $this->data['language'] = $this->language;


    }

    public function index()
    {
              
        $this->data['view'] = 'frontend/aboutus';
        $this->load->view('frontend/layouts/default', $this->data);
    }
}