<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends Base_Controller
{
    public $data = array();
    public $plan_key;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Country_model');
        $this->load->model('State_model');
        $this->load->model('City_model');
        $this->load->model('User_model');
        $this->load->model('User_text_model');
        $this->load->model('Module_model');
        $this->load->model('Address_model');
        $this->load->model('Modules_users_rights_model');
        $this->load->model('Order_payment_details_model');
        $this->data['site_setting'] = $this->getSiteSetting();
        $this->data['language']      = $this->language;
        $this->load->library('stripe/stripe_payment', ['secret_key' => STRIPE_SECRET_KEY], 'stripe');
        $this->plan_key = 'plan_EBUKZRf9kGZkjP';

    }

    public function index()
    {
        if ($this->session->userdata('admin')) {
            redirect(base_url('cms/dashboard'));

        }

        redirect(base_url('cms/account/login'));
    }

    //User Login
    public function login()
    {
        if ($this->session->userdata('admin')) {
            redirect(base_url('cms/dashboard'));

        }
        $this->data['view'] = 'backend/login';
        $this->load->view('backend/login', $this->data);

    }

    //User Login
    public function forgotpassword($PasswordRegenrateKey)
    {
        $checkUser = $this->User_model->getWithMultipleFields(array('PasswordRegenrateKey'=>$PasswordRegenrateKey), true);

        if(!$checkUser)
        {
            $this->session->set_flashdata('message', 'Key expired. Please try again.');
            redirect(base_url());
        }
        else
        {
            $this->data['PasswordRegenrateKey'] = $PasswordRegenrateKey;
            $this->data['user'] = $checkUser;
            $this->data['view'] = 'frontend/forgotpassword';
            $this->load->view('frontend/layouts/default', $this->data);    
        }
        
    }


    public function checkLogin()
    {
        $data = array();
        $post_data = $this->input->post();
        $this->loginValidation();
        $checkUser = $this->checkUser($post_data);

        if ($checkUser != true) {
            $data = array();
            $data['success'] = 'false';
            $data['error'] = 'Email or password incorrect.';
            echo json_encode($data);
            exit();
        } else {
            $data = array();
            $data['success'] = 'Login Successfully';
            $data['error'] = false;
            $data['redirect'] = true;
            if($this->session->userdata['admin']['RoleID'] <= '2')
            {
                $data['url'] = 'cms/dashboard';
            }
            else
            {
                $data['url'] = 'cms/myorder';
            }
            echo json_encode($data);
            exit();
        }
    }

    public function sendforgotmail()
    {
        $data = array();
        $post_data = $this->input->post();
        $this->loginValidation();
        $checkUser = $this->checkUserEmail($post_data);

        if (!$checkUser) {
            $data = array();
            $data['success'] = 'false';
            $data['error'] = 'Email does not exist.';
            echo json_encode($data);
            exit();
        } else {

            $update_user_data = array();
            $update_by = array();

            $update_user_data['PasswordRegenrateKey'] = $PasswordRegenrateKey = md5($checkUser['UserID'].date('Ymdhis'));
            $update_by['UserID'] = $checkUser['UserID'];

            $this->User_model->update($update_user_data,$update_by);

            $this->data['PasswordRegenrateKey'] = $PasswordRegenrateKey;

            
            $message = $this->load->view('frontend/email_templates/forgot_email', $this->data, true); 

            $to = $post_data['Email'];

            $subject = 'Forgot Password';

            $headers = "From: " . strip_tags('info@leggingspro.com') . "\r\n";
            $headers .= "Reply-To: ". strip_tags('info@leggingspro.com') . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

            if(mail($to, $subject, $message, $headers))
            {
                $mailsent = true;
            }


            $data = array();
            $data['success'] = 'Please check your email for link to change password.';
            $data['error'] = false;
            echo json_encode($data);
            exit();
        }
    }
    

    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
            case 'save':
                $this->registrationValidation();
                $this->saveUser();
          break; 
          case 'save_customer':
                $this->customerRegistrationValidation();
                $this->saveCustomer();
          break; 
            case 'update':
                $this->validate();
                $this->update();
          break;
          case 'updatePassword':
                $this->passwordValidatiion();
                $this->updatePassword();
            break;
            case 'delete':
                $this->delete();
          break;      
                 
        }
    }

    private function loginValidation()
    {


        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Email', 'Email', 'required');
        if(isset($post_data['Password']))
        {
            $this->form_validation->set_rules('Password', 'Password', 'required');
        }

        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }

    }

    private function checkUser($post_data)
    {
        
        $post_data['Password'] = md5($post_data['Password']);
        
        $user = $this->User_model->getUserData($post_data, $this->language);

        if (!empty($user)) {

            /*if($user['RoleID'] <= '2')
            {
                $data = array();
                $data['success'] = 'false';
                $data['error'] = 'You can\'t be login from here';
                echo json_encode($data);
                exit();

            }*/
            /*$user = (array)$user;*/
			if($user['IsActive'] == 0)
            {
                $data = array();
                $data['success'] = 'false';
                $data['error'] = 'Your account is not activated yet please wait for admin to activate your account.';
                echo json_encode($data);
                exit();
            }
            $this->session->set_userdata('admin', $user);
            //$this->updateUserLoginStatus();
            return true;
        } else {
            return false;
        }

    }

    private function checkUserEmail($post_data)
    {
        
        $user = $this->User_model->getUserEmailData($post_data, $this->language);

        if (!empty($user)) {

            /*if($user['RoleID'] <= '2')
            {
                $data = array();
                $data['success'] = 'false';
                $data['error'] = 'You can\'t be login from here';
                echo json_encode($data);
                exit();

            }*/
            
            return $user;
        } else {
            return false;
        }

    }



    private function passwordValidatiion()
    {
        //print_rm($this->input->post());

        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
        $this->form_validation->set_rules('Password', lang('password'), 'required|min_length[8]|matches[PasswordConfirmation]');
        $this->form_validation->set_rules('PasswordConfirmation', lang('confirm_password'), 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else
        {
                return true;
        }

    }

    private function registrationValidation()
    {
        //print_rm($this->input->post());

        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
       // $this->form_validation->set_rules('RoleID', lang('role'), 'required');
        $this->form_validation->set_rules('FirstName', 'First Name', 'required');
        $this->form_validation->set_rules('LastName', 'Last Name', 'required');
        $this->form_validation->set_rules('Email', lang('email'), 'required|valid_email|is_unique[users.Email]|matches[EmailConfirmation]');
        $this->form_validation->set_rules('EmailConfirmation', lang('confirm_email'), 'required');
        $this->form_validation->set_rules('Password', lang('password'), 'required|min_length[8]|matches[PasswordConfirmation]');
        $this->form_validation->set_rules('PasswordConfirmation', lang('confirm_password'), 'required');
        $this->form_validation->set_rules('Contact', lang('company'), 'required');
        $this->form_validation->set_rules('BusinessType', lang('business_type'), 'required');

        $this->form_validation->set_rules('BillingAddress1', 'Billing Address 1', 'required');
        $this->form_validation->set_rules('BillingCountry', 'Billing Country' , 'required');
        $this->form_validation->set_rules('BillingState', 'Billing State' , 'required');
        $this->form_validation->set_rules('BillingCity', 'Billing City' , 'required');
        
        $this->form_validation->set_rules('ShippingAddress1', 'Shipping Address' , 'required');
        $this->form_validation->set_rules('ShippingCountry', 'Shipping Country' , 'required');
        $this->form_validation->set_rules('ShippingState', 'Shipping State' , 'required');
        $this->form_validation->set_rules('ShippingCity', 'Shipping City' , 'required');
            

        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else
        {
                return true;
        }

    }


    private function customerRegistrationValidation()
    {
        //print_rm($this->input->post());

        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
       // $this->form_validation->set_rules('RoleID', lang('role'), 'required');
        $this->form_validation->set_rules('FirstName', 'First Name', 'required');
        $this->form_validation->set_rules('LastName', 'Last Name', 'required');
        $this->form_validation->set_rules('Phone', 'Phone', 'required');
        $this->form_validation->set_rules('Email', lang('email'), 'required|valid_email|is_unique[users.Email]|matches[EmailConfirmation]');
        $this->form_validation->set_rules('EmailConfirmation', lang('confirm_email'), 'required');
        $this->form_validation->set_rules('Password', lang('password'), 'required|min_length[8]|matches[PasswordConfirmation]');
        $this->form_validation->set_rules('PasswordConfirmation', lang('confirm_password'), 'required');
        
       

        $this->form_validation->set_rules('BillingAddress1', 'Billing Address 1', 'required');
        $this->form_validation->set_rules('BillingCountry', 'Billing Country' , 'required');
        $this->form_validation->set_rules('BillingState', 'Billing State' , 'required');
        $this->form_validation->set_rules('BillingCity', 'Billing City' , 'required');
        
        $this->form_validation->set_rules('ShippingAddress1', 'Shipping Address' , 'required');
        $this->form_validation->set_rules('ShippingCountry', 'Shipping Country' , 'required');
        $this->form_validation->set_rules('ShippingState', 'Shipping State' , 'required');
        $this->form_validation->set_rules('ShippingCity', 'Shipping City' , 'required');
            

        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else
        {
                return true;
        }

    }

    public function register(){

        
        if($this->session->userdata('admin')){
            redirect(base_url());
        }

        $this->data['countries'] = $this->Country_model->getAllJoinedData(false,'CountryID',$this->language,false,'ASC','countries_text.Title',true);
        $charge = $this->stripe->getPlan($this->plan_key);//plan id get from client strip dashboard
        if(isset($charge->amount)){
            $this->data['PlanAmount'] = $charge->amount;
        }else{
            $this->data['PlanAmount'] = 4000;
        }
        $this->data['view'] = 'frontend/registration';
        $this->load->view('frontend/layouts/default', $this->data);

    }


     public function customer_register(){

        
        if($this->session->userdata('admin')){
            redirect(base_url());
        }

        $this->data['countries'] = $this->Country_model->getAllJoinedData(false,'CountryID',$this->language,false,'ASC','countries_text.Title',true);
        
        $this->data['view'] = 'frontend/customer_registration';
        $this->load->view('frontend/layouts/default', $this->data);

    }

    public function saveCustomer(){

        $post_data                          = $this->input->post();
        //$post_data['RoleID']                = 2;
        $parent                             = 'User_model';
        $child                              = 'User_text_model';
        $save_parent_data                   = array();
        $save_child_data                    = array();
       
        $getSortValue = $this->$parent->getLastRow('UserID');
           
        $sort = 0;
        if(!empty($getSortValue))
        {
           $sort = $getSortValue['SortOrder'] + 1;
        }
       
       
        $save_parent_data['SortOrder']      = $sort;
        $save_parent_data['IsActive']       = 1;
        $save_parent_data['RoleID']         = $post_data['RoleID'];

        $save_parent_data['Password']       = md5($post_data['Password']);
        $save_parent_data['Email']          = $post_data['Email'];
        
        $save_parent_data['Phone']          = $post_data['Phone'];
        
       
        if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
            $save_parent_data['Image'] = $this->uploadImage('Image', 'uploads/images/');
        }


        $save_parent_data['CreatedAt']      = $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');      
       // $save_parent_data['CreatedBy']      = $save_parent_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];
        
         
        

        $insert_id                          = $this->$parent->save($save_parent_data);
        if($insert_id > 0)
        {
            
            $default_lang = getDefaultLanguage();
            
            
            
            $save_child_data['FullName']                    = $post_data['FirstName'].' '.$post_data['LastName'];
            $save_child_data['UserID']                      = $insert_id;
            $save_child_data['SystemLanguageID']            = $default_lang->SystemLanguageID;
            
            $this->$child->save($save_child_data);




            


            $modules = $this->Module_model->getAll();
            foreach($modules as $key => $value)
            {
               
                $other_data[] = [
                        'ModuleID'  => $value->ModuleID,
                        'UserID'    => $insert_id,
                        'CanView'   => (checkRightAccess($value->ModuleID, $post_data['RoleID'], 'CanView') ? 1 : 0),
                        'CanAdd'    => (checkRightAccess($value->ModuleID, $post_data['RoleID'], 'CanAdd') ? 1 : 0),
                        'CanEdit'   => (checkRightAccess($value->ModuleID, $post_data['RoleID'], 'CanEdit') ? 1 : 0),
                        'CanDelete' => (checkRightAccess($value->ModuleID, $post_data['RoleID'], 'CanDelete') ? 1 : 0),
                        'CreatedAt' => date('Y-m-d H:i:s'),
                        'CreatedBy' => $insert_id,
                        'UpdatedAt' => date('Y-m-d H:i:s'),
                        'UpdatedBy' => $insert_id
                ];



            }
            
            
           
            $this->Modules_users_rights_model->insert_batch($other_data);

            // add address 

            $save_address_array = array();
            $save_address_array['UserID']       = $insert_id;
            $save_address_array['Address1']     = $post_data['BillingAddress1'];
            $save_address_array['Address2']     = $post_data['BillingAddress2'];
            $save_address_array['CountryID']    = $post_data['BillingCountry']; 
            $save_address_array['StateID']      = $post_data['BillingState']; 
            $save_address_array['CityID']       = $post_data['BillingCity']; 
            $save_address_array['ZipCode']       = $post_data['BillingZipCode'];
            $save_address_array['Type']         = 'billing';

            $this->Address_model->save($save_address_array);

            $save_address_array['Address1']     = $post_data['ShippingAddress1'];
            $save_address_array['Address2']     = $post_data['ShippingAddress2'];
            $save_address_array['CountryID']    = $post_data['ShippingCountry']; 
            $save_address_array['StateID']      = $post_data['ShippingState']; 
            $save_address_array['CityID']       = $post_data['ShippingCity']; 
            $save_address_array['ZipCode']       = $post_data['ShippingZipCode'];
            $save_address_array['Type']         = 'shipping';

            $this->Address_model->save($save_address_array);


            // end email to admin

            $get_shipping_country = $this->Country_model->getJoinedData(true,'CountryID','countries.CountryID = '.$post_data['ShippingCountry'].'');
            $get_shipping_state = $this->State_model->getJoinedData(true,'StateID','states.StateID = '.$post_data['ShippingState'].'');
            $get_shipping_city = $this->City_model->getJoinedData(true,'CityID','cities.CityID = '.$post_data['ShippingCity'].'');



            $get_billing_country = $this->Country_model->getJoinedData(true,'CountryID','countries.CountryID = '.$post_data['BillingCountry'].'');
            $get_billing_state = $this->State_model->getJoinedData(true,'StateID','states.StateID = '.$post_data['BillingState'].'');
            $get_billing_city = $this->City_model->getJoinedData(true,'CityID','cities.CityID = '.$post_data['BillingCity'].'');


            
            $message_admin = '<tr>
                <td colspan="3" style="text-align: center;text-align: center;
                  background: #923ab7;
                  color: #fff;
                  font-weight: bold;
                  font-size: 18px">New Registration</td>
                </tr>
                <tr><td colspan="3"><img src="'.base_url("assets/frontend/email_images").'/user.jpg"></td></tr>';
            $message_customer = '<tr>
                <td colspan="3" style="text-align: center;text-align: center;
                  background: #923ab7;
                  color: #fff;
                  font-weight: bold;
                  font-size: 18px">Welcome</td>
                </tr>
                <tr><td colspan="3"><img src="'.base_url("assets/frontend/email_images").'/customer.jpg"></td></tr><tr><td colspan="3"><p>Yor are wellcome to leggingspro.Here are your details.</p></td></tr>';    
            $message ='<tr>
                <td colspan="3" style="text-align: center;     text-align: center;
                  background: #923ab7;
                  color: #fff;
                  font-weight: bold;
                  font-size: 18px">Personal Info</td>
                </tr>    
            <tr><td height="15" colspan="3"></td></tr>
            <tr style="background: #f1f1f1;"><td>&nbsp; Name:</td> <td colspan="2">'.$post_data['FirstName'].' '.$post_data['LastName'].'</td></tr>
            <tr><td>&nbsp; Phone:</td> <td colspan="2">'.$post_data['Phone'].'</td></tr>
            <tr style="background: #f1f1f1;"><td>&nbsp; Email:</td> <td colspan="2">'.$post_data['Email'].'</td></tr>
           
            <tr>
                <td colspan="3" style="text-align: center;     text-align: center;
                  background: #923ab7;
                  color: #fff;
                  font-weight: bold;
                  font-size: 18px">Billing Address</td>
                </tr>
            <tr><td height="15" colspan="3"></td></tr>
            <tr style="background: #f1f1f1;"><td>&nbsp; Address Line 1:</td> <td colspan="2">'.$post_data['BillingAddress1'].'</td></tr>
            <tr><td>&nbsp; Address Line 2:</td> <td colspan="2">'.$post_data['BillingAddress2'].'</td></tr>
            <tr style="background: #f1f1f1;"><td>&nbsp; Country:</td> <td colspan="2">'.$get_billing_country[0]['Title'].'</td></tr>
            <tr><td>&nbsp; State:</td> <td colspan="2">'.$get_billing_state[0]['Title'].'</td></tr>
            <tr style="background: #f1f1f1;"><td>&nbsp; City:</td> <td colspan="2">'.$get_billing_city[0]['Title'].'</td></tr>
             <tr>
                <td colspan="3" style="text-align: center;     text-align: center;
                  background: #923ab7;
                  color: #fff;
                  font-weight: bold;
                  font-size: 18px">Shipping Address</td>
                </tr>
            <tr><td height="15" colspan="3"></td></tr>
            <tr style="background: #f1f1f1;"><td>&nbsp; Address Line 1:</td> <td colspan="2">'.$post_data['ShippingAddress1'].'</td></tr>
            <tr><td>&nbsp; Address Line 2:</td> <td colspan="2">'.$post_data['ShippingAddress2'].'</td></tr>
            <tr style="background: #f1f1f1;"><td>&nbsp; Country:</td> <td colspan="2">'.$get_shipping_country[0]['Title'].'</td></tr>
            <tr><td>&nbsp; State:</td> <td colspan="2">'.$get_shipping_state[0]['Title'].'</td></tr>
            <tr style="background: #f1f1f1;"><td>&nbsp; City:</td> <td colspan="2">'.$get_shipping_city[0]['Title'].'</td></tr>';







            $email_data = array();
            $email_data['from']     = 'no_reply@leggingspro.com';
            $email_data['to']       = $this->data['site_setting']->Email;
            $email_data['subject']  = 'New Customer Register';



            $email_data['body']  = emailTemplate($message_admin.$message);
            sendEmail($email_data);

            $email_data = array();
            $email_data['from']     = 'no_reply@leggingspro.com';
            $email_data['to']       = $post_data['Email'];
            $email_data['subject']  = 'Welcome';



            $email_data['body']  = emailTemplate($message_customer.$message);

            sendEmail($email_data);

            // end email
            
            
            $success['error']   = false;
            $success['success'] = 'Your account created successfully.';
            $success['redirect'] = true;
            $success['url'] = '';
            echo json_encode($success);
            exit;


        }else
        {
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }





    public function saveUser(){

        $post_data                          = $this->input->post();
        //$post_data['RoleID']                = 2;
        $check_if_payment_exist = $this->Order_payment_details_model->getWithMultipleFields(array('CustomerID' => $post_data['CustomerID'],'ChargeID' =>$post_data['ChargeID']));

        if(!$check_if_payment_exist){
            $errors['error'] =  'Please pay before become customer for discounted prices';
            $errors['success'] = false;
            echo json_encode($errors);
            exit;

        }

        $parent                             = 'User_model';
        $child                              = 'User_text_model';
        $save_parent_data                   = array();
        $save_child_data                    = array();
       
        $getSortValue = $this->$parent->getLastRow('UserID');
           
        $sort = 0;
        if(!empty($getSortValue))
        {
           $sort = $getSortValue['SortOrder'] + 1;
        }
       
       
        $save_parent_data['SortOrder']      = $sort;
        $save_parent_data['IsActive']       = (isset($post_data['IsActive']) ? 1 : 0 );
        $save_parent_data['RoleID']         = $post_data['RoleID'];

        $save_parent_data['Password']       = md5($post_data['Password']);
        $save_parent_data['Email']          = $post_data['Email'];
        $save_parent_data['Contact']        = $post_data['Contact'];
        $save_parent_data['Company']        = $post_data['Company'];
        $save_parent_data['CompanyWebsite'] = $post_data['CompanyWebsite'];
        $save_parent_data['Phone']          = $post_data['Phone'];
        $save_parent_data['Fax']            = $post_data['Fax'];
        $save_parent_data['BusinessType']   = $post_data['BusinessType'];
        $save_parent_data['PreferredSale']  = $post_data['PreferredSale'];
        $save_parent_data['ResalePeritNumber']   = $post_data['ResalePeritNumber'];
       
        if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
            $save_parent_data['Image'] = $this->uploadImage('Image', 'uploads/images/');
        }



        $save_parent_data['CreatedAt']      = $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');      
       // $save_parent_data['CreatedBy']      = $save_parent_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];
        
         
        

        $insert_id                          = $this->$parent->save($save_parent_data);
        if($insert_id > 0)
        {
            
            $default_lang = getDefaultLanguage();
            
            
            
            $save_child_data['FullName']                        = $post_data['FirstName'].' '.$post_data['LastName'];
            $save_child_data['UserID']                      = $insert_id;
            $save_child_data['SystemLanguageID']             = $default_lang->SystemLanguageID;
            //$save_child_data['CreatedAt']                    = $save_child_data['UpdatedAt']    = date('Y-m-d H:i:s');
            //$save_child_data['CreatedBy']                    = $save_child_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];
            $this->$child->save($save_child_data);


            $modules = $this->Module_model->getAll();
            foreach($modules as $key => $value)
            {
               
                $other_data[] = [
                        'ModuleID'  => $value->ModuleID,
                        'UserID'    => $insert_id,
                        'CanView'   => (checkRightAccess($value->ModuleID, $post_data['RoleID'], 'CanView') ? 1 : 0),
                        'CanAdd'    => (checkRightAccess($value->ModuleID, $post_data['RoleID'], 'CanAdd') ? 1 : 0),
                        'CanEdit'   => (checkRightAccess($value->ModuleID, $post_data['RoleID'], 'CanEdit') ? 1 : 0),
                        'CanDelete' => (checkRightAccess($value->ModuleID, $post_data['RoleID'], 'CanDelete') ? 1 : 0),
                        'CreatedAt' => date('Y-m-d H:i:s'),
                        'CreatedBy' => $insert_id,
                        'UpdatedAt' => date('Y-m-d H:i:s'),
                        'UpdatedBy' => $insert_id
                ];



            }
            
            
           
            $this->Modules_users_rights_model->insert_batch($other_data);

            // add address 

            $save_address_array = array();
            $save_address_array['UserID']       = $insert_id;
            $save_address_array['Address1']     = $post_data['BillingAddress1'];
            $save_address_array['Address2']     = $post_data['BillingAddress2'];
            $save_address_array['CountryID']    = $post_data['BillingCountry']; 
            $save_address_array['StateID']      = $post_data['BillingState']; 
            $save_address_array['CityID']       = $post_data['BillingCity']; 
            $save_address_array['ZipCode']       = $post_data['BillingZipCode'];
            $save_address_array['Type']         = 'billing';

            $this->Address_model->save($save_address_array);

            $save_address_array['Address1']     = $post_data['ShippingAddress1'];
            $save_address_array['Address2']     = $post_data['ShippingAddress2'];
            $save_address_array['CountryID']    = $post_data['ShippingCountry']; 
            $save_address_array['StateID']      = $post_data['ShippingState']; 
            $save_address_array['CityID']       = $post_data['ShippingCity']; 
            $save_address_array['ZipCode']       = $post_data['ShippingZipCode'];
            $save_address_array['Type']         = 'shipping';

            $this->Address_model->save($save_address_array);


             // end email to admin

            $get_shipping_country = $this->Country_model->getJoinedData(true,'CountryID','countries.CountryID = '.$post_data['ShippingCountry'].'');
            $get_shipping_state = $this->State_model->getJoinedData(true,'StateID','states.StateID = '.$post_data['ShippingState'].'');
            $get_shipping_city = $this->City_model->getJoinedData(true,'CityID','cities.CityID = '.$post_data['ShippingCity'].'');



            $get_billing_country = $this->Country_model->getJoinedData(true,'CountryID','countries.CountryID = '.$post_data['BillingCountry'].'');
            $get_billing_state = $this->State_model->getJoinedData(true,'StateID','states.StateID = '.$post_data['BillingState'].'');
            $get_billing_city = $this->City_model->getJoinedData(true,'CityID','cities.CityID = '.$post_data['BillingCity'].'');

            $business_types = array('N/A','Retail Store','Department/Chain Store','Wholesaler');
            $prefer_sales = array('N/A' => 'N/A','BLEE' => 'BERNADETTE LEE(CHINESE)','ED' => 'STAR DE LOZA(SPANISH/ENGLISH)','WEB' => 'ONLINE WEB','YELE' => 'YELETE','SL' => 'SEAN LIN','ANDY' => 'ANDY KIM(KOREAN, ENGLISH)','CECI' => 'CECILIA (SPANISH/ENGLISH)','LEA' => 'LEA BYUN(KOREAN, SPANISH)','JUL'=> 'JULIA ZHU(ENGLISH/MANDARIN)','RG'=>'REYNA GARCIA','NY' =>'NY OFFICE','ELI'=> 'ELIANA ANDRADE','LP' => 'LORENA PINEDO(SPANISH/ENGLISH)');

            $message_admin = '<tr>
                <td colspan="3" style="text-align: center;     text-align: center;
                  background: #923ab7;
                  color: #fff;
                  font-weight: bold;
                  font-size: 18px">New Registration</td>
                </tr>
                <tr><td colspan="3"><img src="'.base_url("assets/frontend/email_images").'/user.jpg"></td></tr>';

            $message_wholesaler = '<tr>
                <td colspan="3" style="text-align: center;     text-align: center;
                  background: #923ab7;
                  color: #fff;
                  font-weight: bold;
                  font-size: 18px">New Registration</td>
                </tr>
                <tr><td colspan="3"><img src="'.base_url("assets/frontend/email_images").'/customer.jpg"></td></tr><tr><td colspan="3"><p>Yor are wellcome to leggingspro.Please wait for approvel of your account.Here are your complete details</p></td></tr>';    

           $message = ' <tr>
                <td colspan="3" style="text-align: center;     text-align: center;
                  background: #923ab7;
                  color: #fff;
                  font-weight: bold;
                  font-size: 18px">Personal Info</td>
                </tr>   <tr><td height="15" colspan="3"></td></tr>
            <tr style="background: #f1f1f1;"><td>&nbsp; Full Name:</td> <td colspan="2">'.$post_data['FirstName'].' '.$post_data['LastName'].'</td></tr>
            <tr style="background: #f1f1f1;"><td>&nbsp; Contact:</td> <td colspan="2">'.$post_data['Contact'].'</td></tr>
            <tr><td>&nbsp; Company:</td> <td colspan="2">'.$post_data['Company'].'</td></tr>
            <tr style="background: #f1f1f1;"><td>&nbsp; Company Website:</td> <td colspan="2">'.$post_data['CompanyWebsite'].'</td></tr>

            <tr><td height="15" colspan="3"></td></tr>
            <tr style="background: #f1f1f1;"><td>&nbsp; Phone:</td> <td colspan="2">'.$post_data['Phone'].'</td></tr>
            <tr><td>&nbsp; Fax:</td> <td colspan="2">'.$post_data['Fax'].'</td></tr>
            <tr style="background: #f1f1f1;"><td>&nbsp; Business Type:</td> <td colspan="2">'.$business_types[$post_data['BusinessType']].'</td></tr>
            
            <tr><td height="15" colspan="3"></td></tr>
            <tr style="background: #f1f1f1;"><td>&nbsp; Preferred Sales:</td> <td colspan="2">'.$prefer_sales[$post_data['PreferredSale']].'</td></tr>
            <tr><td>&nbsp; Email:</td> <td colspan="2">'.$post_data['Email'].'</td></tr>
            <tr style="background: #f1f1f1;"><td>&nbsp; Resale Permit Number :</td> <td colspan="2">'.$post_data['ResalePeritNumber'].'</td></tr>

            <tr>
                <td colspan="3" style="text-align: center;     text-align: center;
                  background: #923ab7;
                  color: #fff;
                  font-weight: bold;
                  font-size: 18px">Billing Address</td>
                </tr>
            <tr><td height="15" colspan="3"></td></tr>
            <tr style="background: #f1f1f1;"><td>&nbsp; Address Line 1:</td> <td colspan="2">'.$post_data['BillingAddress1'].'</td></tr>
            <tr><td>&nbsp; Address Line 2:</td> <td colspan="2">'.$post_data['BillingAddress2'].'</td></tr>
            <tr style="background: #f1f1f1;"><td>&nbsp; Country:</td> <td colspan="2">'.$get_billing_country[0]['Title'].'</td></tr>
            <tr><td>&nbsp; State:</td> <td colspan="2">'.$get_billing_state[0]['Title'].'</td></tr>
            <tr style="background: #f1f1f1;"><td>&nbsp; City:</td> <td colspan="2">'.$get_billing_city[0]['Title'].'</td></tr>
             <tr>
                <td colspan="3" style="text-align: center;     text-align: center;
                  background: #923ab7;
                  color: #fff;
                  font-weight: bold;
                  font-size: 18px">Shipping Address</td>
                </tr>
            <tr><td height="15" colspan="3"></td></tr>
            <tr style="background: #f1f1f1;"><td>&nbsp; Address Line 1:</td> <td colspan="2">'.$post_data['ShippingAddress1'].'</td></tr>
            <tr><td>&nbsp; Address Line 2:</td> <td colspan="2">'.$post_data['ShippingAddress2'].'</td></tr>
            <tr style="background: #f1f1f1;"><td>&nbsp; Country:</td> <td colspan="2">'.$get_shipping_country[0]['Title'].'</td></tr>
            <tr><td>&nbsp; State:</td> <td colspan="2">'.$get_shipping_state[0]['Title'].'</td></tr>
            <tr style="background: #f1f1f1;"><td>&nbsp; City:</td> <td colspan="2">'.$get_shipping_city[0]['Title'].'</td></tr>';







            $email_data = array();
            $email_data['from']     = 'no_reply@leggingspro.com';
            $email_data['to']       = $this->data['site_setting']->Email;
            $email_data['subject']  = 'New Wholesaler Register';



            $email_data['body']  = emailTemplate($message_admin.$message);

            sendEmail($email_data);


            $email_data = array();
            $email_data['from']     = 'no_reply@leggingspro.com';
            $email_data['to']       = $post_data['Email'];
            $email_data['subject']  = 'Welcome';



            $email_data['body']  = emailTemplate($message_wholesaler.$message);

            sendEmail($email_data);

            // end email




            
            $success['error']   = false;
            $success['success'] = 'Your account created successfully please wait for admin to approve your account.';
            $success['redirect'] = true;
            $success['url'] = '';
            echo json_encode($success);
            exit;


        }else
        {
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }

    public function updatePassword()
    {

        $post_data                          = $this->input->post();
        $parent                             = 'User_model';
        $child                              = 'User_text_model';

        $update_user_data = array();
        $update_by = array();

        $update_user_data['Password'] = md5($post_data['Password']);
        $update_user_data['PasswordRegenrateKey'] = '';

        $update_by['UserID'] = $post_data['UserID'];
        $update_by['PasswordRegenrateKey'] = $post_data['PasswordRegenrateKey'];

        $this->$parent->update($update_user_data,$update_by);

        $data = array();
        $data['success'] = 'Password updated Successfully.';
        $data['error'] = false;
        $data['redirect'] = true;
        $data['url'] = '';
        echo json_encode($data);
        exit();

    }



    

    //Logout
    public function logout()
    {
        $data = array();
        $arr_update = array();
        $user = $this->session->userdata('admin');
        $this->session->unset_userdata('admin');
        redirect($this->config->item('base_url'));

    }


    public function stripe_single_payment_for_subscription()
    {
        $post_data = $this->input->post();

        $post_data['token'] = json_decode($post_data['token']);

        if($this->session->userdata('admin'))
        {
            $user_id = $this->session->userdata['admin']['UserID'];    
        }
        else
        {
            $user_id = get_cookie('temp_order_key');
        }


        

        $charge = $this->stripe->getPlan($this->plan_key);//plan id get from client strip dashboard
        if(isset($charge->amount)){
           $amount = $charge->amount;
        }else{
            $amount = 4000;
        }

       
        $customer = $this->stripe->create_customer_by_token($post_data['token']);

        if(!method_exists ($customer, 'getMessage')) {

            $charge = $this->stripe->subscription($customer->id);  // Stripe Payment
            //print_r($charge);
            //$charge = $this->stripe->single_payment($customer->id, 100, getCurrencyCode($this->data['CurrencyID']));  // will change later  to above function
            
            
            if(!method_exists ($charge, 'getMessage')) {
                
                if($charge->customer == $customer->id) {
                    
                    $save_data = [
                        'UserID'             => 0,
                        'CustomerID'         => $customer->id,
                        'ChargeID'           => $charge->id,
                        'TransactionDate'    => date('Y-m-d H:i:s'),
                        'Amount'             => $amount,
                        'PaymentGateway'     => 'stripe'
                    ];
                    $insert_result = $this->Order_payment_details_model->save($save_data);
                    if($insert_result)
                    {
                        $message = 'Payment Successful';
                    }
                    else
                    {
                        $message = 'Some error occurred while saving in DB.';
                    }
                    
                    
                } else {
                    $message = 'Some error occurred while charging.';
                }   // if($charge->paid == true)
                
            } else {
                $message = $customer->getMessage();
            }   // if(!method_exists ($charge, 'getMessage'))
            
        } else {
            $message = $customer->getMessage();
        }   // if(!method_exists ($customer, 'getMessage'))

        $this->session->set_flashdata(['tab_index' => 'surveys', 'message' => $message]);
        // redirect('customer_area');
        $response = array();

        $response['message'] = $message;
        $response['CustomerID'] = $customer->id;
        $response['ChargeID'] = $charge->id;

        echo json_encode($response);
    }



    
}