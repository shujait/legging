<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();

        $this->load->Model([
            ucfirst($this->router->fetch_class()) . '_model',
            ucfirst($this->router->fetch_class()) . '_text_model'
        ]);


        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['Parent_model'] = ucfirst($this->router->fetch_class()) . '_model';
        $this->data['Child_model'] = ucfirst($this->router->fetch_class()) . '_text_model';
        $this->data['TableKey'] = 'CategoryID';
        $this->data['Table'] = 'categories';


    }

    public function detail($ParentID = '')
    {
        $parent                             = $this->data['Parent_model'];
        
        $where = 'categories.ParentID = '.$ParentID;
        
        $categories = $this->$parent->getAllJoinedData(false, $this->data['TableKey'], $this->language,$where);

        $this->data['categoryDet'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $ParentID, 'DESC', '');

        if(!empty($categories))
        {
            $this->data['categories'] = $categories;

            $this->data['view'] = 'frontend/category';
            $this->load->view('frontend/layouts/default', $this->data);
        }
        else
        {
            redirect(base_url('products/index/'.$ParentID));
        }
    }


}