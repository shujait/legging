<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'libraries/paypal-php-sdk/paypal/rest-api-sdk-php/sample/bootstrap.php'); // require paypal files


use PayPal\Api\ItemList;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Amount;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RefundRequest;
use PayPal\Api\Sale;

class Checkout extends Base_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Order_model');
        $this->load->model('User_model');
        $this->load->model('User_text_model');
        $this->load->model('Country_model');
        $this->load->model('State_model');
        $this->load->model('City_model');
        $this->load->model('Address_model');
        $this->load->model('Product_text_model');
        $this->load->model('Order_payment_details_model');

        $this->load->library('stripe/stripe_payment', ['secret_key' => STRIPE_SECRET_KEY], 'stripe');

        $this->config->load('paypal');

        $this->_api_context = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                $this->config->item('client_id'), $this->config->item('secret')
            )
        );

        
    }
    
    
    public function index(){
         $this->order_detail();
     }
    public function order_detail($step = 1){

                     
        $this->data['countries'] = $this->Country_model->getAllJoinedData(false,'CountryID',$this->language);

        if (isset($this->session->userdata['admin']['UserID'])) {

            $user_id = $this->session->userdata['admin']['UserID'];

            $this->data['userdetail'] = $this->User_model->getJoinedData(false,'UserID','users.UserID='.$user_id,'DESC','');

            $this->data['billing_details'] = $this->Address_model->getWithMultipleFields(array('UserID'=>$user_id,'Type'=>'billing'));

            $this->data['shipping_details'] = $this->Address_model->getWithMultipleFields(array('UserID'=>$user_id,'Type'=>'shipping'));
               
            if($this->data['billing_details'])
            {
                $this->data['billing_states'] = $this->State_model->getJoinedData(false,'StateID','states.CountryID='.$this->data['billing_details']->CountryID,'DESC','');

                
                $this->data['billing_cities'] = $this->City_model->getJoinedData(false,'CityID','cities.StateID='.$this->data['billing_details']->StateID,'DESC','');
            }


            if($this->data['shipping_details'])
            {
                $this->data['shipping_states'] = $this->State_model->getJoinedData(false,'StateID','states.CountryID='.$this->data['shipping_details']->CountryID,'DESC','');

                $this->data['shipping_cities'] = $this->City_model->getJoinedData(false,'CityID','cities.StateID='.$this->data['shipping_details']->StateID,'DESC','');
            }

        } else {
            if (!get_cookie('temp_order_key')) {
                $user_id = 0;
                
            } else {
                $user_id = get_cookie('temp_order_key');

                $userdetail = $this->User_model->getJoinedData(false,'UserID','users.TemporaryUserID='.$user_id,'DESC','');

                if(!empty($userdetail))
                {
                    $this->data['userdetail'] = $userdetail;
                    $temp_user_id = $userdetail[0]->UserID;
                    $this->data['billing_details'] = $this->Address_model->getWithMultipleFields(array('UserID'=>$temp_user_id,'Type'=>'billing'));

                    $this->data['shipping_details'] = $this->Address_model->getWithMultipleFields(array('UserID'=>$temp_user_id,'Type'=>'shipping'));
                       
                    if($this->data['billing_details'])
                    {
                        $this->data['billing_states'] = $this->State_model->getJoinedData(false,'StateID','states.CountryID='.$this->data['billing_details']->CountryID,'DESC','');

                        
                        $this->data['billing_cities'] = $this->City_model->getJoinedData(false,'CityID','cities.StateID='.$this->data['billing_details']->StateID,'DESC','');
                    }


                    if($this->data['shipping_details'])
                    {
                        $this->data['shipping_states'] = $this->State_model->getJoinedData(false,'StateID','states.CountryID='.$this->data['shipping_details']->CountryID,'DESC','');

                        $this->data['shipping_cities'] = $this->City_model->getJoinedData(false,'CityID','cities.StateID='.$this->data['shipping_details']->StateID,'DESC','');
                    }
                }
            }
        }
        if($user_id > 0)
        {
            $productCount = getTotalProduct($user_id)[0]->products_count;

            $payment_id = $this->input->get("paymentId") ;
            $PayerID = $this->input->get("PayerID") ;
            $token = $this->input->get("token") ;
            /** clear the session payment ID **/

           /* if (empty($PayerID) || empty($token)) {
                $this->session->set_flashdata('message',"<div class='alert alert-danger'>Payment failed</div>");
                redirect('checkout');
            }*/
            if (!empty($PayerID) || !empty($token)) {
                $payment = Payment::get($payment_id,$this->_api_context);


                /** PaymentExecution object includes information necessary **/
                /** to execute a PayPal account payment. **/
                /** The payer_id is added to the request query parameters **/
                /** when the user is redirected from paypal back to your site **/
                $execution = new PaymentExecution();
                $execution->setPayerId($this->input->get('PayerID'));

                /**Execute the payment **/
                $result = $payment->execute($execution,$this->_api_context);



                //  DEBUG RESULT, remove it later **/
                if ($result->getState() == 'approved') {
                    $trans = $result->getTransactions();

                    // item info
                    $Subtotal = $trans[0]->getAmount()->getDetails()->getSubtotal();
                    $Tax = $trans[0]->getAmount()->getDetails()->getTax();

                    $payer = $result->getPayer();
                    // payer info //
                    $PaymentMethod =$payer->getPaymentMethod();
                    $PayerStatus =$payer->getStatus();
                    $PayerMail =$payer->getPayerInfo()->getEmail();

                    $relatedResources = $trans[0]->getRelatedResources();
                    $sale = $relatedResources[0]->getSale();
                    // sale info //
                    $saleId = $sale->getId();
                    $CreateTime = $sale->getCreateTime();
                    $UpdateTime = $sale->getUpdateTime();
                    $State = $sale->getState();
                    $Total = $sale->getAmount()->getTotal();

                    $save_data = [
                                'UserID'             => $user_id,
                                'TXNID'              => $saleId,
                                'PaymentGateway'     => $PaymentMethod,
                                'PayerStatus'        => $PayerStatus,
                                'PayerMail'          => $PayerMail,
                                'Amount'             => $Total,
                                'SubTotal'           => $Subtotal,
                                'Tax'                => $Tax,
                                'PaymentState'       => $State,
                                'TransactionDate'    => date('Y-m-d H:i:s', strtotime($CreateTime))
                                
                            ];
                    /** it's all right **/
                    /** Here Write your database logic like that insert record or value in database if you want **/
                    $this->data['OrderPaymentDetailID'] = $this->Order_payment_details_model->save($save_data);
                    $this->data['payment_method'] = 'Paypal';
                    $this->session->set_flashdata('message',"<div class='alert alert-success'>Payment success</div>");
                    /*redirect('paypal/success');*/
                }
                else
                {
                    $this->session->set_flashdata('message',"<div class='alert alert-danger'>Payment failed</div>");
                }
            }
        }
        else
        {
            redirect(base_url());
        }


        $this->data['products'] = $this->Order_model->getUserCartDetail($user_id);
        //echo '<pre>';print_r($this->data['products']);exit;
        
        if(empty($this->data['products'])){
            $this->session->set_flashdata('message', "<div class='alert alert-success'>There is no item in the cart.</div>");
            redirect(base_url());
        }
     
        $this->data['total_product'] = getTotalProduct($user_id);
        //$this->data['userinfo'] = $this->User_model->userexistByID($this->session->userdata('UserID'));
       
        //$this->data['step']  = $step;

        $this->data['view'] = 'frontend/checkout';
        $this->load->view('frontend/layouts/default', $this->data);
            
    }
    public function cart(){

        if (isset($this->session->userdata['admin']['UserID'])) {

            $user_id = $this->session->userdata['admin']['UserID'];

        } else {
            if (!get_cookie('temp_order_key')) {
                $user_id = 0;
                
            } else {
                $user_id = get_cookie('temp_order_key');
            }
        }
        if($user_id > 0)
        {
            $productCount = getTotalProduct($user_id)[0]->products_count;
        }
        else
        {
            redirect(base_url());
        }


        $this->data['products'] = $this->Order_model->getUserCartDetail($user_id);
        //echo '<pre>';print_r($this->data['products']);exit;
        
        if(empty($this->data['products'])){
            $this->session->set_flashdata('message', "<div class='alert alert-success'>There is no item in the cart.</div>");
            redirect(base_url());
        }
     
        $this->data['total_product'] = getTotalProduct($user_id);
        $this->data['view'] = 'frontend/cart';
        $this->load->view('frontend/layouts/default', $this->data);

    }
    public function place_order(){
        
        if ($this->input->post() != NULL) {
            $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('telephone', 'Telephone', 'trim|required');
            $this->form_validation->set_rules('billing_address_1', 'Billing Address 1', 'trim|required');
            $this->form_validation->set_rules('billing_country', 'Billing Country', 'trim|required');
            $this->form_validation->set_rules('billing_state', 'Billing State', 'trim|required');
            $this->form_validation->set_rules('billing_city', 'Billing City', 'trim|required');
            $this->form_validation->set_rules('billing_zip_code', 'Billing Zip Code', 'trim|required');
            $this->form_validation->set_rules('shipping_address_1', 'Shipping Address 1', 'trim|required');
            $this->form_validation->set_rules('shipping_country', 'Shipping Country', 'trim|required');
            $this->form_validation->set_rules('shipping_state', 'Shipping State', 'trim|required');
            $this->form_validation->set_rules('shipping_city', 'Shipping City', 'trim|required');
            $this->form_validation->set_rules('shipping_zip_code', 'Shipping Zip Code', 'trim|required');
            $this->form_validation->set_rules('amount', 'amount', 'trim|required');
            

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Please fill all required fileds.</div>');
                redirect('checkout');
            }
            $post_data = $this->input->post();
            if($post_data['payment_method'] == '')
            {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Please select a payment method first to checkout.</div>');
                redirect('checkout');
            }
            
            if (isset($this->session->userdata['admin']['UserID'])) {

                $user_id = $this->session->userdata['admin']['UserID'];

            } else {
                if (!get_cookie('temp_order_key')) {
                    $user_id = 0;
                    
                } else {
                    $user_id = get_cookie('temp_order_key');
                }
            }
            $products = $this->Order_model->getUserCartDetail($user_id);


            $BuyPieces = 0;
            foreach($products as $items)
            {
                if($items['TotalPieces'] > 0)
                {
                    $BuyPieces = $items['TotalPieces']*$items['quantity'];
                }
                else
                {
                    $BuyPieces = $items['quantity'];
                }
                if($BuyPieces > getRemainingProductQuantity($items['product_id']))
                {
                    $productDet = $this->Product_text_model->getWithMultipleFields(array('ProductID'=>$items['product_id']));
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> '.$productDet->Title.' is out of stock.</div>');
                    redirect('checkout');
                }
            }

            
            
            $save_data = array();
            $order_track_id = $save_data['order_track_id'] = RandomString();
            $save_data['user_id'] = $user_id;
            $save_data['first_name'] = $post_data['first_name'];
            $save_data['last_name'] = $post_data['last_name'];
            $save_data['telephone'] = $post_data['telephone'];
            $save_data['email'] = $post_data['email'];
            $save_data['billing_address_1'] = $post_data['billing_address_1'];
            $save_data['billing_address_2'] = $post_data['billing_address_2'];
            $save_data['billing_country'] = $post_data['billing_country'];
            $save_data['billing_state'] = $post_data['billing_state'];
            $save_data['billing_city'] = $post_data['billing_city'];
            $save_data['billing_zip_code'] = $post_data['billing_zip_code'];
            $shipping_address = $save_data['shipping_address_1'] = $post_data['shipping_address_1'];
            $save_data['shipping_address_2'] = $post_data['shipping_address_2'];
            $shipping_country = $save_data['shipping_country'] = $post_data['shipping_country'];
            $shipping_state = $save_data['shipping_state'] = $post_data['shipping_state'];
            $shipping_city = $save_data['shipping_city'] = $post_data['shipping_city'];
            $save_data['shipping_zip_code'] = $post_data['shipping_zip_code'];
            $save_data['payment_method'] = $post_data['payment_method'];
            /*$save_data['invoice_no'] = $post_data['invoice_no'];*/
            $save_data['currency_id'] = $this->data['CurrencyID'];
            $save_data['amount_deposit'] = getSelectedCurrencies($this->data['CurrencyID'], $post_data['amount'], false);
            //$save_data['deposit_date'] = date('Y-m-d H:i:s',strtotime($post_data['depositDate']));
            
            $save_data['created_at'] = $save_data['created_at'] = date('Y-m-d H:i:s');
            
            
            $insert_id = $this->Order_model->save('orders',$save_data);
            
            if($insert_id > 0){
                foreach($products as $product){
                    
                    $Price = (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? $product['PriceForWholeSaler'] : $product['Price']);
                    $DiscountPer = (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? $product['WholeSaleDiscount'] : $product['Discount']);

                    if($DiscountPer > 0)
                    {
                        $Discount = $Price * ($DiscountPer/100);

                        $Price = $Price - $Discount;
                    }
                    $child_data[] = [
                                        'order_id' => $insert_id,
                                        'product_id'	=> $product['product_id'],
                                        'quantity'	=> $product['quantity'],
                                        'size'  => $product['size'],
                                        'colour'  => $product['colour'],
                                        'price'        => getSelectedCurrencies($this->data['CurrencyID'], $Price, false),
                                        'TotalPieces'  => $product['TotalPieces'],
                                        'PacketData'  => $product['PacketData']
                                        
                                ];
                }
                
                
                $this->Order_model->insert_batch('order_items',$child_data);

                $CountryDetail = CountryList($post_data['shipping_country']);

                
                $invoice_number = $CountryDetail->CountryCode.'_'.date('ymdhms');
                $NewInvoiceNumber = $this->ValidateInvoiceNumber($invoice_number);

                $update_payment_table = array();
                
                $update_payment_table['OrderID'] = $insert_id;
                $update_payment_table['InvoiceNumber'] = $NewInvoiceNumber;
                if($post_data['payment_method'] != 'Paypal')
                    $update_payment_table['Amount'] = $post_data['amount'];


                $update = $this->Order_payment_details_model->update($update_payment_table, array('OrderPaymentDetailID'=>$post_data['OrderPaymentDetailID']));

                $message_customer = '<tr>
                                <td colspan="3" style="text-align: center;     text-align: center;
                                  background: #e02881;
                                  color: #fff;
                                  font-weight: bold;
                                  font-size: 18px">Order Confirmation</td>
                            </tr>
                            <tr><td height="15" colspan="3"></td></tr>
                            <tr><td height="60" colspan="3" style="text-align: center;"> <img src="'.base_url("assets/frontend/email_images").'/check1.png" width="50"/> </td></tr>
                            <tr style="text-align: center;"> <td colspan="3"><b>Your Order has been Sucessfully Placed.</b></td></tr>
                            <tr style="text-align: center; height: 50;"> <td colspan="3">Your Order No: <b style="color: #39b54a;">'.$order_track_id.'</b></td></tr>';
                $message_admin = '<tr>
                                <td colspan="3" style="text-align: center;     text-align: center;
                                  background: #e02881;
                                  color: #fff;
                                  font-weight: bold;
                                  font-size: 18px">Order Confirmation</td>
                            </tr>
                            <tr><td height="15" colspan="3"></td></tr>
                            <tr><td height="60" colspan="3" style="text-align: center;"> <img src="'.base_url("assets/frontend/email_images").'/check1.png" width="50"/> </td></tr>
                            <tr style="text-align: center;"> <td colspan="3"><b>You Recieved a New Order.</b></td></tr>
                            <tr style="text-align: center; height: 50;"> <td colspan="3">Your Order No: <b style="color: #39b54a;">'.$order_track_id.'</b></td></tr>';

                $message = '<tr><td height="25" colspan="3"></td></tr>
                            <tr><td height="25" colspan="3" style="background: #a264ad; color: #fff;">&nbsp; <b>Shipping</b></td></tr>
                            <tr><td height="15" colspan="3"></td></tr>
                            <tr style="background: #fadbff;"><td>&nbsp; Name:</td> <td colspan="2">'.$post_data['first_name'].' '.$post_data['last_name'].'</td></tr>
                            <tr><td>&nbsp; Phone:</td> <td colspan="2">'.$post_data['telephone'].'</td></tr>
                            <tr style="background: #fadbff;"><td>&nbsp; Email:</td> <td colspan="2">'.$post_data['email'].'</td></tr>
                            <tr><td>&nbsp; Order No:</td> <td colspan="2">'.$order_track_id.'</td></tr>
                            <tr style="background: #fadbff;"><td>&nbsp; Order Date:</td> <td colspan="2">'.date('d/m/Y',strtotime($save_data['created_at'])).'</td></tr>
                            <tr><td>&nbsp; Address:</td> <td colspan="2">'.$shipping_address.', '.$shipping_city.', '.$shipping_state.', '.$shipping_country.'</td></tr>
                            <tr><td height="15" colspan="3"></td></tr>
                            <tr><td height="25" colspan="3" style="background: #a264ad; color: #fff;">&nbsp; <b>Order Summary</b></td></tr>
                            <tr><td height="15" colspan="3"></td></tr>
                            <tr style="font-size: 14px; font-weight: bold;"><td>&nbsp;Qty</td><td>Product Name</td> <td>Price</td></tr>';


                foreach($products as $product){
                    
                    $Price = (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? $product['PriceForWholeSaler'] : $product['Price']);
                    $DiscountPer = (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? $product['WholeSaleDiscount'] : $product['Discount']);

                    if($DiscountPer > 0)
                    {
                        $Discount = $Price * ($DiscountPer/100);

                        $Price = $Price - $Discount;
                    }

                    $message .= '<tr style="font-size: 14px;"><td>&nbsp;'.$product['quantity'].'</td> <td>'.$product['Title'].'</td> <td>'.getSelectedCurrencies($this->data['CurrencyID'], $Price).'</td></tr>';
                }

                $message .= '<tr><td height="15" colspan="3"></td></tr>
                             <tr style="font-size: 14px;"><td>&nbsp;</td><td style="text-align: right;">Total:</td> <td><b>'.getSelectedCurrencies($this->data['CurrencyID'], $post_data['amount']).'</b></td></tr>
                             <tr style="font-size: 14px;"><td height="15" colspan="3"></td></tr>

                            <tr><td colspan="3" style="height:0.3px; background: #a264ad;"></td></tr>';

                $message_customer_footer = '<tr style="font-size: 14px;"><td height="15" colspan="3"></td></tr>

                            <tr><td height="20" colspan="3" style="text-align: center;"> <img src="'.base_url("assets/frontend/email_images").'/thank.jpg" width="150"/> </td></tr>

                            <tr><td height="25" colspan="3" style="text-align: center;"><b style="color: #a264ad;">Thank you for your Order!</b></td></tr>

                            <tr style="font-size: 14px;"><td height="15" colspan="3"></td></tr>';

                $email_data = array();
                $email_data['from']     = 'no_reply@leggingspro.com';
                $email_data['to']       = $this->data['site_setting']->Email;
                $email_data['subject']  = 'New order recieved';



                $email_data['body']  = emailTemplate($message_admin.$message);
                sendEmail($email_data);

                $email_data = array();
                $email_data['from']     = 'no_reply@leggingspro.com';
                $email_data['to']       = $post_data['email'];
                $email_data['subject']  = 'Order Placed';



                $email_data['body']  = emailTemplate($message_customer.$message.$message_customer_footer);
                sendEmail($email_data);

                $deleted_by['user_id'] = $user_id;
                $this->Order_model->delete('temp_orders',$deleted_by);

                $userdetail = $this->User_model->getJoinedData(false,'UserID','users.TemporaryUserID='.$user_id,'DESC','');

                //$deleted_user_by['TemporaryUserID'] = $user_id;
                $deleted_user_by['UserID'] = $userdetail[0]->UserID;

                $this->Order_model->delete('users',$deleted_user_by);

                $this->Order_model->delete('users_text',$deleted_user_by);

                $this->Order_model->delete('addresses',$deleted_user_by);

                $this->session->set_flashdata('message', '<div class="alert alert-success">Order Place successfully.Your order id is '.$save_data['order_track_id'].'</div>');
                redirect('home');
                
            }else{
                $this->session->set_flashdata('message', '<div class="alert alert-danger">Your order is not place.Please try again.</div>');
                redirect('home');
            }
            
            
            
            
            
        }
        
    }


    public function customerRegistrationValidation()
    {
        //print_rm($this->input->post());

        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
       // $this->form_validation->set_rules('RoleID', lang('role'), 'required');
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('telephone', 'Phone', 'required');
        $this->form_validation->set_rules('email', lang('email'), 'required|valid_email');
        
       

        $this->form_validation->set_rules('billing_address_1', 'Billing Address 1', 'required');
        $this->form_validation->set_rules('billing_country', 'Billing Country' , 'required');
        $this->form_validation->set_rules('billing_state', 'Billing State' , 'required');
        $this->form_validation->set_rules('billing_city', 'Billing City' , 'required');
        
        $this->form_validation->set_rules('shipping_address_1', 'Shipping Address' , 'required');
        $this->form_validation->set_rules('shipping_country', 'Shipping Country' , 'required');
        $this->form_validation->set_rules('shipping_state', 'Shipping State' , 'required');
        $this->form_validation->set_rules('shipping_city', 'Shipping City' , 'required');
            

        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else
        {
            if (isset($this->session->userdata['admin']['UserID'])) {

                $success['error']   = false;
                $success['success'] = 'Processing Payment........';
                $success['redirect'] = false;
                echo json_encode($success);
                exit;

            } else {
                if (!get_cookie('temp_order_key')) {
                    $user_id = 0;
                    
                } else {
                    $user_id = get_cookie('temp_order_key');

                    $userdetail = $this->User_model->getJoinedData(false,'UserID','users.TemporaryUserID='.$user_id,'DESC','');

                    if(!empty($userdetail))
                    {
                        $success['error']   = false;
                        $success['success'] = 'Processing Payment........';
                        $success['redirect'] = false;
                        echo json_encode($success);
                        exit;
                    }

                    $this->saveCustomer($user_id);
                }
            }
                //return true;
        }

    }

    private function saveCustomer($user_id){

        $post_data                          = $this->input->post();
        //$post_data['RoleID']                = 2;
        $parent                             = 'User_model';
        $child                              = 'User_text_model';
        $save_parent_data                   = array();
        $save_child_data                    = array();
       
        $getSortValue = $this->$parent->getLastRow('UserID');
           
        $sort = 0;
        if(!empty($getSortValue))
        {
           $sort = $getSortValue['SortOrder'] + 1;
        }

        $save_parent_data['TemporaryUserID']         = $user_id;
        $save_parent_data['SortOrder']      = $sort;
        $save_parent_data['IsActive']       = 0;
        $save_parent_data['RoleID']         = 3;

        $save_parent_data['Email']          = $post_data['email'];
        
        $save_parent_data['Phone']          = $post_data['telephone'];
        

        $save_parent_data['CreatedAt']      = $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');      
       // $save_parent_data['CreatedBy']      = $save_parent_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];
        
         
        

        $insert_id                          = $this->$parent->save($save_parent_data);
        if($insert_id > 0)
        {
            
            $default_lang = getDefaultLanguage();
            
            
            
            $save_child_data['FullName']                    = $post_data['first_name'].' '.$post_data['last_name'];
            $save_child_data['UserID']                      = $insert_id;
            $save_child_data['SystemLanguageID']            = $default_lang->SystemLanguageID;
            
            $this->$child->save($save_child_data);


            // add address 

            $save_address_array = array();
            $save_address_array['UserID']       = $insert_id;
            $save_address_array['Address1']     = $post_data['billing_address_1'];
            $save_address_array['Address2']     = $post_data['billing_address_2'];
            $save_address_array['CountryID']    = $post_data['billing_country']; 
            $save_address_array['StateID']      = $post_data['billing_state']; 
            $save_address_array['CityID']       = $post_data['billing_city']; 
            $save_address_array['ZipCode']       = $post_data['billing_zip_code'];
            $save_address_array['Type']         = 'billing';

            $this->Address_model->save($save_address_array);

            $save_address_array['Address1']     = $post_data['shipping_address_1'];
            $save_address_array['Address2']     = $post_data['shipping_address_2'];
            $save_address_array['CountryID']    = $post_data['shipping_country']; 
            $save_address_array['StateID']      = $post_data['shipping_state']; 
            $save_address_array['CityID']       = $post_data['shipping_city']; 
            $save_address_array['ZipCode']       = $post_data['shipping_zip_code'];
            $save_address_array['Type']         = 'shipping';

            $this->Address_model->save($save_address_array);
            
            
            $success['error']   = false;
            $success['success'] = 'Processing Payment........';
            $success['redirect'] = false;
            echo json_encode($success);
            exit;


        }else
        {
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }
    
    public function stripe_single_payment()
    {
        $post_data = $this->input->post();

        $post_data['token'] = json_decode($post_data['token']);

        if($this->session->userdata('admin'))
        {
            $user_id = $this->session->userdata['admin']['UserID'];    
        }
        else
        {
            $user_id = get_cookie('temp_order_key');
        }


        /*$invoice_number = $country_code['GlobalCode'].'_'.date('ymdhms');
        $NewInvoiceNumber = $this->ValidateInvoiceNumber($invoice_number);*/

        // $post['amount'] = '1';
        // $post['currency'] = STRIPE_CURRENCY;

        //$UserRole = $user_data['Role'];

        $amount = CHECK_OUT_TOTAL;

        // $survey_price = 10;
        // $amount = $post['survey'] * $survey_price;


        // $customer = $this->stripe->create_customer($post);
        $customer = $this->stripe->create_customer_by_token($post_data['token']);

        if(!method_exists ($customer, 'getMessage')) {

            $charge = $this->stripe->single_payment($customer->id, $amount, getCurrencyCode($this->data['CurrencyID']));  // Stripe Payment
            
            if(!method_exists ($charge, 'getMessage')) {
                
                if($charge->paid == true) {
                    
                    $save_data = [
                        'UserID'             => $user_id,
                        'CustomerID'         => $customer->id,
                        'ChargeID'           => $charge->id,
                        'TransactionDate'    => date('Y-m-d H:i:s'),
                        'Amount'             => $amount,
                        'PaymentGateway'     => 'stripe'
                    ];
                    $insert_result = $this->Order_payment_details_model->save($save_data);
                    if($insert_result)
                    {
                        $message = 'Payment Successful';
                    }
                    else
                    {
                        $message = 'Some error occurred while saving in DB.';
                    }
                    
                    
                } else {
                    $message = 'Some error occurred while charging.';
                }   // if($charge->paid == true)
                
            } else {
                $message = $customer->getMessage();
            }   // if(!method_exists ($charge, 'getMessage'))
            
        } else {
            $message = $customer->getMessage();
        }   // if(!method_exists ($customer, 'getMessage'))

        $this->session->set_flashdata(['tab_index' => 'surveys', 'message' => $message]);
        // redirect('customer_area');
        $response = array();

        $response['message'] = $message;
        $response['OrderPaymentDetailID'] = $insert_result;

        echo json_encode($response);
    }

    public function ValidateInvoiceNumber($invoice_number , $postfix = 0)
    {
        $NewInvoiceNumber = ($postfix === 0) ? $invoice_number : $invoice_number .'_'.$postfix;
        $check_invoice_number = $this->Order_payment_details_model->getWithMultipleFields(array('InvoiceNumber' => $NewInvoiceNumber));
        if($check_invoice_number){

            return $this->ValidateInvoiceNumber($invoice_number , ++$postfix);

        }else{
            return $NewInvoiceNumber;
        }
    }


    public function sendThankyouEmailToCustomer($order_id)
    {
        $data['order_items'] = $order_details  =  $this->Order_model->getAllOrderItems($order_id);
      
        $message = $this->load->view('emails/order_confirmation',$data, true);
        $email_data['to'] = $order_details[0]['email'];
        $email_data['subject'] = 'Order received at legging-pro : Order # ' . $order_details[0]['order_track_id'];
        $email_data['from'] = 'noreply@legging-pro.com';
        $email_data['body'] = $message;
        sendEmail($email_data);
        
        
        
        return true;
    }    
    

    


}
