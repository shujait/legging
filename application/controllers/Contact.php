<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();


        $this->data['language'] = $this->language;


    }

    public function index()
    {
              
        $this->data['view'] = 'frontend/contact';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
            case 'save':
                $this->validation();
                $this->save();
          break;
                 
        }
    }

    private function validation()
    {
        //print_rm($this->input->post());

        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
       
        $this->form_validation->set_rules('Name', 'Name', 'required');
        $this->form_validation->set_rules('Email', lang('email'), 'required');
        $this->form_validation->set_rules('Message', 'Message', 'required');
            

        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else
        {
                return true;
        }

    }

    private function save()
    {
        $post_data = $this->input->post();
        $message_admin = '<tr>
                                <td colspan="3" style="text-align: center;     text-align: center;
                                  background: #e02881;
                                  color: #fff;
                                  font-weight: bold;
                                  font-size: 18px">New Inquiry</td>
                            </tr>';

        $message ='<tr><td height="15" colspan="3"></td></tr>
            <tr style="background: #f1f1f1;"><td>&nbsp; Name:</td> <td colspan="2">'.$post_data['Name'].'</td></tr>
            <tr><td>&nbsp; Email:</td> <td colspan="2">'.$post_data['Email'].'</td></tr>
            <tr style="background: #f1f1f1;"><td>&nbsp; Message:</td> <td colspan="2">'.$post_data['Message'].'</td></tr>';

        $email_data = array();
        $email_data['from']     = 'no_reply@leggingspro.com';
        $email_data['to']       = $this->data['site_setting']->Email;
        $email_data['subject']  = 'New Inquiry';



        $email_data['body']  = emailTemplate($message_admin.$message);
        sendEmail($email_data);

        $success['error']   = false;
        $success['success'] = 'You message sent successfully.';
        $success['redirect'] = true;
        $success['url'] = 'Contact';
        echo json_encode($success);
        exit;
    }

}