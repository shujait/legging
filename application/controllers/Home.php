<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();  
        $this->data['language']      = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->load->Model([
            'Site_images_model',
            'Product_model',
            'Order_model',
            'Product_packets_model'
        ]);


    }


    public function index($id = '1')
    {


        //Tab1 Images
        $this->data['slider'] = $this->Site_images_model->getMultipleRows(array('FileID'=>$id, 'ImageType'=>'SliderImage'), true);
        $this->data['banner_after_slider'] = $this->Site_images_model->getMultipleRows(array('FileID'=>$id, 'ImageType'=>'BannerAftSldImage'), true);
        $this->data['banner_after_featur_prod'] = $this->Site_images_model->getMultipleRows(array('FileID'=>$id, 'ImageType'=>'BannerAftFeatProdImage'), true);
        $this->data['banner_below_cat'] = $this->Site_images_model->getMultipleRows(array('FileID'=>$id, 'ImageType'=>'BannerBelCat'), true);
        $this->data['banner_above_foot'] = $this->Site_images_model->getMultipleRows(array('FileID'=>$id, 'ImageType'=>'BannerAbvFoot'), true);

        $where = ' AND products.OnlyForWholeSale = 0';
        if($this->session->userdata('admin') && $this->session->userdata['admin']['RoleID'] == 4)
        {
            $where = '';
        }

        //Tab2 Images
        $this->data['home_page_cat_images'] = $this->Site_images_model->getMultipleRows(array('FileID'=>$id, 'ImageType'=>'HomePageCatImg'), true);

        //Feature Products
        $this->data['featured_products'] = $this->Product_model->getAllJoinedData(false,'ProductID',$this->language,'products.FeaturedProduct = 1'.$where);

        //Latest Products
        $this->data['latest_products'] = $this->Product_model->getAllJoinedData(false,'ProductID',$this->language,'products.CreatedAt >= DATE(NOW()) - INTERVAL 7 DAY'.$where);
       
        // echo 'here sarfraz';exit;
        $this->data['view'] = 'frontend/home/manage';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function add_to_cart()
    {


        if (isset($this->session->userdata['admin']['UserID'])) {

            $user_id = $this->session->userdata['admin']['UserID'];

        } else {
            if (!get_cookie('temp_order_key')) {
                $user_id = date('YmdHis') . RandomString();
                $cookie = array(
                    'name' => 'temp_order_key',
                    'value' => $user_id,
                    'expire' => time() + 86500,

                );

                set_cookie($cookie);
            } else {
                $user_id = get_cookie('temp_order_key');
            }
        }


        
       $posted_product_quantity = $this->input->post('product_quantity');



       $product_detail = $this->Product_model->get($this->input->post('product_id'),true,'ProductID');



        $already_added = $this->Order_model->alreadyAddToCart($user_id,$this->input->post('product_id'));
        
        if ($already_added) {

            
            
            $update = array();
            $update_by = array();

            $update['quantity'] = $posted_product_quantity; //+ $already_added['quantity'];


            if($this->session->userdata('admin') && $this->session->userdata['admin']['RoleID'] == 4 && !empty($this->input->post('product_packet'))){

                $product_packet = $this->Product_packets_model->getWithMultipleFields(array('ProductPacketID'=>$this->input->post('product_packet')), true);

                $update['TotalPieces'] = $product_packet['TotalPackets'];
                $update['PacketData'] = $product_packet['PacketsData'];

            }
            else
            {
                $update['size'] = $this->input->post('size');
                $update['colour'] = $this->input->post('colour');
                
            }

            if($this->session->userdata('admin') && $this->session->userdata['admin']['RoleID'] == 4 && !empty($this->input->post('product_packet'))){
                $BuyPieces = $posted_product_quantity*$product_packet['TotalPackets'];
            }
            else
            {
                $BuyPieces = $posted_product_quantity;
            }

            if($BuyPieces > getRemainingProductQuantity($this->input->post('product_id')))
            {
                $response['status'] = 'FALSE';
                $response['message'] = 'The product is out of stock.';
                $response['reload'] = false;


                echo json_encode($response);
                exit();
            }

            $update_by['temp_order_id'] = $already_added['temp_order_id'];

            $this->Order_model->update('temp_orders',$update, $update_by);


            $response['status'] = 'TRUE';
            $response['message'] = 'Added Successfully';
            $response['reload'] = true;


            echo json_encode($response);
            exit();

        } else {

            $save_data = array();
            $save_data['user_id'] = $user_id;//$this->session->userdata['user']['user_id'];
            $save_data['quantity'] = $posted_product_quantity;
            
            $save_data['product_id'] = $this->input->post('product_id');


            if($this->session->userdata('admin') && $this->session->userdata['admin']['RoleID'] == 4 && !empty($this->input->post('product_packet'))){

                $product_packet = $this->Product_packets_model->getWithMultipleFields(array('ProductPacketID'=>$this->input->post('product_packet')), true);
                
                $save_data['TotalPieces'] = $product_packet['TotalPackets'];
                $save_data['PacketData'] = $product_packet['PacketsData'];

            }
            else
            {
                $save_data['size'] = $this->input->post('size');
                $save_data['colour'] = $this->input->post('colour');
                
            }

            if($this->session->userdata('admin') && $this->session->userdata['admin']['RoleID'] == 4 && !empty($this->input->post('product_packet'))){
                $BuyPieces = $posted_product_quantity*$product_packet['TotalPackets'];
            }
            else
            {
                $BuyPieces = $posted_product_quantity;
            }

            if($BuyPieces > getRemainingProductQuantity($this->input->post('product_id')))
            {
                $response['status'] = 'FALSE';
                $response['message'] = 'The product is out of stock.';
                $response['reload'] = false;


                echo json_encode($response);
                exit();
            }

            $this->Order_model->save('temp_orders',$save_data);

            $response['status'] = 'TRUE';
            $response['message'] = 'Added Successfully';
            $response['reload'] = true;


            echo json_encode($response);
            exit();
        }


        /*} else {
            $url = 'page/product_details?id=' . $this->input->post('product_id');
            $this->session->set_userdata('url', $url);
            $response['status'] = false;
            $response['message'] = lang('login_first');
            $response['redirect'] = true;
            $response['url'] = 'page/login';


            echo json_encode($response);
            exit();

        }*/
    }
    
    public function update_quantity(){
        $temp_order_id = $this->input->post('temp_order_id');
        $quantity = $this->input->post('quantity');
       
        $update['quantity'] = $quantity;

        $update_by['temp_order_id'] = $temp_order_id;

        $this->Order_model->update('temp_orders',$update, $update_by);

        $response['status'] = 'TRUE';
        echo json_encode($response);
        exit();
        
    }
    
    public function delete_cart_item(){
        $temp_order_id = $this->input->post('temp_order_id');
       
        $deleted_by['temp_order_id'] = $temp_order_id;
        $this->Order_model->delete('temp_orders',$deleted_by);
        $response['status'] = 'TRUE';
        echo json_encode($response);
        exit();
        
    }


}