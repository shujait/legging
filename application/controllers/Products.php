<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();  
        $this->data['language']      = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->load->Model([
            'Site_images_model',
            'Product_model',
            'Product_packets_model',
            'Category_model',
            'Wishlist_model',
            'Reviews_model'
        ]);


    }

    public function index($CategoryID = '')
    {
        $where = ' AND products.OnlyForWholeSale = 0';
        if($this->session->userdata('admin') && $this->session->userdata['admin']['RoleID'] == 4)
        {
            $where = '';
        }
        if($CategoryID == '')
        {
            redirect(base_url());
        }
        $this->data['products'] = $this->Product_model->getAllJoinedData(false,'ProductID',$this->language,'(products.CategoryID = '.$CategoryID.' OR SubCategoryID = '.$CategoryID.' OR SubCategoryIDLevel2 = '.$CategoryID.')'.$where, 'ASC', 'SortOrder', false, 8, 0);

        $this->data['TotalProducts'] = $this->Product_model->getAllJoinedData(false,'ProductID',$this->language,'(products.CategoryID = '.$CategoryID.' OR SubCategoryID = '.$CategoryID.' OR SubCategoryIDLevel2 = '.$CategoryID.')'.$where);

        $this->data['categoryDet'] = $this->Category_model->getJoinedData(false, 'CategoryID', 'categories.CategoryID=' . $CategoryID, 'DESC', '');  
        
        $this->data['view'] = 'frontend/product';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function search()
    {

        $keyword = $this->input->get('keyword');
        $where = ' AND products.OnlyForWholeSale = 0';
        if($this->session->userdata('admin') && $this->session->userdata['admin']['RoleID'] == 4)
        {
            $where = '';
        }
        if($keyword == '')
        {
            redirect(base_url());
        }
        $this->data['products'] = $this->Product_model->getAllJoinedData(false,'ProductID',$this->language,"products_text.Title LIKE '%".$keyword."%'".$where, 'ASC', 'SortOrder', false, 8, 0);

        $this->data['TotalProducts'] = $this->Product_model->getAllJoinedData(false,'ProductID',$this->language,"products_text.Title LIKE '%".$keyword."%'".$where);

        $this->data['categoryDet'] = $keyword; 
        
        $this->data['view'] = 'frontend/product';
        $this->load->view('frontend/layouts/default', $this->data);
    }


    public function myWishlist(){
        if($this->session->userdata('admin')){

            $this->data['products'] = $this->Product_model->wishlist('wishlist.UserID ='.$this->session->userdata['admin']['UserID']);
            $this->data['view'] = 'frontend/wishlist';
            $this->load->view('frontend/layouts/default', $this->data);

        }else{
            redirect('products');
        }
    }

    public function detail($id = '1')
    {

        //Feature Products
        $this->data['product'] = $this->Product_model->getAllJoinedData(false,'ProductID',$this->language,'products.ProductID = '.$id);


        $RemainingProducts = getRemainingProductQuantity($this->data['product'][0]->ProductID);

        if($RemainingProducts <= 0)
        {
            redirect(base_url());
        }

        if(isset($this->session->userdata['admin']['RoleID']) && $this->session->userdata['admin']['RoleID'] == 4){
            $this->data['product_packets'] = $this->Product_packets_model->getMultipleRows(array('ProductID'=>$id), true);
        }
        $this->data['reviews'] = $this->Reviews_model->getReviews($id);	
       
        
        $this->data['view'] = 'frontend/product-detail';
        $this->load->view('frontend/layouts/default', $this->data);
    }


    public function removeItemFromWishlist(){


        $wish_list_data = array();
        $wish_list_data['ProductID']  = $this->input->post('id');
        $wish_list_data['UserID']     = $this->session->userdata['admin']['UserID'];
        $this->Wishlist_model->delete($wish_list_data);
        

        $success['error']   = false;
        $success['success'] = 'Removed Successfully.';
        
        echo json_encode($success);
        exit;
    }



    public function addToWishlist(){
        $wish_list_data = array();
        $wish_list_data['ProductID']  = $this->input->post('ProductID');
        $wish_list_data['UserID']     = $this->session->userdata['admin']['UserID'];

        $already = $this->Wishlist_model->getMultipleRows($wish_list_data);
        if($already){
            $errors['error'] =  'Already in your wishlist';
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }



        $insert_id = $this->Wishlist_model->save($wish_list_data);
        if($insert_id > 0){
            $success['error']   = false;
            $success['success'] = 'Added to wishlist.';
            $success['reload'] = true;
            
            echo json_encode($success);
            exit;
        }else{
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;

        }

    }

    public function LoadMoreProducts()
    {

        $CategoryID = $this->input->post('CategoryID');

        $StartRange = $this->input->post('StartRange');

        $TotalCount = $this->input->post('TotalCount');

        $Currency = $this->input->post('Currency');

        $SortOrder = $this->input->post('SortOrder');

        $Sizes = json_decode($this->input->post('Size'));

        $SortOrderFil = 'SortOrder';

        $SortOrderTyp = 'ASC';

        if($SortOrder != '')
        {
            if($SortOrder == '(Low-High)')
            {
                if($this->session->userdata('admin') && $this->session->userdata['admin']['RoleID'] == 4)
                {
                    $SortOrderFil = 'products.PriceForWholeSaler';
                }
                else
                {
                    $SortOrderFil = 'products.Price';
                }
                $SortOrderTyp = 'ASC';
            }
            if($SortOrder == '(High-Low)')
            {
                if($this->session->userdata('admin') && $this->session->userdata['admin']['RoleID'] == 4)
                {
                    $SortOrderFil = 'products.PriceForWholeSaler';
                }
                else
                {
                    $SortOrderFil = 'products.Price';
                }
                $SortOrderTyp = 'DESC';
            }
            if($SortOrder == '(A-Z)')
            {
                $SortOrderFil = 'products_text.Title';
                $SortOrderTyp = 'ASC';
            }
            if($SortOrder == '(Z-A)')
            {
                $SortOrderFil = 'products_text.Title';
                $SortOrderTyp = 'DESC';
            }
        }

        $where = ' AND products.OnlyForWholeSale = 0';
        if($this->session->userdata('admin') && $this->session->userdata['admin']['RoleID'] == 4)
        {
            $where = '';
        }

        if(count($Sizes) > 0)
        {
            $where .= ' AND (';
            foreach ($Sizes as $size) {
                $where .= "FIND_IN_SET('".$size."',Sizes) OR ";
            }
            $where = rtrim($where,' OR ');
            $where .= ')';
        }
        if(is_numeric($CategoryID))
        {
            $TotalProducts = $this->Product_model->getAllJoinedData(false,'ProductID',$this->language,'(products.CategoryID = '.$CategoryID.' OR SubCategoryID = '.$CategoryID.' OR SubCategoryIDLevel2 = '.$CategoryID.')'.$where, 'ASC', 'SortOrder', false, $TotalCount, $StartRange);

            $products = $this->Product_model->getAllJoinedData(false,'ProductID',$this->language,'(products.CategoryID = '.$CategoryID.' OR SubCategoryID = '.$CategoryID.' OR SubCategoryIDLevel2 = '.$CategoryID.')'.$where, $SortOrderTyp, $SortOrderFil, true, 8, $StartRange);
        }
        else
        {
            $TotalProducts = $this->Product_model->getAllJoinedData(false,'ProductID',$this->language,"products_text.Title LIKE '%".$CategoryID."%'".$where, 'ASC', 'SortOrder', false, $TotalCount, $StartRange);

            $products = $this->Product_model->getAllJoinedData(false,'ProductID',$this->language,"products_text.Title LIKE '%".$CategoryID."%'".$where, $SortOrderTyp, $SortOrderFil, true, 8, $StartRange);         
        }

        $html = '';

        if($products)
        {
            foreach ($products as $product) {
                $product_images = getSiteImages($product->ProductID, 'ProductImage');
                $RemainingProducts = getRemainingProductQuantity($product->ProductID);
                $DiscountPer = (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? $product->WholeSaleDiscount : $product->Discount);
                $DiscountPrice = 0;
                if($DiscountPer > 0)
                {
                    $Price = (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? $product->PriceForWholeSaler : $product->Price);

                    $Discount = $Price * ($DiscountPer/100);

                    $DiscountPrice = $Price - $Discount;
                }
        
                $html .= '<div class="col-md-3 col-sm-6 product-list">
                    <div class="thumbnail no-border no-padding">
                        <div class="media">
                            <a class="media-link" data-gal="prettyPhoto" href="'.($RemainingProducts <= 0 ? 'javascript:void(0);' : base_url('products/detail/'.$product->ProductID)).'">
                                <img src="'.(file_exists($product_images[0]['ImageName']) ? base_url() . $product_images[0]['ImageName'] : base_url('uploads/no_image.png')).'" alt=""/>
                            </a>
                        </div>
                        <div class="caption text-center" style="min-height: 250px">
                            <h4 class="caption-title"><a href="'.($RemainingProducts <= 0 ? 'javascript:void(0);' : base_url('products/detail/'.$product->ProductID)).'">'.$product->Title.'</a></h4>

                            <div class="price">
                                <ins class="discount" '.($DiscountPrice>0 ? 'style="text-decoration: line-through; color:red"' : '').'>'.(isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? getSelectedCurrencies($Currency,$product->PriceForWholeSaler) : getSelectedCurrencies($Currency,$product->Price)).'</ins>';

                                if($DiscountPrice>0)
                                {
                                
                                    $html .= '<ins style="font-size: 18px;">'.getSelectedCurrencies($Currency,$DiscountPrice).'</ins>';
                                }
                            $html .= '</div>
                            <div class="buttons">
                                <a class="btn btn-grey" '.($RemainingProducts <= 0 ? 'disabled="disabled"' : '').' href="'.($RemainingProducts <= 0 ? 'javascript:void(0);' : base_url('products/detail/'.$product->ProductID)).'">'.($RemainingProducts <= 0 ? 'Out of stock' : 'BUY').'</a>
                            </div>
                        </div>
                    </div>
                </div>'; 
            }
            $success['error']   = false;
            $success['success'] = 'Got products.';
            $success['productCount'] = count($TotalProducts);
            $success['html'] = $html;
            
            echo json_encode($success);
            exit;
        }else{
            $errors['error'] =  'No More Products.';
            $errors['success'] = false;
            echo json_encode($errors);
            exit;

        }
        
    }

    public function GetFilteredProducts()
    {

        $CategoryID = $this->input->post('CategoryID');

        $Currency = $this->input->post('Currency');

        $SortOrder = $this->input->post('SortOrder');

        $Sizes = json_decode($this->input->post('Size'));

        $SortOrderFil = 'SortOrder';

        $SortOrderTyp = 'ASC';

        if($SortOrder != '')
        {
            if($SortOrder == '(Low-High)')
            {
                if($this->session->userdata('admin') && $this->session->userdata['admin']['RoleID'] == 4)
                {
                    $SortOrderFil = 'products.PriceForWholeSaler';
                }
                else
                {
                    $SortOrderFil = 'products.Price';
                }
                $SortOrderTyp = 'ASC';
            }
            if($SortOrder == '(High-Low)')
            {
                if($this->session->userdata('admin') && $this->session->userdata['admin']['RoleID'] == 4)
                {
                    $SortOrderFil = 'products.PriceForWholeSaler';
                }
                else
                {
                    $SortOrderFil = 'products.Price';
                }
                $SortOrderTyp = 'DESC';
            }
            if($SortOrder == '(A-Z)')
            {
                $SortOrderFil = 'products_text.Title';
                $SortOrderTyp = 'ASC';
            }
            if($SortOrder == '(Z-A)')
            {
                $SortOrderFil = 'products_text.Title';
                $SortOrderTyp = 'DESC';
            }
        }

        $where = ' AND products.OnlyForWholeSale = 0';
        if($this->session->userdata('admin') && $this->session->userdata['admin']['RoleID'] == 4)
        {
            $where = '';
        }

        if(count($Sizes) > 0)
        {
            $where .= ' AND (';
            foreach ($Sizes as $size) {
                $where .= "FIND_IN_SET('".$size."',Sizes) OR ";
            }
            $where = rtrim($where,' OR ');
            $where .= ')';
        }

        if(is_numeric($CategoryID))
        {
            $TotalProducts = $this->Product_model->getAllJoinedData(false,'ProductID',$this->language,'(products.CategoryID = '.$CategoryID.' OR SubCategoryID = '.$CategoryID.' OR SubCategoryIDLevel2 = '.$CategoryID.')'.$where);

            $products = $this->Product_model->getAllJoinedData(false,'ProductID',$this->language,'(products.CategoryID = '.$CategoryID.' OR SubCategoryID = '.$CategoryID.' OR SubCategoryIDLevel2 = '.$CategoryID.')'.$where, $SortOrderTyp, $SortOrderFil, true, 8, 0);
        }
        else
        {
            $TotalProducts = $this->Product_model->getAllJoinedData(false,'ProductID',$this->language,"products_text.Title LIKE '%".$CategoryID."%'".$where);

            $products = $this->Product_model->getAllJoinedData(false,'ProductID',$this->language,"products_text.Title LIKE '%".$CategoryID."%'".$where, $SortOrderTyp, $SortOrderFil, true, 8, 0);   
        }

        $html = '';

        if($products)
        {
            foreach ($products as $product) {
                $product_images = getSiteImages($product->ProductID, 'ProductImage');
                $RemainingProducts = getRemainingProductQuantity($product->ProductID);
                $DiscountPer = (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? $product->WholeSaleDiscount : $product->Discount);
                $DiscountPrice = 0;
                if($DiscountPer > 0)
                {
                    $Price = (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? $product->PriceForWholeSaler : $product->Price);

                    $Discount = $Price * ($DiscountPer/100);

                    $DiscountPrice = $Price - $Discount;
                }
        
                $html .= '<div class="col-md-3 col-sm-6 product-list">
                    <div class="thumbnail no-border no-padding">
                        <div class="media">
                            <a class="media-link" data-gal="prettyPhoto" href="'.($RemainingProducts <= 0 ? 'javascript:void(0);' : base_url('products/detail/'.$product->ProductID)).'">
                                <img src="'.(file_exists($product_images[0]['ImageName']) ? base_url() . $product_images[0]['ImageName'] : base_url('uploads/no_image.png')).'" alt=""/>
                            </a>
                        </div>
                        <div class="caption text-center" style="min-height: 250px">
                            <h4 class="caption-title"><a href="'.($RemainingProducts <= 0 ? 'javascript:void(0);' : base_url('products/detail/'.$product->ProductID)).'">'.$product->Title.'</a></h4>

                            <div class="price">
                                <ins class="discount" '.($DiscountPrice>0 ? 'style="text-decoration: line-through; color:red"' : '').'>'.(isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? getSelectedCurrencies($Currency,$product->PriceForWholeSaler) : getSelectedCurrencies($Currency,$product->Price)).'</ins>';

                                if($DiscountPrice>0)
                                {
                                
                                    $html .= '<ins style="font-size: 18px;">'.getSelectedCurrencies($Currency,$DiscountPrice).'</ins>';
                                }
                            $html .= '</div>
                            <div class="buttons">
                                <a class="btn btn-grey" '.($RemainingProducts <= 0 ? 'disabled="disabled"' : '').' href="'.($RemainingProducts <= 0 ? 'javascript:void(0);' : base_url('products/detail/'.$product->ProductID)).'">'.($RemainingProducts <= 0 ? 'Out of stock' : 'BUY').'</a>
                            </div>
                        </div>
                    </div>
                </div>'; 
            }
            $success['error']   = false;
            $success['success'] = 'Got products.';
            $success['productCount'] = count($TotalProducts);
            $success['html'] = $html;
            
            echo json_encode($success);
            exit;
        }else{
            $html = '<div class="not-found-product">
                        <img src="'.base_url().'assets/backend/img/notfound.png">
                        <p>Products not found!</p>
                    </div>';

            $errors['error'] =  'No More Products.';
            $errors['success'] = false;
            $errors['productCount'] = 0;
            $errors['html'] = $html;
            echo json_encode($errors);
            exit;

        }
        
    }

	public function addReview()
    {

        $this->reviewValidation();
        $post_data = $this->input->post();
        $save_review_array = array();
        $save_review_array['UserID']     = $this->session->userdata['admin']['UserID'];
        $save_review_array['ProductID']     = $post_data['ProductID'];
        $save_review_array['Review']     = $post_data['review'];
        $save_review_array['Rating']     = $post_data['rating'];

        $this->Reviews_model->save($save_review_array);
        $success['error']   = false;
        $success['reload']   = True;
        $success['success'] = 'Your review is posted successfully.';
        echo json_encode($success);
        exit;

    }


    private function reviewValidation()
    {


        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('review', 'Review description', 'required');
        $this->form_validation->set_rules('rating', 'Rating', 'required');






        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }

    }

}