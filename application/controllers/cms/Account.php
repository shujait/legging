<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('User_text_model');
        $this->load->model('Address_model');
        $this->load->model('Country_model');
        $this->load->model('State_model');
        $this->load->model('City_model');
        $this->data['site_setting'] = $this->getSiteSetting();
        $this->data['language']      = $this->language;

    }

    public function index()
    {
        if ($this->session->userdata('admin')) {
            redirect(base_url('cms/dashboard'));

        }

        redirect(base_url());
    }

    //User Login
    public function login()
    {
        redirect(base_url());
        if ($this->session->userdata('admin')) {
            redirect(base_url('cms/dashboard'));

        }
        $this->data['view'] = 'backend/login';
        $this->load->view('backend/login', $this->data);

    }


    public function profile(){

        if($this->session->userdata['admin']['RoleID'] == 1 || $this->session->userdata['admin']['RoleID'] == 2){
                redirect(base_url('cms/user/edit/'.$this->session->userdata['admin']['UserID'])); 
        }
        
        $this->data['result']           = $this->User_model->getJoinedData(false,'UserID','users.UserID = '.$this->session->userdata['admin']['UserID'].'');
    
        
        if(!$this->data['result']){
            $this->session->set_flashdata('message',lang('some_thing_went_wrong'));
           redirect(base_url('cms/dashboard')); 
        }
        
        
       
       
        $this->data['result'] = $this->data['result'][0];
        
        $this->data['shipping_address'] = $this->Address_model->getWithMultipleFields(array('UserID' => $this->session->userdata['admin']['UserID'],'Type' => 'shipping'),true);
        
        $this->data['billing_address']  = $this->Address_model->getWithMultipleFields(array('UserID' => $this->session->userdata['admin']['UserID'],'Type' => 'billing'),true);



        $this->data['countries'] = $this->Country_model->getAllJoinedData(false,'CountryID',$this->language,false,'ASC','countries_text.Title',true);


        if($this->data['shipping_address'] && $this->data['shipping_address']['CountryID'] != 0){
            $this->data['shipping_states'] = $this->State_model->getAllJoinedData(true,'StateID',$this->language,'states.CountryID = '.$this->data['shipping_address']['CountryID'] .'','ASC','states_text.Title',true);   
        }


        if($this->data['billing_address'] && $this->data['billing_address']['CountryID'] != 0){
            $this->data['billing_states'] = $this->State_model->getAllJoinedData(true,'StateID',$this->language,'states.CountryID = '.$this->data['billing_address']['CountryID'] .'','ASC','states_text.Title',true);   
        }


        //city

        if($this->data['shipping_address'] && $this->data['shipping_address']['StateID'] != 0){
            $this->data['shipping_cities'] = $this->City_model->getAllJoinedData(true,'CityID',$this->language,'cities.StateID = '.$this->data['shipping_address']['StateID'] .'','ASC','cities_text.Title',true);   
        }


        if($this->data['billing_address'] && $this->data['billing_address']['StateID'] != 0){
            $this->data['billing_cities'] = $this->City_model->getAllJoinedData(true,'CityID',$this->language,'cities.StateID = '.$this->data['billing_address']['StateID'] .'','ASC','cities_text.Title',true);   
        }

        //city
        

        if($this->session->userdata['admin']['RoleID'] == 4){
            $this->data['view'] = 'frontend/profile';
        }elseif($this->session->userdata['admin']['RoleID'] == 3){
            $this->data['view'] = 'frontend/customer_profile';
        }
       
        
       
        $this->load->view('backend/layouts/default',$this->data);
    }


    public function checkLogin()
    {
        $data = array();
        $post_data = $this->input->post();
        $this->loginValidation();
        $checkUser = $this->checkUser($post_data);

        if ($checkUser != true) {
            $data = array();
            $data['success'] = 'false';
            $data['error'] = 'Email or password incorrect.';
            echo json_encode($data);
            exit();
        } else {
            $data = array();
            $data['success'] = 'Login Successfully';
            $data['error'] = false;
            $data['redirect'] = true;
            $data['url'] = 'cms/dashboard';
            echo json_encode($data);
            exit();
        }
    }

    private function loginValidation()
    {


        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Email', 'Email', 'required');
        $this->form_validation->set_rules('Password', 'Password', 'required');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }

    }

    private function checkUser($post_data)
    {
        $post_data['Password'] = md5($post_data['Password']);

        $user = $this->User_model->getUserData($post_data, $this->language);

        if (!empty($user)) {

            if($user['RoleID'] > '2')
            {
                $data = array();
                $data['success'] = 'false';
                $data['error'] = 'You can\'t be login from here';
                echo json_encode($data);
                exit();

            }
            /*$user = (array)$user;*/
            $this->session->set_userdata('admin', $user);
            //$this->updateUserLoginStatus();
            return true;
        } else {
            return false;
        }

    }


    //Logout
    public function logout()
    {
        $data = array();
        $arr_update = array();
        $user = $this->session->userdata('admin');
        $this->session->unset_userdata('admin');
        redirect($this->config->item('base_url') . 'cms/account/login');

    }

    private function registrationValidation()
    {
        //print_rm($this->input->post());

        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
       // $this->form_validation->set_rules('RoleID', lang('role'), 'required');
        
        $this->form_validation->set_rules('Contact', lang('company'), 'required');
        $this->form_validation->set_rules('BusinessType', lang('business_type'), 'required');

        $this->form_validation->set_rules('BillingAddress1', 'Billing Address 1', 'required');
        $this->form_validation->set_rules('BillingCountry', 'Billing Country' , 'required');
        $this->form_validation->set_rules('BillingState', 'Billing State' , 'required');
        $this->form_validation->set_rules('BillingCity', 'Billing City' , 'required');
        
        $this->form_validation->set_rules('ShippingAddress1', 'Shipping Address' , 'required');
        $this->form_validation->set_rules('ShippingCountry', 'Shipping Country' , 'required');
        $this->form_validation->set_rules('ShippingState', 'Shipping State' , 'required');
        $this->form_validation->set_rules('ShippingCity', 'Shipping City' , 'required');
            

        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else
        {
                return true;
        }

    }

     private function customerRegistrationValidation()
    {
        //print_rm($this->input->post());

        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
       // $this->form_validation->set_rules('RoleID', lang('role'), 'required');
        $this->form_validation->set_rules('FullName', 'Full Name', 'required');
        $this->form_validation->set_rules('Phone', 'Phone', 'required');
        

        $this->form_validation->set_rules('BillingAddress1', 'Billing Address 1', 'required');
        $this->form_validation->set_rules('BillingCountry', 'Billing Country' , 'required');
        $this->form_validation->set_rules('BillingState', 'Billing State' , 'required');
        $this->form_validation->set_rules('BillingCity', 'Billing City' , 'required');
        
        $this->form_validation->set_rules('ShippingAddress1', 'Shipping Address' , 'required');
        $this->form_validation->set_rules('ShippingCountry', 'Shipping Country' , 'required');
        $this->form_validation->set_rules('ShippingState', 'Shipping State' , 'required');
        $this->form_validation->set_rules('ShippingCity', 'Shipping City' , 'required');
            

        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else
        {
                return true;
        }

    }


    public function updateWholeSaler(){

        $this->registrationValidation();

        $post_data                          = $this->input->post();
        //$post_data['RoleID']                = 2;
        $parent                             = 'User_model';
        $child                              = 'User_text_model';
        $save_parent_data                   = array();
        $save_child_data                    = array();
       
        
        

        
        
        $save_parent_data['Contact']        = $post_data['Contact'];
        $save_parent_data['Company']        = $post_data['Company'];
        $save_parent_data['CompanyWebsite'] = $post_data['CompanyWebsite'];
        $save_parent_data['Phone']          = $post_data['Phone'];
        $save_parent_data['Fax']            = $post_data['Fax'];
        $save_parent_data['BusinessType']   = $post_data['BusinessType'];
        $save_parent_data['PreferredSale']  = $post_data['PreferredSale'];
        $save_parent_data['ResalePeritNumber']   = $post_data['ResalePeritNumber'];
        if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
            $save_parent_data['Image'] = $this->session->userdata['admin']['Image'] =$this->uploadImage('Image', 'uploads/images/');

        }



        $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');      
       // $save_parent_data['CreatedBy']      = $save_parent_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];
        
         
        

        $this->$parent->update($save_parent_data,array('UserID' => $this->session->userdata['admin']['UserID']));
        
            // add address 

            $save_address_array = array();
           
            $save_address_array['Address1']     = $post_data['BillingAddress1'];
            $save_address_array['Address2']     = $post_data['BillingAddress2'];
            $save_address_array['CountryID']    = $post_data['BillingCountry']; 
            $save_address_array['StateID']      = $post_data['BillingState']; 
            $save_address_array['CityID']       = $post_data['BillingCity']; 
            $save_address_array['ZipCode']       = $post_data['BillingZipCode'];
           

            $this->Address_model->update($save_address_array,array('UserID' => $this->session->userdata['admin']['UserID'],'Type' => 'billing'));

            $save_address_array['Address1']     = $post_data['ShippingAddress1'];
            $save_address_array['Address2']     = $post_data['ShippingAddress2'];
            $save_address_array['CountryID']    = $post_data['ShippingCountry']; 
            $save_address_array['StateID']      = $post_data['ShippingState']; 
            $save_address_array['CityID']       = $post_data['ShippingCity']; 
            $save_address_array['ZipCode']       = $post_data['ShippingZipCode'];
            
            $this->Address_model->update($save_address_array,array('UserID' => $this->session->userdata['admin']['UserID'],'Type' => 'shipping'));


             



            
            $success['error']   = false;
            $success['success'] = 'Updated Successfully';
            $success['reload'] = true;
           
            echo json_encode($success);
            exit;


        
    }


    public function updateCustomer(){

        $this->customerRegistrationValidation();

        $post_data                          = $this->input->post();
        //$post_data['RoleID']                = 2;
        $parent                             = 'User_model';
        $child                              = 'User_text_model';
        $save_parent_data                   = array();
        $save_child_data                    = array();
       
        
        

        
        
       
        $save_parent_data['Phone']          = $post_data['Phone'];
        
       

        if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
            $save_parent_data['Image'] = $this->session->userdata['admin']['Image'] = $this->uploadImage('Image', 'uploads/images/');
        }

        $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');      
       // $save_parent_data['CreatedBy']      = $save_parent_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];
        
         
        

        $this->$parent->update($save_parent_data,array('UserID' => $this->session->userdata['admin']['UserID']));

        $default_lang = getDefaultLanguage();
            
            
            
            $save_child_data['FullName']                    = $post_data['FullName'];
            $update_child_data = array();
            $update_child_data['UserID']                      = $this->session->userdata['admin']['UserID'];
            $update_child_data['SystemLanguageID']            = $default_lang->SystemLanguageID;
            
            $this->$child->update($save_child_data,$update_child_data);
        
            // add address 

            $save_address_array = array();
           
            $save_address_array['Address1']     = $post_data['BillingAddress1'];
            $save_address_array['Address2']     = $post_data['BillingAddress2'];
            $save_address_array['CountryID']    = $post_data['BillingCountry']; 
            $save_address_array['StateID']      = $post_data['BillingState']; 
            $save_address_array['CityID']       = $post_data['BillingCity']; 
            $save_address_array['ZipCode']       = $post_data['BillingZipCode'];
           

            $this->Address_model->update($save_address_array,array('UserID' => $this->session->userdata['admin']['UserID'],'Type' => 'billing'));

            $save_address_array['Address1']     = $post_data['ShippingAddress1'];
            $save_address_array['Address2']     = $post_data['ShippingAddress2'];
            $save_address_array['CountryID']    = $post_data['ShippingCountry']; 
            $save_address_array['StateID']      = $post_data['ShippingState']; 
            $save_address_array['CityID']       = $post_data['ShippingCity']; 
            $save_address_array['ZipCode']       = $post_data['ShippingZipCode'];
            
            $this->Address_model->update($save_address_array,array('UserID' => $this->session->userdata['admin']['UserID'],'Type' => 'shipping'));


             



            
            $success['error']   = false;
            $success['success'] = 'Updated Successfully';
            $success['reload'] = true;
            
            echo json_encode($success);
            exit;


        
    }
}