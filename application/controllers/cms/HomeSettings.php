<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeSettings extends Base_Controller {
    public  $data = array();
    
    public function __construct() 
    {
           
        parent::__construct();
        checkAdminSession();
                
                $this->load->Model([
            ucfirst($this->router->fetch_class()).'_model',
            ucfirst($this->router->fetch_class()).'_text_model',
            'Site_images_model'
        ]);
                
                
                
                
                $this->data['language']      = $this->language;
                $this->data['ControllerName'] = $this->router->fetch_class();
                $this->data['Parent_model']   = ucfirst($this->router->fetch_class()).'_model';
                $this->data['Child_model']    = ucfirst($this->router->fetch_class()).'_text_model';
                $this->data['TableKey'] = 'HomeSettingsID';
                $this->data['Table'] = 'homesettingses';
       
        
    }
     
    
    public function index($id = '1')
    {
          $parent                             = $this->data['Parent_model'];
          $child                              = $this->data['Child_model'];

          $this->data['result']          = $this->$parent->getJoinedData(false,$this->data['TableKey'],$this->data['Table'].'.'.$this->data['TableKey'].'='.$id,'DESC','');

          //Tab1 Images
          $this->data['slider'] = $this->Site_images_model->getMultipleRows(array('FileID'=>$id, 'ImageType'=>'SliderImage'), true);
          $this->data['banner_after_slider'] = $this->Site_images_model->getMultipleRows(array('FileID'=>$id, 'ImageType'=>'BannerAftSldImage'), true);
          $this->data['banner_after_featur_prod'] = $this->Site_images_model->getMultipleRows(array('FileID'=>$id, 'ImageType'=>'BannerAftFeatProdImage'), true);
          $this->data['banner_below_cat'] = $this->Site_images_model->getMultipleRows(array('FileID'=>$id, 'ImageType'=>'BannerBelCat'), true);
          $this->data['banner_above_cat'] = $this->Site_images_model->getMultipleRows(array('FileID'=>$id, 'ImageType'=>'BannerAbvFoot'), true);

          //Tab2 Images
          $this->data['home_page_cat_images'] = $this->Site_images_model->getMultipleRows(array('FileID'=>$id, 'ImageType'=>'HomePageCatImg'), true);


          $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/add';
        
          $this->data['results'] = $this->$parent->getAllJoinedData(false,$this->data['TableKey'],$this->language);
          
          $this->load->view('backend/layouts/default',$this->data);
    }
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
            case 'saveBannerImages':
                $this->update();
            break;

            case 'saveCategoryImage':
                $this->SaveCategoryImage();
            break;
                 
        }
    }
    
    
    private function validate(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Title', lang('title'), 'required|is_unique['.$this->data['Table'].'_text.Title]');
      
        



        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }
     
    private function update()
    {

        if(!checkUserRightAccess(55,$this->session->userdata['admin']['UserID'],'CanEdit')){
            $errors['error'] =  lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }

        $post_data = $this->input->post();
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        if(isset($post_data[$this->data['TableKey']])){
            
            $id = base64_decode($post_data[$this->data['TableKey']]);
            $this->data['result']          = $this->$parent->getJoinedData(false,$this->data['TableKey'],$this->data['Table'].'.'.$this->data['TableKey'].'='.$id,'DESC','');


            if(!$this->data['result']){
               $errors['error'] =  lang('some_thing_went_wrong');
               $errors['success'] =   false;
               $errors['redirect'] = true;
               $errors['url'] = 'cms/'.$this->router->fetch_class();
               echo json_encode($errors);
               exit;
            }

            //Upload Banners
            if (isset($_FILES['SliderImage']) && !empty($_FILES['SliderImage'])) {
                $file = 'SliderImage';
                $path = 'uploads/banner_images/';
                $key = $id;
                $type = 'SliderImage';
                $this->uploadImage($file, $path, $key, $type, TRUE);
            } else {
                $data['FileID'] = $id;
                $data['ImageType'] = 'SliderImage';
                $data['ImageName'] = 'assets/backend/images/no_image.jpg';
                $this->Site_images_model->save($data);
            }

            if(!empty($this->input->post('Description')))
            {
                $Description = $this->input->post('Description');

                foreach($Description as $SiteImageID => $Des)
                {
                    $update_redirect_link = array();
                    $update_by = array();

                    $update_redirect_link['ImageDescription'] = $Des;
                    $update_by['SiteImageID'] = $SiteImageID;

                    $this->Site_images_model->update($update_redirect_link, $update_by);
                }
            }

            //Upload Banners
            if (isset($_FILES['BannerAftSldImage']) && !empty($_FILES['BannerAftSldImage'])) {
                $file = 'BannerAftSldImage';
                $path = 'uploads/banner_images/';
                $key = $id;
                $type = 'BannerAftSldImage';
                $this->uploadImage($file, $path, $key, $type, TRUE);
            } else {
                $data['FileID'] = $id;
                $data['ImageType'] = 'BannerAftSldImage';
                $data['ImageName'] = 'assets/backend/images/no_image.jpg';
                $this->Site_images_model->save($data);
            }

            //Upload Banners
            if (isset($_FILES['BannerAftFeatProdImage']) && !empty($_FILES['BannerAftFeatProdImage'])) {
                $file = 'BannerAftFeatProdImage';
                $path = 'uploads/banner_images/';
                $key = $id;
                $type = 'BannerAftFeatProdImage';
                $this->uploadImage($file, $path, $key, $type, TRUE);
            } else {
                $data['FileID'] = $id;
                $data['ImageType'] = 'BannerAftFeatProdImage';
                $data['ImageName'] = 'assets/backend/images/no_image.jpg';
                $this->Site_images_model->save($data);
            }

            //Upload Banners
            if (isset($_FILES['BannerBelCat']) && !empty($_FILES['BannerBelCat'])) {
                $file = 'BannerBelCat';
                $path = 'uploads/banner_images/';
                $key = $id;
                $type = 'BannerBelCat';
                $this->uploadImage($file, $path, $key, $type, TRUE);
            } else {
                $data['FileID'] = $id;
                $data['ImageType'] = 'BannerBelCat';
                $data['ImageName'] = 'assets/backend/images/no_image.jpg';
                $this->Site_images_model->save($data);
            }

            //Upload Banners
            if (isset($_FILES['BannerAbvFoot']) && !empty($_FILES['BannerAbvFoot'])) {
                $file = 'BannerAbvFoot';
                $path = 'uploads/banner_images/';
                $key = $id;
                $type = 'BannerAbvFoot';
                $this->uploadImage($file, $path, $key, $type, TRUE);
            } else {
                $data['FileID'] = $id;
                $data['ImageType'] = 'BannerAbvFoot';
                $data['ImageName'] = 'assets/backend/images/no_image.jpg';
                $this->Site_images_model->save($data);
            }

            if(!empty($this->input->post('RedirectLink')))
            {
                $RedirectLinks = $this->input->post('RedirectLink');
                

                foreach($RedirectLinks as $SiteImageID => $Link)
                {
                    $update_redirect_link = array();
                    $update_by = array();

                    $update_redirect_link['RedirectLink'] = $Link;
                  //  $update_redirect_link['ImageDescription'] = $Description_R[$SiteImageID];
                    $update_by['SiteImageID'] = $SiteImageID;

                    $this->Site_images_model->update($update_redirect_link, $update_by);
                }
            }
                
            unset($post_data['form_type']);
            $save_parent_data                   = array();
            $save_child_data                    = array();
                    
            $save_parent_data['UpdatedAt']      = date('Y-m-d H:i:s');     
            $save_parent_data['UpdatedBy']      = $this->session->userdata['admin']['UserID'];
                    
            $update_by  = array();
            $update_by[$this->data['TableKey']]  = base64_decode($post_data[$this->data['TableKey']]);
                    
                    
            $this->$parent->update($save_parent_data,$update_by);
                    
                
        
              $success['error']   = false;
              $success['success'] = lang('update_successfully');
              $success['redirect'] = true;
              $success['url'] = 'cms/'.$this->router->fetch_class().'/?tab='.$post_data['tab'];
        
              echo json_encode($success);
              exit;  
        
        }else{
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
            
        }
    }


    private function SaveCategoryImage()
    {
        
        if(!checkUserRightAccess(55,$this->session->userdata['admin']['UserID'],'CanEdit')){
            $errors['error'] =  lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }

        $post_data = $this->input->post();
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        if(isset($post_data[$this->data['TableKey']])){
            
            $id = base64_decode($post_data[$this->data['TableKey']]);
            $this->data['result']          = $this->$parent->getJoinedData(false,$this->data['TableKey'],$this->data['Table'].'.'.$this->data['TableKey'].'='.$id,'DESC','');


            if(!$this->data['result']){
               $errors['error'] =  lang('some_thing_went_wrong');
               $errors['success'] =   false;
               $errors['redirect'] = true;
               $errors['url'] = 'cms/'.$this->router->fetch_class();
               echo json_encode($errors);
               exit;
            }


            if(!empty($this->input->post('Description')))
            {
                $Description = $this->input->post('Description');

                foreach($Description as $SiteImageID => $Des)
                {
                    $update_redirect_link = array();
                    $update_by = array();

                    $update_redirect_link['ImageDescription'] = $Des;
                    $update_by['SiteImageID'] = $SiteImageID;

                    $this->Site_images_model->update($update_redirect_link, $update_by);
                }
            }

            //Upload Banners
            if (isset($_FILES['HomePageCatImg']) && !empty($_FILES['HomePageCatImg'])) {
                $file = 'HomePageCatImg';
                $path = 'uploads/home_category_images/';
                $key = $id;
                $type = 'HomePageCatImg';
                $this->uploadImage($file, $path, $key, $type, TRUE);
            } else {
                $data['FileID'] = $id;
                $data['ImageType'] = 'HomePageCatImg';
                $data['ImageName'] = 'assets/backend/images/no_image.jpg';
                $this->Site_images_model->save($data);
            }

            if(!empty($this->input->post('RedirectLink')))
            {
                $RedirectLinks = $this->input->post('RedirectLink');

                foreach($RedirectLinks as $SiteImageID => $Link)
                {
                    $update_redirect_link = array();
                    $update_by = array();

                    $update_redirect_link['RedirectLink'] = $Link;
                    $update_by['SiteImageID'] = $SiteImageID;

                    $this->Site_images_model->update($update_redirect_link, $update_by);
                }
            }

                
            unset($post_data['form_type']);
            $save_parent_data                   = array();
            $save_child_data                    = array();
                    
            $save_parent_data['UpdatedAt']      = date('Y-m-d H:i:s');     
            $save_parent_data['UpdatedBy']      = $this->session->userdata['admin']['UserID'];
                    
            $update_by  = array();
            $update_by[$this->data['TableKey']]  = base64_decode($post_data[$this->data['TableKey']]);
                    
                    
            $this->$parent->update($save_parent_data,$update_by);
                    
                
        
              $success['error']   = false;
              $success['success'] = lang('update_successfully');
              $success['redirect'] = true;
              $success['url'] = 'cms/'.$this->router->fetch_class().'/?tab='.$post_data['tab'];
        
              echo json_encode($success);
              exit;  
        
        }else{
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
            
        }
    }
    

}