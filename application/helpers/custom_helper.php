<?php
if (!defined('BASEPATH'))
 exit('No direct script access allowed');


    function currentDate()
	{
		return date('Y-m-d H:i:s');
	}

	
   function print_rm($data)
	{
            echo '<pre>';
            print_r($data);exit;
	}

	function dump($data)
    {
        echo '<pre>';
        print_r($data);exit;
    }
	

    function checkRightAccess($module_id,$role_id,$can){
        $CI = & get_Instance();
        $CI->load->model('Module_rights_model');
        
        $fetch_by = array();
        $fetch_by['ModuleID'] = $module_id;
        $fetch_by['RoleID']   = $role_id;
        $fetch_by[$can]       = 1;
        
        $result = $CI->Module_rights_model->getWithMultipleFields($fetch_by);
        if($result){
            return true;
        }else{
            return false;
        }
    }
    
    
    function NullToEmpty($data)
    {
        $returnArr = array();
        if (isset($data[0])) // checking if array is a multi-dimensional one, if so then checking for each row
        {
            $i = 0;
            foreach ($data as $row)
            {
                foreach ($row as $key => $value) {
                    if (null === $value) {
                        $returnArr[$i][$key] = "";
                    }else{
                        $returnArr[$i][$key] = $value;
                    }
                }
                $i++;
            }
        }else{
            foreach ($data as $key => $value) {
                if (null === $value) {
                    $returnArr[$key] = "";
                }else{
                    $returnArr[$key] = $value;
                }
            }
        }
        return $returnArr;
    }

    function checkUserRightAccess($module_id,$user_id,$can){
        $CI = & get_Instance();
        $CI->load->model('Modules_users_rights_model');
        
        $fetch_by = array();
        $fetch_by['ModuleID'] = $module_id;
        $fetch_by['UserID']   = $user_id;
        $fetch_by[$can]       = 1;
        
        $result = $CI->Modules_users_rights_model->getWithMultipleFields($fetch_by);
        if($result){
            return true;
        }else{
            return false;
        }
    }
	
   function getSystemLanguages(){
       $CI = & get_Instance();
       $CI->load->model('System_language_model');
       $languages = $CI->System_language_model->getAllLanguages();
       return $languages;
   }
	function canWriteReview($user_id, $product_id){
        $CI = & get_Instance();
        $query = $CI->db->select('rating', 'Rating');
        $query = $CI->db->where("UserID", $user_id);
        $query = $CI->db->where("ProductID", $product_id);
        $query = $CI->db->get('reviews');
        $result = $query->result_array();
        return (count($result) > 0 ) ? false : true;
    }
   
    function getDefaultLanguage()
   {
        $CI = & get_Instance();
        $CI->load->Model('System_language_model');
        $fetch_by = array();
        $fetch_by['IsDefault'] = 1;
        $result = $CI->System_language_model->getWithMultipleFields($fetch_by);
        return $result;
   }
   
	function getProductAvgRating($product_id){
        $CI = & get_Instance();
       $query = $CI->db->select_avg('rating', 'Rating');
       $query = $CI->db->where("ProductID", $product_id);
       $query = $CI->db->get('reviews');
       $result = $query->row();
      
       return round($result->Rating);
    }
   
   function getAllActiveModules($role_id,$system_language_id,$where){
        $CI = & get_Instance();
        $CI->load->Model('Module_rights_model');
        $result = $CI->Module_rights_model->getModulesWithRights($role_id,$system_language_id,$where);
        return $result;
   }
   
   
   function getActiveUserModule($user_id,$system_language_id,$where){
        $CI = & get_Instance();
        $CI->load->Model('Modules_users_rights_model');
        $result = $CI->Modules_users_rights_model->getModulesWithRights($user_id,$system_language_id,$where,true);
        return $result;
   }
   
   function getSiteImages($FileID, $ImageType)
   {
        $CI = & get_Instance();
        $CI->load->Model('Site_images_model');
        $fetch_by = array();
        $fetch_by['FileID'] = $FileID;
        $fetch_by['ImageType'] = $ImageType;
        $result = $CI->Site_images_model->getMultipleRows($fetch_by, true);
        return $result;
   }
   
   function getAllSizes()
   {
        $CI = & get_Instance();
        $CI->load->Model('Size_model');
        $default_lang = getDefaultLanguage();
        $result = $CI->Size_model->getAllJoinedData(false, 'SizeID',$default_lang->ShortCode, 'sizes.IsActive=1');
        return $result;
   }
   
   function getColorDetail($ColourID)
   {
        $CI = & get_Instance();
        $CI->load->Model('Colour_text_model');
        $fetch_by = array();
        $fetch_by['ColourID'] = $ColourID;
        $result = $CI->Colour_text_model->getWithMultipleFields($fetch_by, true);
        return $result;
   }

   
   function getSizeDetail($SizeID)
   {
        $CI = & get_Instance();
        $CI->load->Model('Size_text_model');
        $fetch_by = array();
        $fetch_by['SizeID'] = $SizeID;
        $result = $CI->Size_text_model->getWithMultipleFields($fetch_by, true);
        return $result;
   }

   
   function getTotalProduct($UserID)
   {
        $CI = & get_Instance();
        $CI->load->Model('Order_model');
        $fetch_by = array();
        $result = $CI->Order_model->getTotalProduct($UserID);
        return $result;
   }


   function getRemainingProductQuantity($ProductID)
   {
        $CI = & get_Instance();
        $CI->load->Model('Order_model');
        $CI->load->Model('Product_model');
        $fetch_by = array();
        $product = $CI->Product_model->getWithMultipleFields(array('ProductID'=>$ProductID));

        $InStock = $product->InStock;

        $soldPieces = $CI->Order_model->getSoldProductQuantity($ProductID);

        $result = $InStock - $soldPieces;
        
        return $result;
   }
   
   function getUserCartDetail($UserID)
   {
        $CI = & get_Instance();
        $CI->load->Model('Order_model');
        $fetch_by = array();
        $result = $CI->Order_model->getUserCartDetail($UserID);
        return $result;
   }

   function checkUserProductCart($UserID,$product_id)
   {
        $CI = & get_Instance();
        $CI->load->Model('Order_model');
        $fetch_by = array();
        $result = $CI->Order_model->checkUserProductCart($UserID, $product_id);
        return $result;
   }

   function RandomString()
  {
      $characters = '0123456789012345678901234567890123456789012345678912';
      $randstring = '';
      for ($i = 0; $i < 4; $i++) {
          $randstring .= $characters[rand(0, 50)];
      }
      return $randstring;
  }
	
   function checkAdminSession()
   {
	   $CI = & get_Instance();
	   if($CI->session->userdata('admin'))
	   {
		   return true;
		   
	   }else
	   {
		   redirect($CI->config->item('base_url'));
	   }
   }

  function siteSetting() {

      $CI = & get_Instance();
      $CI->load->Model('Site_setting_model');
      
      return $CI->Site_setting_model->get(1, false, 'SiteSettingID');
  }


   function getActiveCurrencies(){
        $CI = & get_Instance();
        $CI->load->Model('DollerSystem_model');
        $default_lang = getDefaultLanguage();
        $result = $CI->DollerSystem_model->getAllJoinedData(false, 'DollerSystemID',$default_lang->ShortCode, 'dollersystems.IsActive=1');
        return $result;
   }


   function getSelectedCurrencies($DollerSystemID='5',$Amount, $getWithSign = true){
        $CI = & get_Instance();
        $CI->load->Model('DollerSystem_model');
        $default_lang = getDefaultLanguage();
        $result = $CI->DollerSystem_model->getJoinedData(false, 'DollerSystemID', 'dollersystems.DollerSystemID='.$DollerSystemID);
        if($getWithSign)
        {
          $CurrentPrice = $result[0]->Sign.''.number_format(($result[0]->Price*$Amount),2);
        }
        else {
          $CurrentPrice = $result[0]->Price*$Amount;
        }
        return $CurrentPrice;
   }

   function getCurrencyCode($DollerSystemID='5'){
        $CI = & get_Instance();
        $CI->load->Model('DollerSystem_model');
        $default_lang = getDefaultLanguage();
        $result = $CI->DollerSystem_model->getJoinedData(false, 'DollerSystemID', 'dollersystems.DollerSystemID='.$DollerSystemID);
        return $result[0]->Code;
   }

  function checkWishlist($product_id) {

      $CI = & get_Instance();
      $CI->load->Model('Wishlist_model');
      $wish_list_data = array();
      $wish_list_data['ProductID']  = $product_id;
      $wish_list_data['UserID']     = $CI->session->userdata['admin']['UserID'];
      
       $already = $CI->Wishlist_model->getMultipleRows($wish_list_data);
       if($already){
        return true;
       }else{
        return false;
       }
  }

   function emailTemplate($data = ''){
    $site_setting = siteSetting();
   // print_rm($site_setting);
      return '<!DOCTYPE html>
<html>
   <head>
      <title></title>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   </head>
   <body>
      <table width="540" style="font-family: arial; border: solid 1px #ccc;">
         <thead>
            <tr>
               <td colspan="3" style="background:#8c8c8c; height: 10px;"></td>
            </tr>
            <tr>
               <td style="text-align: left;">
                  <img  src="'.base_url("assets/frontend/images").'/logo.png" width="130">
               </td>
               <td style="text-align: right;">
                  <i class="fa fa-phone" style="color: #e2197c;"></i> <a style="color:#000; text-decoration: none;" href="tel:+12 345 678 901">'.$site_setting->PhoneNumber.'</a>
                  <br>
                  <i class="fa fa-envelope" style="color: #e2197c;"></i> <a  style="color:#000; text-decoration: none;" href="mailto:'.$site_setting->Email.'">'.$site_setting->Email.'</a>
               </td>
            </tr>
            
         </thead>
         <tbody>

           '.$data.' 


            <tr style="font-size: 14px;"><td height="15" colspan="3"></td></tr>
         </tbody>
         <tfoot>
            <tr style="background:#ddd;">
               <td colspan="3" style="text-align:center;">
                  <img src="'.base_url("assets/frontend/images").'/logo.png" width="120">
               </td>
            </tr>
            <tr>
               <td colspan="3" height="30" style="text-align: center; font-size: 12px;">
                  '.$site_setting->Email.'| '.$site_setting->PhoneNumber.'
               </td>
            </tr>
            <tr bgcolor="#585858">
               <td colspan="3" height="30" style="text-align: center; color: #fff; font-size: 14px;">© '.date("Y").' All Copy Rights Reserved Leggings Pro</td>
            </tr>
         </tfoot>
      </table>
   </body>
</html>';
   }
   
   
   function sendEmail($data= array())
	{
	
	
		$CI = & get_Instance();
		$CI->load->library('email');
		$CI->email->from($data['from']);
		$CI->email->to($data['to']);
		$CI->email->subject($data['subject']);
		$CI->email->message($data['body']);
		$CI->email->set_mailtype('html');
		
       if($CI->email->send()){
			return true;

		}else
		{
			return false;
		}
		
		

	}

  function getMenuData($MenuPosition)
  {

    $CI = & get_Instance();
    $CI->load->Model('Menu_model');
    $result = $CI->Menu_model->getJoinedData(false,'MenuID','menus.MenuPosition='.$MenuPosition,'DESC','');

    return json_decode($result[0]->Menu);

  }
if (!function_exists('dateformat')) {

    function dateformat ($date,$type = NULL) {

            if ($date == '0000-00-00 00:00:00'){                

             return na(NULL);

            }else{

             $datetime = strtotime($date);
            
             if($type == 'full'){

            return date("d-M-Y H:i A",$datetime);

            }else if ($type == 'shortdate'){

            return date("d-M-y", $datetime);

            }else if ($type == 'datenumeric'){

            return date("d-m-y", $datetime);

            }
            else if ($type == 'date'){

            return date("d-M-Y", $datetime);

            }
            else if ($type == 'slashdate'){

            return date("d/M/Y", $datetime);

            }   
            else if ($type == 'datedesc'){

            return date("Y-m-d", $datetime);

            }       
            else if ($type == 'inbox'){

            return date("M d", $datetime);

            }
            else if ($type == 'time'){

            return date("g:i A", $datetime);            

            }else if($type == "order"){
                
            return date("d-M-Y H:i A", $datetime);
                
            }else if($type == "de"){
            
            return date("d.m.Y", $datetime);
            
            }
            
          }
  }

}
/**
   * getChecked()
   */
if (!function_exists('getChecked')) {
  function getChecked($row, $status)
  {
      if ($row == $status) {
          return "checked=\"checked\"";
      }
  }
}
  /**
   * getArrayChecked()
   */
if (!function_exists('getArrayChecked')) {
  function getArrayChecked(array $array, $status)
  {
      if(!empty($array)){
      if (in_array($status,$array)) {
          return "checked=\"checked\"";
      }
    }
  }
}
  /**
   * getSelected()
   */
if (!function_exists('getSelected')) {
  function getSelected($row, $status)
  {
      if ($row == $status) {
          return 'selected="selected"';
      }
  }
}
/**

* getArraySelected()

*/
if (!function_exists('getArraySelected')) {
    function getArraySelected(array $array, $status)
    {
        if(!empty($array)){
        if (in_array($status, $array)) {
            return 'selected="selected"';
        }
      }
    }
}

if (!function_exists('getGenderTitle')) {

function getGenderTitle($id = null) {
    $days = array(
                 1 => "Mr",
                 2 => "Mrs"
                 );
    if($id){
     return $days[$id];
    }else{
     return $days;
      }
    }
}
/**
   * cleanOut()
   */
   
  function cleanOut($text) {
     $text =  strtr($text, array('\r\n' => "", '\r' => "", '\n' => ""));
     $text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
     $text = str_replace('<br>', '<br />', $text);
     return stripslashes($text);
  }
if (!function_exists('CountryList')) {

function CountryList($id = null) {
    
    
       $CI = & get_Instance();
           $default_lang = getDefaultLanguage();
           $CI->db->select('a.CountryID,a.CountryCode,b.Title');
           $CI->db->from('countries AS a');
           $CI->db->join('countries_text AS b','a.CountryID = b.CountryID' );           
           $CI->db->where('a.Hide',0);
           $CI->db->where('a.IsActive',1);
            if($id){
                $CI->db->where('a.CountryID',$id);
            }
           $CI->db->where('b.SystemLanguageID',$default_lang->SystemLanguageID); 
           $result = $CI->db->get();
        if ($result->num_rows() > 0) {
            if($id){
            return $result->row();
            }else{
                return $result->result();
            }
        }else
        {
            return false;
        }   
    }
}
if (!function_exists('GetState')) {

function GetState($id = null) {
    
    
       $CI = & get_Instance();
           $default_lang = getDefaultLanguage();
           $CI->db->select('a.StateID,b.Title');
           $CI->db->from('states AS a');
           $CI->db->join('states_text AS b','a.StateID = b.StateID' );           
           $CI->db->where('a.Hide',0);
           $CI->db->where('a.IsActive',1);
            if($id){
                $CI->db->where('a.StateID',$id);
            }
           $CI->db->where('b.SystemLanguageID',$default_lang->SystemLanguageID); 
           $result = $CI->db->get();
        if ($result->num_rows() > 0) {
            if($id){
            return $result->row();
            }else{
                return $result->result();
            }
        }else
        {
            return false;
        }   
    }
}
if (!function_exists('GetCity')) {

function GetCity($id = null) {
    
    
       $CI = & get_Instance();
           $default_lang = getDefaultLanguage();
           $CI->db->select('a.CityID,b.Title');
           $CI->db->from('cities AS a');
           $CI->db->join('cities_text AS b','a.CityID = b.CityID' );           
           $CI->db->where('a.Hide',0);
           $CI->db->where('a.IsActive',1);
            if($id){
                $CI->db->where('a.CityID',$id);
            }
           $CI->db->where('b.SystemLanguageID',$default_lang->SystemLanguageID); 
           $result = $CI->db->get();
        if ($result->num_rows() > 0) {
            if($id){
            return $result->row();
            }else{
                return $result->result();
            }
        }else
        {
            return false;
        }   
    }
}
if (!function_exists('StatetList')) {

function StatetList($id = null) {
      $ch = curl_init();
 
    // Now set some options (most are optional)
 
    // Set URL to download
    curl_setopt($ch, CURLOPT_URL, base_url('State.json'));
 
    // Include header in result? (0 = yes, 1 = no)
    curl_setopt($ch, CURLOPT_HEADER, 0);
 
    // Should cURL return or print out the data? (true = return, false = print)
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
    // Timeout in seconds
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
 
    // Download the given URL, and return output
    $output = curl_exec($ch);
 
    // Close the cURL resource, and free system resources
    curl_close($ch);
     $arr = json_decode($output);

    return isset($arr->$id)?json_encode($arr->$id):$arr;

    }
}
if (!function_exists('getFeature')) {

function getFeature()
    {
    
           $CI = & get_Instance();
           $default_lang = getDefaultLanguage();
           $CI->db->select('a.FeatureID,b.Title');
           $CI->db->from('features AS a');
           $CI->db->join('features_text AS b','a.FeatureID = b.FeatureID' );           
           $CI->db->where('a.Hide',0);
           $CI->db->where('a.IsActive',1);
           $CI->db->where('b.SystemLanguageID',$default_lang->SystemLanguageID); 
           $result = $CI->db->get();
        if ($result->num_rows() > 0) {
            return $result->result();
        }else
        {
            return false;
        }
    }
}
if (!function_exists('getPages')) {

function getPages()
    {
    
           $CI = & get_Instance();
           $default_lang = getDefaultLanguage();
           $CI->db->select('a.PageID,b.Title');
           $CI->db->from('pages AS a');
           $CI->db->join('pages_text AS b','a.PageID = b.PageID' );           
           $CI->db->where('a.Hide',0);
           $CI->db->where('a.IsActive',1);
           $CI->db->where('b.SystemLanguageID',$default_lang->SystemLanguageID); 
           $result = $CI->db->get();
        if ($result->num_rows() > 0) {
            return $result->result();
        }else
        {
            return false;
        }
    }
}
if (!function_exists('getCategory')) {

function getCategory($cat = "")
    {
    
           $CI = & get_Instance();
           $default_lang = getDefaultLanguage();
           $CI->db->select('b.CategoryTextID,b.Title,a.ParentID');
           $CI->db->from('categories AS a');
           $CI->db->join('categories_text AS b','a.CategoryID = b.CategoryID' );           
           $CI->db->where('a.Hide',0);
           $CI->db->where('a.IsActive',1);
           $CI->db->where('b.SystemLanguageID',$default_lang->SystemLanguageID);
           if($cat){
            $CI->db->where('b.CategoryTextID !=',$cat);   
           }
           $CI->db->order_by('b.CategoryTextID','ASC'); 
           $result = $CI->db->get();
        if ($result->num_rows() > 0) {
            return $result->result();
        }else
        {
            return false;
        }
    }
}
if (!function_exists('getSubCategory')) {

function getSubCategory($cat = "")
    {
    
           $CI = & get_Instance();
           $default_lang = getDefaultLanguage();
           $CI->db->select('b.CategoryTextID,b.Title,a.ParentID');
           $CI->db->from('categories AS a');
           $CI->db->join('categories_text AS b','a.CategoryID = b.CategoryID' );           
           $CI->db->where('a.Hide',0);
           $CI->db->where('a.IsActive',1);
           $CI->db->where('b.SystemLanguageID',$default_lang->SystemLanguageID);
           if($cat){
            $CI->db->where('a.ParentID =',$cat);   
           }
           $CI->db->order_by('b.CategoryID','ASC'); 
           $result = $CI->db->get();
        return $result->num_rows();
    }
}
if (!function_exists('getCategoryName')) {

function getCategoryName($id)
    {
    
           $CI = & get_Instance();
           $default_lang = getDefaultLanguage();
           $CI->db->select('a.CategoryID,b.CategoryTextID,b.Title');
           $CI->db->from('categories AS a');
           $CI->db->join('categories_text AS b','a.CategoryID = b.CategoryID' );           
           $CI->db->where('a.Hide',0);
           $CI->db->where('a.IsActive',1);
           $CI->db->where('b.CategoryTextID',$id);
           $CI->db->where('b.SystemLanguageID',$default_lang->SystemLanguageID); 
           $result = $CI->db->get();
        if ($result->num_rows() > 0) {
            return $result->row();
        }else
        {
            return false;
        }
    }
}
if (!function_exists('getCategoryProduct')) {

    function getCategoryProduct($CategoryID)
    {
    
           $CI = & get_Instance();
           $default_lang = getDefaultLanguage();
           $CI->db->select('*');
           $CI->db->from('products AS a');
           $CI->db->join('products_text AS b','a.ProductID = b.ProductID' );           
           $CI->db->where('a.Hide',0);
           $CI->db->where('a.IsActive',1);
           $CI->db->where('a.CategoryID',$CategoryID);
           $CI->db->where('b.SystemLanguageID',$default_lang->SystemLanguageID); 
           $result = $CI->db->get();
           //echo $CI->db->last_query();
           //exit();
        if ($result->num_rows() > 0) {
            return $result->result();
        }else
        {
            return false;
        }
    }
}
if (!function_exists('YearList')) {

function YearList()
    { 
        $arr = array();
        $year = range(1901,date('Y'));
    
         foreach($year as $val){
             $arr[$val]= $val;
         }
        return $arr;
    }
}
if (!function_exists('MonthList')) {

function MonthList()
    { 
        $arr = array();
        $year = range(1,12);
    
         foreach($year as $val){
             $arr[$val]= $val;
         }
        return $arr;
    }
}
if (!function_exists('MenuPosition')) {

function MenuPosition()
    { 
        $arr = array('1' => lang('menu_header'),'2' => lang('menu_footer'),'3' => lang('menu_left'),'4' => lang('menu_right'));
        return $arr;
    }
}
if (!function_exists('get_page_by_id')) {

    function get_page_by_id($id)
                     {
        
           $CI = & get_Instance();
           $default_lang = getDefaultLanguage();
           $CI->db->select('a.PageID,b.Title,a.Url');
           $CI->db->from('pages AS a');
           $CI->db->join('pages_text AS b','a.PageID = b.PageID' );           
           $CI->db->where('a.Hide',0);
           $CI->db->where('a.IsActive',1);
           $CI->db->where('a.PageID',$id);
           $CI->db->where('b.SystemLanguageID',$default_lang->SystemLanguageID); 
           $result = $CI->db->get();
        if ($result->num_rows() > 0) {
            return $result->row();
        }else
        {
            return false;
        }
    }
}
if (!function_exists('AdminMenu')) {
    function AdminMenu(array $menu, $id, $menutype ,$level = 0)
    {
      $html = '';
      if(count(has_child($menu,$id))<=0 && $level==0)
      {

          if($menutype == 1){
              $page = get_page_by_id($id);
          }else{
              $page = getCategoryName($id);
          }
          $html .=  '<li class="dd-item" data-id="'.$id.'" data-menu-type="'.$menutype.'"><span class="menu-remove pull-right">x</span>
                      <div class="dd-handle">'.$page->Title.'</div>
                  </li>';
      }

      else if(count(has_child($menu,$id))<=0 && $level>0)
      {
    
          if($menutype == 1){
              $page = get_page_by_id($id);
          }else{
              $page = getCategoryName($id);
          }

          $html .=  '<li class="dd-item" data-id="'.$id.'" data-menu-type="'.$menutype.'"><span class="menu-remove pull-right">x</span>

                      <div class="dd-handle">'.$page->Title.'</div>

                  </li>';
      }

      else if(count(has_child($menu,$id))>0)
      {     

          $childs = has_child($menu,$id);
          $flag = 0;
          foreach ($childs as $child)
          {
              if($menutype == 1){
                  $page = get_page_by_id($id);
              }else{
                  $page = getCategoryName($id);
              }

              if($flag==0)
              {

                  $html .= '<li class="dd-item" data-id="'.$id.'" data-menu-type="'.$menutype.'"><span class="menu-remove pull-right">x</span>
                              <div class="dd-handle">'.$page->Title.'</div>
                                  <ol class="dd-list">';     
                  $flag = 1;                      
                  $level++;
              }
             $html .= AdminMenu($menu,$child['id'],$child['menutype'],$level);
          }
             $html .= '</ol></li>';
      }

      return   $html;

    }
}
if (!function_exists('HeaderMenu')) {
    function HeaderMenu(array $menu, $id, $menutype ,$level = 0)
    {
      $html = '';
      if(count(has_child($menu,$id))<=0 && $level==0)
      {

          if($menutype == 1){
              $page = get_page_by_id($id);
              $url = $page->Url;
          }else{
              $page = getCategoryName($id);
              $url = 'category/detail/'.$page->CategoryID;
          }
          $html .=  '<li class="nav-item">
                        <a class="nav-link" href="'.base_url($url).'">'.strtoupper($page->Title).'</a>
                    </li>';
      }

      else if(count(has_child($menu,$id))<=0 && $level>0)
      {
    
          if($menutype == 1){
              $page = get_page_by_id($id);
              $url = $page->Url;
          }else{
              $page = getCategoryName($id);
              $url = 'category/detail/'.$page->CategoryID;
          }

          $html .=  '<li class="nav-item">
                        <a class="nav-link" href="'.base_url($url).'">'.strtoupper($page->Title).'</a>
                    </li>';
      }

      else if(count(has_child($menu,$id))>0 && $menutype == 2)
      {   
          $childs = has_child($menu,$id);
          $flag = 0;
          $carouselMenu = '';
          $products = getCategoryProduct($id);

          if($products){
            $carouselMenu = '<li class="col-sm-3">
                <ul>
                    <li class="dropdown-header">New in Stores</li>
                    <div id="menuCarousel-'.$id.'" class="carousel slide megaCarousel" data-ride="carousel">
                        <div class="carousel-inner">';
                          foreach($products as $key => $product)
                          {
                            $Price = (isset($CI->session->userdata['admin']) && $CI->session->userdata['admin']['RoleID'] == 4 ? $product->PriceForWholeSaler : $product->Price);

                            if($product->Discount > 0)
                            {
                                $Discount = $Price * ($product->Discount/100);

                                $Price = $Price - $Discount;
                            }

                            $product_images = getSiteImages($product->ProductID, 'ProductImage');
                            $carouselMenu .= '<div class="item '.($key == 0 ? 'active':'').'">
                                <a href="#"><img src="'.(file_exists($product_images[0]['ImageName']) ? base_url() . $product_images[0]['ImageName'] : base_url('uploads/no_image.png')).'" class="img-responsive" alt="product 1"></a>
                                <h4><small>'.$product->Title.'</small></h4>
                                <button class="btn btn-success" type="button">$'.number_format($Price,2).'</button>
                            </div>';
                          }
                        $carouselMenu .= '</div>
                        <a class="carousel-arrow left" href="#menuCarousel-'.$id.'" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
                        <a class="carousel-arrow right" href="#menuCarousel-'.$id.'" data-slide="next"><i class="fa fa-chevron-right"></i></a>
                        <!-- End Carousel Inner -->
                    </div>
                </ul>
            </li>';
          }
          foreach ($childs as $key=>$child)
          {
              if($menutype == 1){
                  $page = get_page_by_id($id);
              }else{
                  $page = getCategoryName($id);
              }

              if($flag==0)
              {

                  $html .= '<li class="nav-item dropdown mega-dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">'.strtoupper($page->Title).' <span class="fa fa-chevron-down pull-right"></span></a>
                                  <ul class="dropdown-menu mega-dropdown-menu row">';     
                  $flag = 1;                      
                  $level++;
              }
              if($key == 0)
              {
                $html .= $carouselMenu;
              }
              $html .= HeaderMenuChild($menu,$id,$child['id'],$child['menutype'],$level);
          }
             $html .= '</ul></li>';
      }

      return   $html;

    }
}

if (!function_exists('HeaderMenuChild')) {
    function HeaderMenuChild(array $menu,$category_id , $id, $menutype ,$level = 0)
    {

       $CI = & get_Instance();
      $html = '';

      if(count(has_child($menu,$id))<=0 && $level==0)
      {

          if($menutype == 1){
              $page = get_page_by_id($id);
              $url = $page->Url;
          }else{
              $page = getCategoryName($id);
              $url = 'products/index/'.$id;
          }
          $html .=  '<li class="col-sm-3">
                                <ul>
                                    <li class="dropdown-header"><a href="'.base_url($url).'">'.strtoupper($page->Title).'</a></li>
                                </ul>
                            </li>';
      }

      else if(count(has_child($menu,$id))<=0 && $level>0)
      {
    
          if($menutype == 1){
              $page = get_page_by_id($id);
              $url = $page->Url;
          }else{
              $page = getCategoryName($id);
              $url = 'products/index/'.$id;
          }

          $html .=  '<li class="col-sm-3">
                          <ul>
                              <li class="dropdown-header"><a href="'.base_url($url).'">'.strtoupper($page->Title).'</a></li>
                          </ul>
                      </li>';
      }

      else if(count(has_child($menu,$id))>0)
      {     

          $childs = has_child($menu,$id);
          $flag = 0;
          foreach ($childs as $child)
          {
              if($menutype == 1){
                  $page = get_page_by_id($id);
                  $url = $page->Url;
              }else{
                  $page = getCategoryName($id);
                  $url = 'category/detail/'.$page->CategoryID;
              }

              if($flag==0)
              {

                  $html .= '<li class="col-sm-3">
                          <ul>
                              <li class="dropdown-header"><a href="'.base_url($url).'">'.strtoupper($page->Title).'</a></li>';     
                  $flag = 1;                      
                  $level++;
              }
             $html .= HeaderMenuGrandChild($menu,$id,$child['id'],$child['menutype'],$level);
          }
             $html .= '</ul></li>';
      }

      return   $html;

    }
}



if (!function_exists('HeaderMenuGrandChild')) {
    function HeaderMenuGrandChild(array $menu,$category_id , $id, $menutype ,$level = 0)
    {

       $CI = & get_Instance();
      $html = '';
      $childs = has_child($menu,$category_id);
      //print_rm($childs);
      $flag = 0;
      foreach ($childs as $child)
      {
          if($menutype == 1){
              $page = get_page_by_id($id);
              $url = $page->Url;
          }else{
              $page = getCategoryName($id);
              $url = 'products/index/'.$id;
          }

          if($flag==0)
          {

              $html .= '<li><a href="'.base_url($url).'">'.strtoupper($page->Title).'</a></li>';     
              $flag = 1;
          }
      }

      return   $html;

    }
}
if (!function_exists('has_child')) {

    function has_child($menu,$id)
    {

        $child = array();
        $i = 0;
        foreach ($menu as $row) 
        {
            if($row->parent==$id)

            {
                $child[$i]['id'] = $row->id;
                $child[$i]['parent'] = $row->parent;
                $child[$i]['menutype'] = $row->menutype;
                $i++; 

            }

        }
        
        return $child;
    }
}
if(!function_exists('buildTree')){
function buildTree(array $data, $parent = 0,$sub_mark,$cat = '') {
     $option = array();
                foreach($data as $k){
                 if ($k->ParentID == $parent) { 
                $option[] = '<option value='.$k->CategoryTextID.' '.getSelected($cat,$k->CategoryTextID).'>'.$sub_mark.$k->Title;
                $option[] = buildTree($data,$k->CategoryTextID,$sub_mark.'--');
                $option[] = '</option>';     
                }
        }
                    return implode( "\r\n", $option);

    }
}
 
 
	