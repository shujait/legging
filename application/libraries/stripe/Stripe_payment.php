<?php
require_once __DIR__ . '/lib/Stripe.php';

class Stripe_payment
{
	public function __construct($params)
	{
		Stripe::setApiKey($params['secret_key']);
    }

    public function create_customer($CCInfo){
		try { 
			$customer = Stripe_Customer::create([
				'card'	=> array(
					'number'	=> $CCInfo['cardnumber'],
					'exp_month'	=> $CCInfo['exp_month'],
					'exp_year'	=> $CCInfo['exp_year'],
					'cvc'		=> $CCInfo['cvv'],
					'name'		=> $CCInfo['name']
				),
				'email'	=> $CCInfo['email']
			]);
			
			return $customer;
			
		} catch(Exception $e) { 
			return $e;
		}

	} // End create_customer

	public function create_customer_by_token($token){
		try { 
			// return Stripe_Customer::create(['source' => $token]);
			return Stripe_Customer::create([
				'email' => $token->email,
				'source' => $token->id
			]);
		} catch(Exception $e) { 
			return $e;
		}

	} // End create_customer

	public function single_payment($customer_id, $amount, $currency) {
		try {
			$charge = Stripe_Charge::create(array(
				'amount'		=> ($amount * 100),
				'currency'		=> $currency,
				'customer'		=> $customer_id
				)
			);
			
			return $charge;
			
		} catch(Exception $e) { 
			return $e;
		}
		
	} // End single_payment


	public function subscription($customer_id)
	{
		try{
			  

			  $subscription = Stripe_Subscription::create([
			    'customer' => $customer_id,
			    'items' => [['plan' => 'plan_EBUKZRf9kGZkjP']],
			  ]);

			  return $subscription; 
			}
			catch(Exception $e) { 
				return $e;
			}
	}


	public function getPlan($plan_id)
	{
		try{
			  

			  $plan_data = Stripe_Plan::retrieve($plan_id);

			  return $plan_data; 
			}
			catch(Exception $e) { 
				return $e;
			}
	}

	public function refund($charge_id) {
		try {
			$charge = Stripe_Charge::retrieve($charge_id);
			$refund = $charge->refunds->create();
			
			return $refund;
			
		} catch(Exception $e) {
			return $e;
		}
	} // End Refund
}

?>