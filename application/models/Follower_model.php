<?php
Class Follower_model extends Base_Model
{
	public function __construct()
	{
		parent::__construct("user_followers");
		
	}
	
	
	public function getFollowing($id)
	{
		$query = $this->db->query("Select users.*,users_text.FullName,users_text.Designation,users_text.Industry,users_text.City,users_text.Bio from user_followers JOIN users ON user_followers.Following = users.UserID JOIN users_text ON users.UserID = users_text.UserID JOIN system_languages ON system_languages.SystemLanguageID = users_text.SystemLanguageID where user_followers.Follower = ".$id." AND system_languages.ShortCode = 'EN'  ");
	   //echo $this->db->last_query();exit();
		if($query->num_rows() > 0)
		{
			
			return $query->result_array();
			 
		}else
		{
			return NULL;
		}
			
	}
	public function getTotalFollowing($id)
	{
		$query = $this->db->query("Select Count(Follower) as Total from user_followers where Follower = ".$id." ");
		if($query->num_rows() > 0)
		{
			$result =  $query->result_array();
			return $result[0]['Total'];
		}else
		{
			return NULL;
		}
			
	}
	
	public function getFollower($id)
	{
		$query = $this->db->query("Select users.*,users_text.FullName,users_text.Designation,users_text.Industry,users_text.City,users_text.Bio from user_followers JOIN users ON user_followers.Follower = users.UserID JOIN users_text ON users.UserID = users_text.UserID JOIN system_languages ON system_languages.SystemLanguageID = users_text.SystemLanguageID where user_followers.Following = ".$id." AND system_languages.ShortCode = 'EN' ");
		
		
		if($query->num_rows() > 0)
		{
			
			return   $query->result_array();
			 
		}else
		{
			
			return NULL;
		}
			
	}
	
	public function getTotalFollower($id)
	{
		$query = $this->db->query("Select Count(Following) as Total from user_followers where Following = ".$id." ");
		if($query->num_rows() > 0)
		{
			$result =  $query->result_array();
			return $result[0]['Total'];
		}else
		{
			
			return NULL;
		}
			
	}
	
		
}