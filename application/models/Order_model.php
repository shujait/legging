<?php

class Order_model extends CI_Model {

    public function __construct() {
// Call the CI_Model constructor
        parent::__construct();
    }

   public function alreadyAddToCart($user_id='',$product_id = ''){
            
            $query = "SELECT * from temp_orders WHERE user_id=$user_id AND product_id = $product_id";
            $result = $this->db->query($query);
            return $result->row_array();
    }
    
    
    public function update($table,$data, $search)
    {
        $this->db->update($table, $data, $search);
        $this->db->last_query();
    }
    
    
     public function save($table,$data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }
    
    public function getTotalProduct($user_id){
        $this->db->select('COUNT(product_id) as products_count, SUM(quantity) as total');
        $this->db->from('temp_orders');
        $this->db->where('user_id',$user_id);
        return $this->db->get()->result();
    }

    public function getSoldProductQuantity($ProductID){
        $this->db->select('*');
        $this->db->from('order_items');
        $this->db->where('product_id',$ProductID);
        $soldPieces = 0;
        foreach($this->db->get()->result() as $items)
        {
            if($items->TotalPieces > 0)
            {
                $soldPieces += $items->TotalPieces*$items->quantity;
            }
            else
            {
                $soldPieces += $items->quantity;
            }
        }

        return $soldPieces;
    }
    
    
    public function getUserCartDetail($user_id=''){
            
            $query = "SELECT temp_orders.*,products.*,products_text.* from temp_orders LEFT JOIN products ON temp_orders.product_id = products.ProductID LEFT JOIN products_text ON products_text.ProductID = products.ProductID WHERE temp_orders.user_id=$user_id";
            $result = $this->db->query($query);
            return $result->result_array();
    }

    public function checkUserProductCart($user_id='', $product_id=''){
            
            $query = "SELECT count(product_id) as CheckProd from temp_orders WHERE temp_orders.user_id=$user_id AND product_id=$product_id";
            $result = $this->db->query($query);
            return $result->row()->CheckProd;
    }
    
    public function delete($table,$deleted_by)
    {
        $this->db->delete($table, $deleted_by);
        

    }
    
    
    
    public function insert_batch($table,$data)
    {
            return $this->db->insert_batch($table, $data);
    }
    
    public function getAllUserOrder($user_id){
        
        $this->db->select('*');
        $this->db->from('orders');
        if($user_id != 0){
            $this->db->where('user_id',$user_id);
        }
        
        
        return $this->db->get()->result_array();
        
        
    }
    
    public function getOrdersDetail($order_id){
        
        $this->db->select('orders.*,order_items.*,products.*,products_text.*');
        $this->db->from('orders');
        
        $this->db->join('order_items','order_items.order_id = orders.order_id');
        $this->db->join('products','products.ProductID = order_items.product_id');
        $this->db->join('products_text','products_text.ProductID = products.ProductID');
        
        $this->db->where('orders.order_id',$order_id);
        
        
        $this->db->group_by('order_items.product_id');
        
        return $this->db->get()->result_array();
        
        
    }
    
    
    public function getAllDealerOrders($user_id){
        
        $this->db->select('orders.*,order_items.*,ads.name as ad_title,ads.image_1,ads.ad_price,d.FirstName As DealerFirstName,d.LastName,d.Email As DealerEmail,d.Telephone As DealerTelephone,u.FirstName As UserFirstName,u.LastName,u.Email As UserEmail,u.Telephone As UserTelephone');
        $this->db->from('orders');
        
        $this->db->join('order_items','order_items.order_id = orders.order_id');
        $this->db->join('ads','ads.id = order_items.product_id');
        $this->db->join('users d','d.ID = ads.user_id_fk');
        
        $this->db->join('users u','u.ID = orders.user_id');
        
        $this->db->where('ads.user_id_fk',$user_id);
        
        
        $this->db->group_by('orders.order_id');
        
        return $this->db->get()->result_array();
        
        
    }
    
    
    public function getAllOrderItems($id,$is_dealer = 0,$dealer_id = 0){
        
        $this->db->select('orders.*,order_items.*,ads.email as company_email,ads.name as company_name,ads.nature_of_bus as ad_title,ads.image_1,ads.ad_price,d.FirstName As DealerFirstName,d.LastName as DealerLastName,d.Email As DealerEmail,d.Telephone As DealerTelephone,u.FirstName As UserFirstName,u.LastName as UserLastName,u.Email As UserEmail,u.Telephone As UserTelephone');
        $this->db->from('orders');
        
        $this->db->join('order_items','order_items.order_id = orders.order_id');
        $this->db->join('ads','ads.id = order_items.product_id');
        $this->db->join('users d','d.ID = ads.user_id_fk');
        
        $this->db->join('users u','u.ID = orders.user_id');
        if($is_dealer == 1){
             $this->db->where('d.ID',$dealer_id);
        }
        $this->db->where('orders.order_id',$id);
        
        return $this->db->get()->result_array();
        
        
    }
}
