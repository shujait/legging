<?php
	Class Product_model extends Base_Model
	{
	    public function __construct()
	    {
	        parent::__construct("products");

	    }

	    public function getProductData($as_array=false,$join_field,$system_language_code = false,$where = false,$sort = 'ASC',$sort_field = 'SortOrder')
	    {

	            $this->db->select('products.*,  products_text.*, categories_text.Title as Category, sub_cat_text.Title as SubCategory,sub_cat_text_level_2.Title as SubCategoryLevel2');
	            $this->db->join('products_text','products.ProductID = products_text.ProductID' );
	            $this->db->join('categories_text','products.CategoryID = categories_text.CategoryID', 'LEFT' );
	            $this->db->join('categories_text as sub_cat_text','products.SubCategoryID = sub_cat_text.CategoryID', 'LEFT' );
	            $this->db->join('categories_text as sub_cat_text_level_2','products.SubCategoryIDLevel2 = sub_cat_text_level_2.CategoryID', 'LEFT' );
	            $this->db->join('system_languages','system_languages.SystemLanguageID = products_text.SystemLanguageID' );
	            
	            
	            
	            
	            
	            if($system_language_code) {
	                    $this->db->where('system_languages.ShortCode', $system_language_code);
	            }else
	            {
	                    $this->db->where('system_languages.IsDefault','1');
	            }
	            if($where)
	            {
	                    $this->db->where($where);
	            }
	            $this->db->where('products.Hide','0');
	            $this->db->order_by('products.'.$sort_field,$sort);
	            $result = $this->db->get('products');
	            //echo $this->db->last_query();exit();
	            if($as_array)
	            {
	                   
	                $data =  $result->result_array();
	            }else{
	                $data = $result->result();
	            }

	           

	            return $data;
	            
	    }
            
            
            public function getSliderImages()
	    {

	            $this->db->select('site_images.ImageName');
	            $this->db->from('products');
                    $this->db->join('site_images', 'site_images.FileID = products.ProductID');
	            $this->db->where('products.Hide','0');
                    $this->db->where('site_images.ImageType','ProductImage');
                    $this->db->group_by('site_images.FileID');
	            $this->db->limit(5);
	            $result = $this->db->get();
	            return $result->result_array();
	            
	    }


	    public function wishlist($where = false,$system_language_code = 'EN')
	    {

	            $this->db->select('products.*,  products_text.*,site_images.ImageName');
	            $this->db->join('products_text','products.ProductID = products_text.ProductID' );
	            $this->db->join('site_images', 'site_images.FileID = products.ProductID AND site_images.ImageType = "ProductImage"','left');
	            $this->db->join('wishlist','wishlist.ProductID = products.ProductID', 'LEFT' );

	           
	            $this->db->join('system_languages','system_languages.SystemLanguageID = products_text.SystemLanguageID' );
	            
	            
	            
	            
	            
	            if($system_language_code) {
	                    $this->db->where('system_languages.ShortCode', $system_language_code);
	            }else
	            {
	                    $this->db->where('system_languages.IsDefault','1');
	            }
	            if($where)
	            {
	                    $this->db->where($where);
	            }
	            $this->db->where('products.Hide','0');
	           
	            $result = $this->db->get('products');
	            //echo $this->db->last_query();exit();
	           return $result->result_array();
	           

	            
	            
	    }
            
	}