<?php
Class User_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("users");

    }

    
    /*
    
    public function getUserData($data,$system_language_code = false){
        
        $this->db->select('users.*,users_text.FullName');
        $this->db->from('users');
        $this->db->join('users_text','users.UserID = users_text.UserID');
        $this->db->join('system_languages','system_languages.SystemLanguageID = users_text.SystemLanguageID' );
        $this->db->where('users.Email',$data['Email']);
        $this->db->where('users.Password',$data['Password']);
         
        if($system_language_code) {
                    $this->db->where('system_languages.ShortCode', $system_language_code);
        }else
        {
                $this->db->where('system_languages.IsDefault','1');
        }
        return $this->db->get()->row_array();
       
        
        
    }
    */
    
    public function getUserData($data,$system_language_code = false){
        
        $this->db->select('users.*,users_text.FullName,roles.IsActive as RoleActivation');
        $this->db->from('users');
        $this->db->join('users_text','users.UserID = users_text.UserID');
        $this->db->join('roles','roles.RoleID = users.RoleID','Left');
        $this->db->join('system_languages','system_languages.SystemLanguageID = users_text.SystemLanguageID' );
        
        //$where = '(users.Email OR users.UName = "'.$data['Email'].'") AND users.Password = "'.$data['Password'].'"';
       // $this->db->where($where);
       $this->db->where('users.Email',$data['Email']);
       //$this->db->or_where('users.UName',$data['Email']);
       $this->db->where('users.Password',$data['Password']);
         
        if($system_language_code) {
                    $this->db->where('system_languages.ShortCode', $system_language_code);
        }else
        {
                $this->db->where('system_languages.IsDefault','1');
        }
        return $this->db->get()->row_array();
       
        
        
    }

    public function getUserEmailData($data,$system_language_code = false){
        
        $this->db->select('users.*,users_text.FullName,roles.IsActive as RoleActivation');
        $this->db->from('users');
        $this->db->join('users_text','users.UserID = users_text.UserID');
        $this->db->join('roles','roles.RoleID = users.RoleID','Left');
        $this->db->join('system_languages','system_languages.SystemLanguageID = users_text.SystemLanguageID' );
        
        //$where = '(users.Email OR users.UName = "'.$data['Email'].'") AND users.Password = "'.$data['Password'].'"';
       // $this->db->where($where);
       $this->db->where('users.Email',$data['Email']);
         
        if($system_language_code) {
                    $this->db->where('system_languages.ShortCode', $system_language_code);
        }else
        {
                $this->db->where('system_languages.IsDefault','1');
        }
        return $this->db->get()->row_array();
       
        
        
    }
    
    public function getAllUsers($as_array=false,$join_field,$system_language_code = false,$where = false,$sort = 'ASC',$sort_field = 'SortOrder')// please for now not usable function in text model 
    {

            $this->table = 'users';
            $first_field = $this->table.'.'.$join_field;
            $second_field = $this->table.'_text.'.$join_field;
            $this->db->select($this->table.'.*, ' . $this->table . '_text.*,roles_text.Title as RoleTitle');
            $this->db->join($this->table.'_text',$first_field.' = '.$second_field );

            $this->db->join('system_languages','system_languages.SystemLanguageID = '.$this->table.'_text.SystemLanguageID' );
            
            $this->db->join('roles','roles.RoleID = '.$this->table.'.RoleID' );
            $this->db->join('roles_text','roles.RoleID = roles_text.RoleID' );
            
            
            
            if($system_language_code) {
                    $this->db->where('system_languages.ShortCode', $system_language_code);
            }else
            {
                    $this->db->where('system_languages.IsDefault','1');
            }
            if($where)
            {
                    $this->db->where($where);
            }
            $this->db->where($this->table.'.Hide','0');
            $this->db->group_by('users.UserID');
            $this->db->order_by($this->table.'.'.$sort_field,$sort);
            $result = $this->db->get($this->table);
            //echo $this->db->last_query();exit();
            if($as_array)
            {
                   
                $data =  $result->result_array();
            }else{
                $data = $result->result();
            }

           

            return $data;
            
    }
}
?>