<?php 

    $slider_images = '';
    if (!empty($slider)) {

        $slider_images .= '<div class="form-group clearfix">
           <div class="col-sm-12 padding-left-0 padding-right-0">
              <div class="jFiler jFiler-theme-dragdropbox">
                 <div class="jFiler-items jFiler-row">
                    <ul class="jFiler-items-list jFiler-items-grid">';
                        foreach($slider as $img){
                       $slider_images .= '<li class="jFiler-item ArticleImage" data-jfiler-index="1" style="" id="img-'.$img['SiteImageID'].'">
                          <div class="jFiler-item-container">
                             <div class="jFiler-item-inner">
                                <div class="jFiler-item-thumb">
                                   <div class="jFiler-item-status"></div>
                                   <div class="jFiler-item-info"></div>
                                   <div class="jFiler-item-thumb-image"><img src="' . (file_exists($img['ImageName']) ? base_url() . $img['ImageName'] : base_url('uploads/no_image.png')) . '" draggable="false"></div>
                                </div>
                                <div class="jFiler-item-assets jFiler-row">
                                   <ul class="list-inline pull-left">
                                      <li><span class="jFiler-item-others"><i class="icon-jfi-file-image jfi-file-ext-jpg"></i></span></li>
                                   </ul>
                                   <ul class="list-inline pull-right">
                                      <li><a class="icon-jfi-trash jFiler-item-trash-action remove_image" data-image-id="'. $img['SiteImageID'] .'" data-image-path="'. $img['ImageName'] .'"></a></li>
                                   </ul>
                                </div>
                             </div>
                          </div>
                       </li>
                       <li class="jFiler-item ArticleImage SliderDescription" data-jfiler-index="1" style="" id="redirect-'.$img['SiteImageID'].'">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea class="form-control" name="Description['.$img['SiteImageID'].']" id="Description'.$img['SiteImageID'].'" style="height: 100px;">'.$img['ImageDescription'].'</textarea>
                                    </div>
                                </div>
                            </div>
                       </li>';
                         } 
                       
        $slider_images .= '</ul>
                     </div>
                  </div>
               </div>
            </div>';

        
    }

    $banner_after_slider_images = '';
    if (!empty($banner_after_slider)) {

        $banner_after_slider_images .= '<div class="form-group clearfix">
           <div class="col-sm-12 padding-left-0 padding-right-0">
              <div class="jFiler jFiler-theme-dragdropbox">
                 <div class="jFiler-items jFiler-row">
                    <ul class="jFiler-items-list jFiler-items-grid">';
                        foreach($banner_after_slider as $img){
                       $banner_after_slider_images .= '<li class="jFiler-item ArticleImage" data-jfiler-index="1" style="" id="img-'.$img['SiteImageID'].'">
                          <div class="jFiler-item-container">
                             <div class="jFiler-item-inner">
                                <div class="jFiler-item-thumb">
                                   <div class="jFiler-item-status"></div>
                                   <div class="jFiler-item-info"></div>
                                   <div class="jFiler-item-thumb-image"><img src="' . (file_exists($img['ImageName']) ? base_url() . $img['ImageName'] : base_url('uploads/no_image.png')) . '" draggable="false"></div>
                                </div>
                                <div class="jFiler-item-assets jFiler-row">
                                   <ul class="list-inline pull-left">
                                      <li><span class="jFiler-item-others"><i class="icon-jfi-file-image jfi-file-ext-jpg"></i></span></li>
                                   </ul>
                                   <ul class="list-inline pull-right">
                                      <li><a class="icon-jfi-trash jFiler-item-trash-action remove_image" data-image-id="'. $img['SiteImageID'] .'" data-image-path="'. $img['ImageName'] .'"></a></li>
                                   </ul>
                                </div>
                             </div>
                          </div>
                       </li>
                       <li class="jFiler-item ArticleImage ImageRedirectLink" data-jfiler-index="1" style="" id="redirect-'.$img['SiteImageID'].'">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title">Redirect Link</label>
                                        <input type="text" name="RedirectLink['.$img['SiteImageID'].']" required  class="form-control" id="RedirectLink'.$img['SiteImageID'].'" value="'.$img['RedirectLink'].'">
                                    </div>
                                </div>
                            </div>
                       </li>
                       <li class="jFiler-item ArticleImage" style="width:100%;" data-jfiler-index="1" style="" id="redirect-'.$img['SiteImageID'].'">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea class="form-control" name="Description['.$img['SiteImageID'].']" id="Description'.$img['SiteImageID'].'" style="height: 100px;">'.$img['ImageDescription'].'</textarea>
                                    </div>
                                </div>
                            </div>
                       </li> ';
                         } 
                       
        $banner_after_slider_images .= '</ul>
                     </div>
                  </div>
               </div>
            </div>';

        
    }

    $banner_after_featur_prod_images = '';
    if (!empty($banner_after_featur_prod)) {

        $banner_after_featur_prod_images .= '<div class="form-group clearfix">
           <div class="col-sm-12 padding-left-0 padding-right-0">
              <div class="jFiler jFiler-theme-dragdropbox">
                 <div class="jFiler-items jFiler-row">
                    <ul class="jFiler-items-list jFiler-items-grid">';
                        foreach($banner_after_featur_prod as $img){
                       $banner_after_featur_prod_images .= '<li class="jFiler-item ArticleImage" data-jfiler-index="1" style="" id="img-'.$img['SiteImageID'].'">
                          <div class="jFiler-item-container">
                             <div class="jFiler-item-inner">
                                <div class="jFiler-item-thumb">
                                   <div class="jFiler-item-status"></div>
                                   <div class="jFiler-item-info"></div>
                                   <div class="jFiler-item-thumb-image"><img src="' . (file_exists($img['ImageName']) ? base_url() . $img['ImageName'] : base_url('uploads/no_image.png')) . '" draggable="false"></div>
                                </div>
                                <div class="jFiler-item-assets jFiler-row">
                                   <ul class="list-inline pull-left">
                                      <li><span class="jFiler-item-others"><i class="icon-jfi-file-image jfi-file-ext-jpg"></i></span></li>
                                   </ul>
                                   <ul class="list-inline pull-right">
                                      <li><a class="icon-jfi-trash jFiler-item-trash-action remove_image" data-image-id="'. $img['SiteImageID'] .'" data-image-path="'. $img['ImageName'] .'"></a></li>
                                   </ul>
                                </div>
                             </div>
                          </div>
                       </li>
                       <li class="jFiler-item ArticleImage ImageRedirectLink" data-jfiler-index="1" style="" id="redirect-'.$img['SiteImageID'].'">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title">Redirect Link</label>
                                        <input type="text" name="RedirectLink['.$img['SiteImageID'].']" required  class="form-control" id="RedirectLink'.$img['SiteImageID'].'" value="'.$img['RedirectLink'].'">
                                    </div>
                                </div>
                            </div>
                       </li>
                       <li class="jFiler-item ArticleImage" style="width:100%;" data-jfiler-index="1" style="" id="redirect-'.$img['SiteImageID'].'">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea class="form-control" name="Description['.$img['SiteImageID'].']" id="Description'.$img['SiteImageID'].'" style="height: 100px;">'.$img['ImageDescription'].'</textarea>
                                    </div>
                                </div>
                            </div>
                       </li>';
                         } 
                       
        $banner_after_featur_prod_images .= '</ul>
                     </div>
                  </div>
               </div>
            </div>';

        
    }

    $banner_below_cat_images = '';
    if (!empty($banner_below_cat)) {

        $banner_below_cat_images .= '<div class="form-group clearfix">
           <div class="col-sm-12 padding-left-0 padding-right-0">
              <div class="jFiler jFiler-theme-dragdropbox">
                 <div class="jFiler-items jFiler-row">
                    <ul class="jFiler-items-list jFiler-items-grid">';
                        foreach($banner_below_cat as $img){
                       $banner_below_cat_images .= '<li class="jFiler-item ArticleImage" data-jfiler-index="1" style="" id="img-'.$img['SiteImageID'].'">
                          <div class="jFiler-item-container">
                             <div class="jFiler-item-inner">
                                <div class="jFiler-item-thumb">
                                   <div class="jFiler-item-status"></div>
                                   <div class="jFiler-item-info"></div>
                                   <div class="jFiler-item-thumb-image"><img src="' . (file_exists($img['ImageName']) ? base_url() . $img['ImageName'] : base_url('uploads/no_image.png')) . '" draggable="false"></div>
                                </div>
                                <div class="jFiler-item-assets jFiler-row">
                                   <ul class="list-inline pull-left">
                                      <li><span class="jFiler-item-others"><i class="icon-jfi-file-image jfi-file-ext-jpg"></i></span></li>
                                   </ul>
                                   <ul class="list-inline pull-right">
                                      <li><a class="icon-jfi-trash jFiler-item-trash-action remove_image" data-image-id="'. $img['SiteImageID'] .'" data-image-path="'. $img['ImageName'] .'"></a></li>
                                   </ul>
                                </div>
                             </div>
                          </div>
                       </li>
                       <li class="jFiler-item ArticleImage ImageRedirectLink" data-jfiler-index="1" style="" id="redirect-'.$img['SiteImageID'].'">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title">Redirect Link</label>
                                        <input type="text" name="RedirectLink['.$img['SiteImageID'].']" required  class="form-control" id="RedirectLink'.$img['SiteImageID'].'" value="'.$img['RedirectLink'].'">
                                    </div>
                                </div>
                            </div>
                       </li>
                       <li class="jFiler-item ArticleImage" style="width:100%;" data-jfiler-index="1" style="" id="redirect-'.$img['SiteImageID'].'">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea class="form-control" name="Description['.$img['SiteImageID'].']" id="Description'.$img['SiteImageID'].'" style="height: 100px;">'.$img['ImageDescription'].'</textarea>
                                    </div>
                                </div>
                            </div>
                       </li>';
                         } 
                       
        $banner_below_cat_images .= '</ul>
                     </div>
                  </div>
               </div>
            </div>';

        
    }
    
    $banner_above_cat_images = '';
    if (!empty($banner_above_cat)) {

        $banner_above_cat_images .= '<div class="form-group clearfix">
           <div class="col-sm-12 padding-left-0 padding-right-0">
              <div class="jFiler jFiler-theme-dragdropbox">
                 <div class="jFiler-items jFiler-row">
                    <ul class="jFiler-items-list jFiler-items-grid">';
                        foreach($banner_above_cat as $img){
                       $banner_above_cat_images .= '<li class="jFiler-item ArticleImage" data-jfiler-index="1" style="" id="img-'.$img['SiteImageID'].'">
                          <div class="jFiler-item-container">
                             <div class="jFiler-item-inner">
                                <div class="jFiler-item-thumb">
                                   <div class="jFiler-item-status"></div>
                                   <div class="jFiler-item-info"></div>
                                   <div class="jFiler-item-thumb-image"><img src="' . (file_exists($img['ImageName']) ? base_url() . $img['ImageName'] : base_url('uploads/no_image.png')) . '" draggable="false"></div>
                                </div>
                                <div class="jFiler-item-assets jFiler-row">
                                   <ul class="list-inline pull-left">
                                      <li><span class="jFiler-item-others"><i class="icon-jfi-file-image jfi-file-ext-jpg"></i></span></li>
                                   </ul>
                                   <ul class="list-inline pull-right">
                                      <li><a class="icon-jfi-trash jFiler-item-trash-action remove_image" data-image-id="'. $img['SiteImageID'] .'" data-image-path="'. $img['ImageName'] .'"></a></li>
                                   </ul>
                                </div>
                             </div>
                          </div>
                       </li>
                       <li class="jFiler-item ArticleImage ImageRedirectLink" data-jfiler-index="1" style="" id="redirect-'.$img['SiteImageID'].'">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title">Redirect Link</label>
                                        <input type="text" name="RedirectLink['.$img['SiteImageID'].']" required  class="form-control" id="RedirectLink'.$img['SiteImageID'].'" value="'.$img['RedirectLink'].'">
                                    </div>
                                </div>
                            </div>
                       </li>
                       <li class="jFiler-item ArticleImage" style="width:100%;" data-jfiler-index="1" style="" id="redirect-'.$img['SiteImageID'].'">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea class="form-control" name="Description['.$img['SiteImageID'].']" id="Description'.$img['SiteImageID'].'" style="height: 100px;">'.$img['ImageDescription'].'</textarea>
                                    </div>
                                </div>
                            </div>
                       </li>';
                         } 
                       
        $banner_above_cat_images .= '</ul>
                     </div>
                  </div>
               </div>
            </div>';

        
    }
    
    $home_page_cat_image = '';
    if (!empty($home_page_cat_images)) {

        $home_page_cat_image .= '<div class="form-group clearfix">
           <div class="col-sm-12 padding-left-0 padding-right-0">
              <div class="jFiler jFiler-theme-dragdropbox">
                 <div class="jFiler-items jFiler-row">
                    <ul class="jFiler-items-list jFiler-items-grid">';
                        foreach($home_page_cat_images as $img){
                       $home_page_cat_image .= '<li class="jFiler-item ArticleImage" data-jfiler-index="1" style="" id="img-'.$img['SiteImageID'].'">
                          <div class="jFiler-item-container">
                             <div class="jFiler-item-inner">
                                <div class="jFiler-item-thumb">
                                   <div class="jFiler-item-status"></div>
                                   <div class="jFiler-item-info"></div>
                                   <div class="jFiler-item-thumb-image"><img src="' . (file_exists($img['ImageName']) ? base_url() . $img['ImageName'] : base_url('uploads/no_image.png')) . '" draggable="false"></div>
                                </div>
                                <div class="jFiler-item-assets jFiler-row">
                                   <ul class="list-inline pull-left">
                                      <li><span class="jFiler-item-others"><i class="icon-jfi-file-image jfi-file-ext-jpg"></i></span></li>
                                   </ul>
                                   <ul class="list-inline pull-right">
                                      <li><a class="icon-jfi-trash jFiler-item-trash-action remove_image" data-image-id="'. $img['SiteImageID'] .'" data-image-path="'. $img['ImageName'] .'"></a></li>
                                   </ul>
                                </div>
                             </div>
                          </div>
                          
                            
                          
                       </li>
                       <li class="jFiler-item ArticleImage ImageRedirectLink" data-jfiler-index="1" style="" id="redirect-'.$img['SiteImageID'].'">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title">Redirect Link</label>
                                        <input type="text" name="RedirectLink['.$img['SiteImageID'].']" required  class="form-control" id="RedirectLink'.$img['SiteImageID'].'" value="'.$img['RedirectLink'].'">
                                    </div>
                                </div>
                            </div>
                       </li>
                       <li class="jFiler-item ArticleImage" style="width:100%;" data-jfiler-index="1" style="" id="redirect-'.$img['SiteImageID'].'">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea class="form-control" name="Description['.$img['SiteImageID'].']" id="Description'.$img['SiteImageID'].'" style="height: 100px;">'.$img['ImageDescription'].'</textarea>
                                    </div>
                                </div>
                            </div>
                       </li>
                       ';
                         } 
                       
        $home_page_cat_image .= '</ul>
                     </div>
                  </div>
               </div>
            </div>';

        
    }

    
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">home</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang($ControllerName);?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <ul class="nav nav-pills nav-pills-warning">
                            <li class="<?php echo ((isset($_GET['tab']) && $_GET['tab'] == 1) || !isset($_GET['tab']) ? 'active' : '')?>">
                                <a href="#pill1" data-toggle="tab">Slider and Banner Images</a>
                            </li>
                            <li class="<?php echo ((isset($_GET['tab']) && $_GET['tab'] == 2) ? 'active' : '')?>">
                                <a href="#pill2" data-toggle="tab">Home Page Category Settings</a>
                            </li>
                            <!-- <li>
                                <a href="#pill3" data-toggle="tab">Options</a>
                            </li> -->
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane <?php echo ((isset($_GET['tab']) && $_GET['tab'] == 1) || !isset($_GET['tab']) ? 'active' : '')?>" id="pill1">
                                <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                                    <input type="hidden" name="form_type" value="saveBannerImages">
                                    <input type="hidden" name="tab" value="1">
                                    <input type="hidden" name="HomeSettingsID" value="<?php echo base64_encode($result[0]->HomeSettingsID); ?>">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group m-b-20">
                                                <label>Slider Images</label><br>
                                                <input type="file" name="SliderImage[]" id="filer_input1" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <?php echo $slider_images;?>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group m-b-20">
                                                <label>Banner Images After Slider</label><br>
                                                <input type="file" name="BannerAftSldImage[]" id="filer_input2" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <?php echo $banner_after_slider_images;?>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group m-b-20">
                                                <label>Banner Images After Featured Products (Choose one Image)</label><br>
                                                <input type="file" name="BannerAftFeatProdImage[]" id="filer_input3" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <?php echo $banner_after_featur_prod_images;?>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group m-b-20">
                                                <label>Banner Images Below Category (Choose one Image)</label><br>
                                                <input type="file" name="BannerBelCat[]" id="filer_input5" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <?php echo $banner_below_cat_images;?>
                                    </div>



                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group m-b-20">
                                                <label>Banner Images Above Footer (Choose one Image)</label><br>
                                                <input type="file" name="BannerAbvFoot[]" id="filer_input6" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <?php echo $banner_above_cat_images;?>
                                    </div>



                                    <div class="form-group text-right m-b-0">
                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                            <?php echo lang('submit');?>
                                        </button>
                                        <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                            <button type="button" class="btn btn-default waves-effect m-l-5">
                                                <?php echo lang('back');?>
                                            </button>
                                        </a>
                                    </div>

                                </form>
                            </div>
                            <div class="tab-pane <?php echo ((isset($_GET['tab']) && $_GET['tab'] == 2) ? 'active' : '')?>" id="pill2">
                                <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                                    <input type="hidden" name="form_type" value="saveCategoryImage">
                                    <input type="hidden" name="tab" value="2">
                                    <input type="hidden" name="HomeSettingsID" value="<?php echo base64_encode($result[0]->HomeSettingsID); ?>">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group m-b-20">
                                                <label>Home Page Category Images</label><br>
                                                <input type="file" name="HomePageCatImg[]" id="filer_input7" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <?php echo $home_page_cat_image;?>
                                    </div>

                                    <div class="form-group text-right m-b-0">
                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                            <?php echo lang('submit');?>
                                        </button>
                                        <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                            <button type="button" class="btn btn-default waves-effect m-l-5">
                                                <?php echo lang('back');?>
                                            </button>
                                        </a>
                                    </div>
                                </form>
                            </div>
                            <!-- <div class="tab-pane" id="pill3">
                                Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate one-to-one customer service with robust ideas.
                                <br />
                                <br />Dynamically innovate resource-leveling customer service for state of the art customer service.
                            </div> -->
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>

<div class="modal fade" id="DeleteImage" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                <h4 class="modal-title custom_align" id="Heading"><?php echo lang('delete'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> <?php echo lang('AreYouSureDeleteFile'); ?></div>
            </div>
            <div class="modal-footer ">
                <a type="button" class="btn btn-success delete_url" ><span class="glyphicon glyphicon-ok-sign"></span> <?php echo lang('Yes'); ?></a>
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> <?php echo lang('No'); ?></button>
            </div>
        </div>
        <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog --> 
</div>

<script type="text/javascript">

    $(document).on('click', '.remove_image', function () {
        var image_id       = $(this).attr('data-image-id');
        var image_path       = $(this).attr('data-image-path');
        $(".delete_url").attr('data-modal-image-id', image_id);
        $(".delete_url").attr('data-modal-image-path', image_path);
        $('#DeleteImage').modal('show');
    });

    $(document).on('click', '.delete_url', function () {
        var image_id       = $(this).attr('data-modal-image-id');
        var image_path       = $(this).attr('data-modal-image-path');
        var $this = $(this);
        $.ajax({
                type: "POST",
                url: '<?php echo base_url() . 'cms/' . $ControllerName . '/DeleteImage'; ?>',
                data: {
                    image_path: image_path,
                    image_id: image_id
                },
                success: function (result) {
                    $.unblockUI;
                    if (result.error != false) {
                        $("#img-"+image_id).remove();
                        $("#redirect-"+image_id).remove();
                    }



                },
                complete: function () {
                    $('#DeleteImage').modal('hide');
                    $.unblockUI();
                }
            });
    });

</script>