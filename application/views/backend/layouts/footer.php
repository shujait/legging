<?php if($this->uri->segment('1') == 'cms'){ ?> 
</div>
</div>
<?php }?>
<footer class="footer">
    <div class="container">
         <div class="row">
            <div class="col-md-12">
                <div class="footer-social ">
                    <ul>
                        <li>
                            <a target="_blank" href="<?php echo $site_setting->FacebookUrl;?>"><img src="<?php echo base_url(); ?>assets/backend/img/11.png"></a>
                        </li>
                        
                        <li>
                            <a target="_blank" href="<?php echo $site_setting->GoogleUrl;?>"><img src="<?php echo base_url(); ?>assets/backend/img/gplus.png"></a>
                        </li>
                        <li>
                            <a target="_blank" href="<?php echo $site_setting->LinkedInUrl;?>"><img src="<?php echo base_url(); ?>assets/backend/img/05.png"></a>
                        </li>
                        <li>
                            <a target="_blank" href="<?php echo $site_setting->TwitterUrl;?>"><img src="<?php echo base_url(); ?>assets/backend/img/06.png"></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="footer-nav ">
                    <ul>
                        <?php  $menu = getMenuData('2');

                                if($menu){

                                    foreach ($menu as $key => $value) {
                                        $page = get_page_by_id($value->id);
                                        if($value->id != '4')
                                        {   
                                     ?>
                                            <li><a href="<?php echo base_url($page->Url); ?>"><?php echo $page->Title; ?></a></li>
                                   <?php 
                                        }
                                    }
                               }
                         ?>
                        
                        
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="footer-email ">
                    <ul>
                        <li><?php echo $site_setting->Email; ?></li>
                        <li><?php echo $site_setting->PhoneNumber; ?></li>
                        <li class="address"><?php echo $site_setting->Skype; ?></li>
                    </ul>
                </div>
            </div>
        </div>
       
    </div>
    <div class="copy">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>&copy; <?php echo date('Y');?> All Copy Rights Reserved Leggings Pro. Crafted by <a href="https://www.thexpertz.com" target="_blank" style="color: white;">Web Development</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--Newsletter Modal -->

<div class="modal fade newsletterStyle" id="newsletterModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body">
                <div class="newsletterForm">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <label data-error="wrong" data-success="right" for="newsletter-email" class="hide"></label>
                            <input type="email" id="newsletter-email" class="form-control validate" placeholder="Your email">
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <button type="submit" class="btn btn-block btn-success">Send</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Newsletter Modal -->
<!--Login Modal -->
<div class="modal fade formModal" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold">Sign in</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?php echo base_url();?>account/checkLogin" method="post" onsubmit="return false;"  class="form-horizontal loginForm form_data">
                    <div class="md-form mb-5">
                        <label data-error="wrong" data-success="right" for="defaultForm-email">Your email</label>
                        <input type="email" id="defaultForm-email" name="Email" class="form-control validate">
                    </div>

                    <div class="md-form mb-4">
                        <label data-error="wrong" data-success="right" for="defaultForm-pass">Your password</label>
                        <input type="password" id="defaultForm-pass" name="Password" class="form-control validate">
                        <a href="#" data-toggle="modal" data-target="#modalforgetPassword" data-dismiss="modal">Forget password?</a>

                    </div>
                    <div class="row">
                        <div class="col-md-4 col-xs-12">
                            <button type="submit" class="btn btn-success">Login</button>

                        </div>
                        <!-- <div class="col-md-8 col-xs-12 text-right register-modal-link">
                        <a href="<?php //echo base_url('account/register');?>">Register as Wholesaler</a><br>
                        <a href="<?php //echo base_url('account/customer_register');?>">Register as Customer</a>
                        </div> -->
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Login Modal -->
<!--Forget Password Modal -->
<div class="modal fade formModal" id="modalforgetPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold">Retrieve Password</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?php echo base_url();?>account/sendforgotmail" method="post" onsubmit="return false;"  class="form-horizontal loginForm form_data">
                    <div class="md-form">
                        <label data-error="wrong" data-success="right" for="ForgetForm-email">Your email</label>
                        <input type="email" name="Email" id="ForgetForm-email" class="form-control validate">
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <button type="submit" class="btn btn-block btn-success">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php 

    if (isset($this->session->userdata['admin']['UserID'])) {

        $user_id = $this->session->userdata['admin']['UserID'];

    } else {
        if (!get_cookie('temp_order_key')) {
            $user_id = 0;
            
        } else {
            $user_id = get_cookie('temp_order_key');
        }
    }
    if($user_id > 0)
    {
        $productdet = getUserCartDetail($user_id);
    }
    else
    {
        $productdet = '';
    }
?>

<div class="modal fade popup-cart" id="popup-cart" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="container">
            <div class="cart-items">
                <div class="cart-items-inner">
                <div class="cart-box-heading">
                    SUMMARY
                    <a href="#" class="close-btn pull-right" data-dismiss="modal">X</a>
                </div>
                <div class="discount">
                    <a href="#">Do you have a Promo code?</a>
                </div>

                <?php 
                    if($productdet != '')
                    {
                        $totalPrice = 0;
                        foreach ($productdet as $key => $value) {

                            $Price = (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? $value['PriceForWholeSaler'] : $value['Price']);
                            $DiscountPer = (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? $value['WholeSaleDiscount'] : $value['Discount']);

                            if($DiscountPer > 0)
                            {
                              $Discount = $Price * ($DiscountPer/100);

                              $Price = $Price - $Discount;
                            }
                ?>
                            <div class="media">
                                <a class="pull-left" href="#"><img class="media-object item-image" src="#" alt=""></a>
                                <p class="pull-right item-price"><?php echo getSelectedCurrencies($CurrencyID,$Price);?></p>
                                <div class="media-body">
                                    <h4 class="media-heading item-title"><a href="#"><?php echo ($value['TotalPieces'] > 0 ? $value['TotalPieces'] * $value['quantity'] : $value['quantity']) ?> x <?php echo $value['Title'];?></a></h4>
                                    <!-- <p class="item-desc">Lorem ipsum dolor</p> -->
                                </div>
                            </div>
                <?php
                            $totalPrice += ($value['TotalPieces'] > 0 ? $value['TotalPieces'] * $value['quantity'] * $Price : $value['quantity'] * $Price);
                        }
                ?>

                    <div class="media">
                        <p class="pull-right item-price">-$15.00</p>
                        <div class="media-body">
                            <h4 class="media-heading item-title summary">Discount<span>(-50%)</span></h4>
                        </div>
                    </div>

                    <div class="media">
                        <p class="pull-right item-price"><?php echo getSelectedCurrencies($CurrencyID,$totalPrice);?></p>
                        <div class="media-body">
                            <h4 class="media-heading item-title summary subtotal">Subtotal</h4>
                        </div>
                    </div>

                    

                <?php
                    }
                    else
                    {
                ?>
                    <div class="media">
                        <div class="media-body">
                            <p class="item-desc">Empty Cart</p>
                        </div>
                    </div>
                <?php 
                    }
                ?>
                    
                    <div class="media">
                        <div class="media-body">
                            <div>

                              <a href="<?php echo base_url('cart');?>" class="view-cart">View Cart</a>
                               <a href="<?php echo base_url('checkout');?>" class="checkout-btn">Checkout</a>
                            <a href="#" class="checkout-btn">Checkout with <span><img src="<?php echo base_url(); ?>assets/backend/img/paypal.png"></span></a>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>

<script type="text/javascript">
    (function($){
    $(document).ready(function(){
        $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
            event.preventDefault(); 
            event.stopPropagation(); 
            $(this).parent().siblings().removeClass('open');
            $(this).parent().toggleClass('open');
        });
    });
})(jQuery);
</script>

<script type="text/javascript">
    $(document).ready(function () {
                   
       $('#CountryID, .CountryID').on('change',function(){
           
            var CountryID = $(this).val();
            var StateType = $(this).attr('data-state-type');

            if(CountryID == '')
            {
                if(StateType == 'BillingState')
                {
                    $('#'+StateType).html('<option value=""><?php echo lang("choose_state");?></option>');
                }
                else if(StateType == 'ShippingState')
                {
                    $('#'+StateType).html('<option value=""><?php echo lang("choose_state");?></option>');
                }
                else
                {
                    $('#StateID').html('<option value=""><?php echo lang("choose_state");?></option>');
                }
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            
            
                $.ajax({
                    type: "POST",
                    url: base_url + 'home/getCountryStates',
                    data: {
                        'CountryID': CountryID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        if(StateType == 'BillingState')
                        {
                            $('#'+StateType).html(result.html);
                        }
                        else if(StateType == 'ShippingState')
                        {
                            $('#'+StateType).html(result.html);
                        }
                        else
                        {
                            $('#StateID').html(result.html);
                            if(result.array)
                            {
                                $(".selectpicker").selectpicker('refresh');
                            }
                        }

                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }
           
       });
       $('#StateID, .StateID').on('change',function(){
           
            var StateID = $(this).val();
            var CityType = $(this).attr('data-city-type');

            if(StateID == '')
            {
                if(CityType == 'BillingCity')
                {
                    $('#'+CityType).html('<option value=""><?php echo lang("choose_city");?></option>');
                }
                else if(CityType == 'ShippingCity')
                {
                    $('#'+CityType).html('<option value=""><?php echo lang("choose_city");?></option>');
                }
                else
                {
                    $('#CityID').html('<option value=""><?php echo lang("choose_city");?></option>');
                }
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            
            
                $.ajax({
                    type: "POST",
                    url: base_url + 'home/getStateCities',
                    data: {
                        'StateID': StateID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#CityID').html(result.html);
                        if(CityType == 'BillingCity')
                        {
                            $('#'+CityType).html(result.html);
                        }
                        else if(CityType == 'ShippingCity')
                        {
                            $('#'+CityType).html(result.html);
                        }
                        else
                        {
                            $('#CityID').html(result.html);
                            if(result.array)
                            {
                                $(".selectpicker").selectpicker('refresh');
                            }
                        }

                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }
           
       });
       $('.CategoryID').on('change',function(){
           
            var CategoryID = $(this).val();
            var level = $(this).attr('data-level');

            if(CategoryID == '')
            {
                $('#SubCategoryID').html('<option value=""><?php echo lang("choose_sub_category");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            
            
                $.ajax({
                    type: "POST",
                    url: base_url + 'cms/category/getSubCategory',
                    data: {
                        'CategoryID': CategoryID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {
                        if(level == 1){
                             $('#SubCategoryID').html(result.html);

                         }else{
                            $('#SubCategoryIDLevel2').html(result.html);
                         }
                         $(".selectpicker").selectpicker('refresh');
                       
                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }
           
       });


       $('.LoadMoreProducts').on('click',function(){
           
            var CategoryID = $(this).attr('data-category-id');
            var StartRange = $('#StartRange').val();
            var TotalCount = $('#TotalCount').val();
            var currency = $('#currency').val();
            var sortOrder = $('#sortOrder').val();
            var size = [];
            $( ".SelectSize" ).each(function( index ) {
              if($(this).hasClass('active'))
              {
                size.push($(this).attr('data-size'));
              }
            });

            $.blockUI({
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        
        
            $.ajax({
                type: "POST",
                url: base_url + 'products/LoadMoreProducts',
                data: {
                    'CategoryID': CategoryID,
                    'StartRange': StartRange,
                    'TotalCount': TotalCount,
                    'Currency': currency,
                    'SortOrder': sortOrder,
                    'Size': JSON.stringify(size)
                },
                dataType: "json",
                cache: false,
                //async:false,
                success: function(result) {
                    if(result.error == false){
                         $('#MoreProducts').append(result.html);

                         $('#TotalCount').val(result.productCount);
                         $('#StartRange').val(parseInt($('#StartRange').val()) + 8);

                         if(result.productCount <= 8)
                         {
                            $('.parentLoadMoreDiv').hide();
                         }
                     }
                   
                },
                complete: function() {
                    $.unblockUI();
                }
            });
           
       });

       $('#DollersystemID').on('change',function(){
           
            var DollersystemID = $(this).val();

            if(DollersystemID != '')
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            
            
                $.ajax({
                    type: "POST",
                    url: base_url + 'cms/package/getAllPackages',
                    data: {
                        'DollersystemID': DollersystemID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#DurationPackage').html(result.html);

                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }
           
       });
       
    });



    $('.PacketID').on('change',function(){

        var divId = makeid();

        $(this).parents('.newPacketDet').attr('id', divId);

        var pieces = $(this).val();

        var options_drp = '';
        var selected = '';
        for(var i=1; i<=pieces; i++)
        {
            if(i == pieces)
            {
                selected = 'selected=""selected';
            }
            options_drp += '<option '+selected+' value="'+i+'">'+i+'</option>';            
        }

        $(".variationfield").find('.noofpieces').html(options_drp);

        var GetHtml = $(".variationfield").html();
        $('#packetVariationModal').find('.modal-body').html('');
            
        $('#totalPieces').val(pieces);
        $('#packetVariationModal').find('.modal-body').append(GetHtml);
        $('#packetVariationModal').find('.modal-footer').find('.btn-default').attr('data-div-id',divId)

        $('.makePackets').click(function(){
            var mainDivId = $(this).attr('data-div-id');

            var packetSizes = $('#packetVariationModal').find(".packetSizes");
            var packetColors = $('#packetVariationModal').find(".packetColors");
            var packetPieces = $('#packetVariationModal').find(".packetPieces");

            var packetArr = [];
            if(packetSizes.length > 0)
            {
                var packetstable = '<div class="col-md-8 tableDiv"><table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%"><thead><tr><th>Size</th><th>Color</th><th>Pieces</th></tr></thead><tbody>';

                for(var i = 0; i < packetSizes.length; i++){
                    
                    var packetSizesArr = $(packetSizes[i]).val().split('-');
                    var packetColorsArr = $(packetColors[i]).val().split('-');

                    var packet = {'size':packetSizesArr[0],'color':packetColorsArr[0],'piece':$(packetPieces[i]).val()};

                    packetArr.push(packet);

                    packetstable += '<tr><td>'+packetSizesArr[1]+'</td><td>'+packetColorsArr[1]+'</td><td>'+$(packetPieces[i]).val()+'</td></tr>';

                }

                packetstable += "</tbody></table><input type='hidden' name='packetsJson[]' value='"+JSON.stringify(packetArr)+"'>";

                $('#'+mainDivId).find('.tableDiv').remove();

                $('#'+mainDivId).append(packetstable);
            }
            $('#packetVariationModal').modal('hide');
            $('#packetVariationModal').find('.modal-body').html('');
        });


        $('#packetVariationModal').modal({
            backdrop: 'static',
            keyboard: false
        });

    });

    $('.delRow').click(function(){
        $(this).parents('.newPacketDet').remove();
    });
    
    $('.SelectSize').click(function(){
        if($(this).hasClass('active'))
        {
            $(this).removeClass('active');
        }
        else
        {
            $(this).addClass('active');
        }
        GetFilteredProducts();
    });

function GetFilteredProducts()
{

    //return true;
    var CategoryID = $('#CategoryID').val();
    var currency = $('#currency').val();
    var sortOrder = $('#sortOrder').val();
    var size = [];
    $( ".SelectSize" ).each(function( index ) {
      if($(this).hasClass('active'))
      {
        size.push($(this).attr('data-size'));
      }
    });


    $.blockUI({
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });


    $.ajax({
        type: "POST",
        url: base_url + 'products/GetFilteredProducts',
        data: {
            'CategoryID': CategoryID,
            'Currency': currency,
            'SortOrder': sortOrder,
            'Size': JSON.stringify(size)
        },
        dataType: "json",
        cache: false,
        //async:false,
        success: function(result) {

            if(result.error == false){
                $('#StartRange').val('8');
            }

            $('#MoreProducts').html(result.html);

            $('#TotalCount').val(result.productCount);

            if(result.productCount <= 8)
             {
                $('.parentLoadMoreDiv').hide();
             }
             else
             {
                $('.parentLoadMoreDiv').show();  
             }
           
        },
        complete: function() {
            $.unblockUI();
        }
    });
}

function ChangeCurrency(DollersystemID)
{

    $.blockUI({
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });


    $.ajax({
        type: "POST",
        url: base_url + 'home/changeCurrencies',
        data: {
            'DollersystemID': DollersystemID
        },
        dataType: "json",
        cache: false,
        //async:false,
        success: function(result) {
            location.reload();
        },
        complete: function() {
            $.unblockUI();
        }
    });
}

function add()
{
    var GetHtml = $('#newpackets').html();

    $('.morePackets').append(GetHtml);

    $('.PacketID').on('change',function(){

        var divId = makeid();

        $(this).parents('.newPacketDet').attr('id', divId);

        var pieces = $(this).val();

        var options_drp = '';
        var selected = '';
        for(var i=1; i<=pieces; i++)
        {
            if(i == pieces)
            {
                selected = 'selected=""selected';
            }
            options_drp += '<option '+selected+' value="'+i+'">'+i+'</option>';            
        }

        $(".variationfield").find('.noofpieces').html(options_drp);

        var GetHtml = $(".variationfield").html();
        $('#packetVariationModal').find('.modal-body').html('');
            
        $('#totalPieces').val(pieces);
        $('#packetVariationModal').find('.modal-body').append(GetHtml);
        $('#packetVariationModal').find('.modal-footer').find('.btn-default').attr('data-div-id',divId)

        $('.makePackets').click(function(){
            var mainDivId = $(this).attr('data-div-id');

            var packetSizes = $('#packetVariationModal').find(".packetSizes");
            var packetColors = $('#packetVariationModal').find(".packetColors");
            var packetPieces = $('#packetVariationModal').find(".packetPieces");

            var packetArr = [];
            if(packetSizes.length > 0)
            {
                var packetstable = '<div class="col-md-8 tableDiv"><table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%"><thead><tr><th>Size</th><th>Color</th><th>Pieces</th></tr></thead><tbody>';

                for(var i = 0; i < packetSizes.length; i++){
                    
                    var packetSizesArr = $(packetSizes[i]).val().split('-');
                    var packetColorsArr = $(packetColors[i]).val().split('-');

                    var packet = {'size':packetSizesArr[0],'color':packetColorsArr[0],'piece':$(packetPieces[i]).val()};

                    packetArr.push(packet);

                    packetstable += '<tr><td>'+packetSizesArr[1]+'</td><td>'+packetColorsArr[1]+'</td><td>'+$(packetPieces[i]).val()+'</td></tr>';

                }

                packetstable += "</tbody></table><input type='hidden' name='packetsJson[]' value='"+JSON.stringify(packetArr)+"'>";

                $('#'+mainDivId).find('.tableDiv').remove();
                $('#'+mainDivId).append(packetstable);
            }
            $('#packetVariationModal').modal('hide');
            $('#packetVariationModal').find('.modal-body').html('');
        });


        $('#packetVariationModal').modal({
            backdrop: 'static',
            keyboard: false
        });

    });

    $('.delRow').click(function(){
        $(this).parents('.newPacketDet').remove();
    });
}

function recalculatePiece(pieces)
{
    var totalPieces = $('#totalPieces').val();
    var remainingPieces = parseInt(totalPieces)-parseInt(pieces);

    if(remainingPieces == 0)
    {
        return false;
    }
    else
    {
        var options_drp = '';
        var selected = '';
        for(var i=1; i<=remainingPieces; i++)
        {
            if(i == pieces)
            {
                selected = 'selected=""selected';
            }
            options_drp += '<option '+selected+' value="'+i+'">'+i+'</option>';            
        }

        $(".variationfield").find('.noofpieces').html(options_drp);

        var GetHtml = $(".variationfield").html();
            
        $('#totalPieces').val(remainingPieces);
        $('#packetVariationModal').find('.modal-body').append(GetHtml);
           
    }
}

function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

$('.add_to_cart').click(function(){
    var product_id = $(this).attr('data-pro-id');
        var product_quantity = $('#quantity_field').val();
        <?php 
        if(isset($this->session->userdata['admin']['RoleID']) && $this->session->userdata['admin']['RoleID'] == 4 && !empty($product_packets)){ 
        ?>
            var product_packet = $("#product_packets").val();
        <?php 
        }else{
        ?>
            var size = $("input[name='size']:checked"). val();
            var colour = $("input[name='color']:checked"). val();
        <?php 
        }
        ?>
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        // console.log(qty); return;
        $.post('<?=base_url()?>home/add_to_cart', 
        {
            product_id:product_id, 
            product_quantity:product_quantity,
            <?php 
            if(isset($this->session->userdata['admin']['RoleID']) && $this->session->userdata['admin']['RoleID'] == 4 && !empty($product_packets)){ 
            ?>
                product_packet:product_packet
            <?php 
            }else{
            ?>      
                size,
                colour
            <?php 
            }
            ?>
        }, function(resp){
            res = $.parseJSON(resp)
            if (res.status == 'TRUE') {
                    $.unblockUI();
                    showSuccess(res.message);
                    
                    if(res.reload){
                        setTimeout(function () {
                            location.reload();
                        }, 1000);
                    }
                    
               // $('#openAddToCartConfirmationModal').click();

                } else {
                     $.unblockUI();
                     showError(res.message);
                   
                    if (res.redirect) {
                        setTimeout(function () {
                            window.location.href = base_url + res.url;
                        }, 1000);
                    }
                }

        })
    })
    
    
    $('.delete_cart_item').click(function(){
    if(confirm('Are you sure you want to delete this?')){
        var temp_order_id = $(this).attr('data-temp-id');
     
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        // console.log(qty); return;
        $.post('<?=base_url()?>home/delete_cart_item', 
        {
            temp_order_id:temp_order_id, 
            
        }, function(resp){
            res = $.parseJSON(resp)
            if (res.status == 'TRUE') {
                    
                    $("#delete-"+temp_order_id).remove();
                    $.unblockUI();

                    location.reload();
                    
               // $('#openAddToCartConfirmationModal').click();

                } 

        })
    }
    });

    function changeQuantity(temp_order_id, quantity)
    {
     
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        // console.log(qty); return;
        $.post('<?=base_url()?>home/update_quantity', 
        {
            temp_order_id:temp_order_id,
            quantity:quantity 
            
        }, function(resp){
            res = $.parseJSON(resp)
            if (res.status == 'TRUE') {
                    $.unblockUI();
                    
               // $('#openAddToCartConfirmationModal').click();

                } 

        })
    }
</script>

<script src="<?php echo base_url();?>assets/backend/js/owl.carousel.min.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            loop: true,
            margin: 12,
            nav: true,
            pagination: false,
            navText: ['<img src="<?php echo base_url('assets/backend');?>/img/arrow-left.jpg"/>', '<img src="<?php echo base_url('assets/backend');?>/img/arrow-right.jpg"/>'],
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 5
                }
            }
        });
    });
    $(window).on('load', function () {
        //$('#newsletterModal').modal('show');
    });

    /**/
    jQuery(document).on('click', '.mega-dropdown', function(e) {
        e.stopPropagation()
    })
    /**/
</script>
<script src="<?php echo base_url();?>assets/backend/js/easyzoom.js"></script>
    <script>
        // Instantiate EasyZoom instances
        var $easyzoom = $('.easyzoom').easyZoom();

        // Setup thumbnails example
        var api1 = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');

        $('.thumbnails').on('click', 'a', function(e) {
            var $this = $(this);

            e.preventDefault();

            // Use EasyZoom's `swap` method
            api1.swap($this.data('standard'), $this.attr('href'));
        });

        // Setup toggles example
        var api2 = $easyzoom.filter('.easyzoom--with-toggle').data('easyZoom');

        $('.toggle').on('click', function() {
            var $this = $(this);

            if ($this.data("active") === true) {
                $this.text("Switch on").data("active", false);
                api2.teardown();
            } else {
                $this.text("Switch off").data("active", true);
                api2._init();
            }
        });
    </script>


<script type="text/javascript">
    (function(e){var t,o={className:"autosizejs",append:"",callback:!1,resizeDelay:10},i='<textarea tabindex="-1" style="position:absolute; top:-999px; left:0; right:auto; bottom:auto; border:0; padding: 0; -moz-box-sizing:content-box; -webkit-box-sizing:content-box; box-sizing:content-box; word-wrap:break-word; height:0 !important; min-height:0 !important; overflow:hidden; transition:none; -webkit-transition:none; -moz-transition:none;"/>',n=["fontFamily","fontSize","fontWeight","fontStyle","letterSpacing","textTransform","wordSpacing","textIndent"],s=e(i).data("autosize",!0)[0];s.style.lineHeight="99px","99px"===e(s).css("lineHeight")&&n.push("lineHeight"),s.style.lineHeight="",e.fn.autosize=function(i){return this.length?(i=e.extend({},o,i||{}),s.parentNode!==document.body&&e(document.body).append(s),this.each(function(){function o(){var t,o;"getComputedStyle"in window?(t=window.getComputedStyle(u,null),o=u.getBoundingClientRect().width,e.each(["paddingLeft","paddingRight","borderLeftWidth","borderRightWidth"],function(e,i){o-=parseInt(t[i],10)}),s.style.width=o+"px"):s.style.width=Math.max(p.width(),0)+"px"}function a(){var a={};if(t=u,s.className=i.className,d=parseInt(p.css("maxHeight"),10),e.each(n,function(e,t){a[t]=p.css(t)}),e(s).css(a),o(),window.chrome){var r=u.style.width;u.style.width="0px",u.offsetWidth,u.style.width=r}}function r(){var e,n;t!==u?a():o(),s.value=u.value+i.append,s.style.overflowY=u.style.overflowY,n=parseInt(u.style.height,10),s.scrollTop=0,s.scrollTop=9e4,e=s.scrollTop,d&&e>d?(u.style.overflowY="scroll",e=d):(u.style.overflowY="hidden",c>e&&(e=c)),e+=w,n!==e&&(u.style.height=e+"px",f&&i.callback.call(u,u))}function l(){clearTimeout(h),h=setTimeout(function(){var e=p.width();e!==g&&(g=e,r())},parseInt(i.resizeDelay,10))}var d,c,h,u=this,p=e(u),w=0,f=e.isFunction(i.callback),z={height:u.style.height,overflow:u.style.overflow,overflowY:u.style.overflowY,wordWrap:u.style.wordWrap,resize:u.style.resize},g=p.width();p.data("autosize")||(p.data("autosize",!0),("border-box"===p.css("box-sizing")||"border-box"===p.css("-moz-box-sizing")||"border-box"===p.css("-webkit-box-sizing"))&&(w=p.outerHeight()-p.height()),c=Math.max(parseInt(p.css("minHeight"),10)-w||0,p.height()),p.css({overflow:"hidden",overflowY:"hidden",wordWrap:"break-word",resize:"none"===p.css("resize")||"vertical"===p.css("resize")?"none":"horizontal"}),"onpropertychange"in u?"oninput"in u?p.on("input.autosize keyup.autosize",r):p.on("propertychange.autosize",function(){"value"===event.propertyName&&r()}):p.on("input.autosize",r),i.resizeDelay!==!1&&e(window).on("resize.autosize",l),p.on("autosize.resize",r),p.on("autosize.resizeIncludeStyle",function(){t=null,r()}),p.on("autosize.destroy",function(){t=null,clearTimeout(h),e(window).off("resize",l),p.off("autosize").off(".autosize").css(z).removeData("autosize")}),r())})):this}})(window.jQuery||window.$);

var __slice=[].slice;(function(e,t){var n;n=function(){function t(t,n){var r,i,s,o=this;this.options=e.extend({},this.defaults,n);this.$el=t;s=this.defaults;for(r in s){i=s[r];if(this.$el.data(r)!=null){this.options[r]=this.$el.data(r)}}this.createStars();this.syncRating();this.$el.on("mouseover.starrr","span",function(e){return o.syncRating(o.$el.find("span").index(e.currentTarget)+1)});this.$el.on("mouseout.starrr",function(){return o.syncRating()});this.$el.on("click.starrr","span",function(e){return o.setRating(o.$el.find("span").index(e.currentTarget)+1)});this.$el.on("starrr:change",this.options.change)}t.prototype.defaults={rating:void 0,numStars:5,change:function(e,t){}};t.prototype.createStars=function(){var e,t,n;n=[];for(e=1,t=this.options.numStars;1<=t?e<=t:e>=t;1<=t?e++:e--){n.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"))}return n};t.prototype.setRating=function(e){if(this.options.rating===e){e=void 0}this.options.rating=e;this.syncRating();return this.$el.trigger("starrr:change",e)};t.prototype.syncRating=function(e){var t,n,r,i;e||(e=this.options.rating);if(e){for(t=n=0,i=e-1;0<=i?n<=i:n>=i;t=0<=i?++n:--n){this.$el.find("span").eq(t).removeClass("glyphicon-star-empty").addClass("glyphicon-star")}}if(e&&e<5){for(t=r=e;e<=4?r<=4:r>=4;t=e<=4?++r:--r){this.$el.find("span").eq(t).removeClass("glyphicon-star").addClass("glyphicon-star-empty")}}if(!e){return this.$el.find("span").removeClass("glyphicon-star").addClass("glyphicon-star-empty")}};return t}();return e.fn.extend({starrr:function(){var t,r;r=arguments[0],t=2<=arguments.length?__slice.call(arguments,1):[];return this.each(function(){var i;i=e(this).data("star-rating");if(!i){e(this).data("star-rating",i=new n(e(this),r))}if(typeof r==="string"){return i[r].apply(i,t)}})}})})(window.jQuery,window);$(function(){return $(".starrr").starrr()})

$(function(){

  $('#new-review').autosize({append: "\n"});

  var reviewBox = $('#post-review-box');
  var newReview = $('#new-review');
  var openReviewBtn = $('#open-review-box');
  var closeReviewBtn = $('#close-review-box');
  var ratingsField = $('#ratings-hidden');

  openReviewBtn.click(function(e)
  {
    reviewBox.slideDown(400, function()
      {
        $('#new-review').trigger('autosize.resize');
        newReview.focus();
      });
    openReviewBtn.fadeOut(100);
    closeReviewBtn.show();
  });

  closeReviewBtn.click(function(e)
  {
    e.preventDefault();
    reviewBox.slideUp(300, function()
      {
        newReview.focus();
        openReviewBtn.fadeIn(200);
      });
    closeReviewBtn.hide();
    
  });

  $('.starrr').on('starrr:change', function(e, value){
    ratingsField.val(value);
  });
});
    
</script>
<!-- WhatsHelp.io widget -->
<script type="text/javascript">
    (function () {
        var options = {
            whatsapp: "<?php echo $site_setting->PhoneNumber; ?>", // WhatsApp number
            call_to_action: "Message us", // Call to action
            position: "right", // Position may be 'right' or 'left'
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /WhatsHelp.io widget -->


<script src="<?php echo base_url();?>assets/backend/plugins/tinymce/tinymce.min.js"></script>

<!-- Forms Validations Plugin -->
<script src="<?php echo base_url();?>assets/backend/js/jquery.validate.min.js"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="<?php echo base_url();?>assets/backend/js/moment.min.js"></script>
<!--  Charts Plugin -->
<script src="<?php echo base_url();?>assets/backend/js/chartist.min.js"></script>
<!--  Plugin for the Wizard -->
<script src="<?php echo base_url();?>assets/backend/js/jquery.bootstrap-wizard.js"></script>
<!--  Notifications Plugin    -->
<script src="<?php echo base_url();?>assets/backend/js/bootstrap-notify.js"></script>
<!--   Sharrre Library    -->
<script src="<?php echo base_url();?>assets/backend/js/jquery.sharrre.js"></script>
<!-- DateTimePicker Plugin -->
<script src="<?php echo base_url();?>assets/backend/js/bootstrap-datetimepicker.js"></script>
<!-- Vector Map plugin -->
<script src="<?php echo base_url();?>assets/backend/js/jquery-jvectormap.js"></script>
<!-- Sliders Plugin -->
<script src="<?php echo base_url();?>assets/backend/js/nouislider.min.js"></script>
<!--  Google Maps Plugin    -->
<!--<script src="https://maps.googleapis.com/maps/api/js"></script>-->
<!-- Select Plugin -->
<script src="<?php echo base_url();?>assets/backend/js/jquery.select-bootstrap.js"></script>
<!--  DataTables.net Plugin    -->
<script src="<?php echo base_url();?>assets/backend/js/jquery.datatables.js"></script>
<!-- Sweet Alert 2 plugin -->
<script src="<?php echo base_url();?>assets/backend/js/sweetalert2.js"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="<?php echo base_url();?>assets/backend/js/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin    -->
<script src="<?php echo base_url();?>assets/backend/js/fullcalendar.min.js"></script>
<!-- TagsInput Plugin -->
<script src="<?php echo base_url();?>assets/backend/js/jquery.tagsinput.js"></script>
<!-- Material Dashboard javascript methods -->
<script src="<?php echo base_url();?>assets/backend/js/material-dashboard.js"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="<?php echo base_url();?>assets/backend/js/demo.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/script.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/jquery.blockUI.js"></script>


<script src="<?php echo base_url();?>assets/backend/plugins/jquery.filer/js/jquery.filer.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/custom-angular.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/backend/js/jquery.fileuploads.init.js"></script>

<?php if($this->session->flashdata('message')){ ?>
    <script>

        $( document ).ready(function() {
            showError('<?php echo $this->session->flashdata('message'); ?>');
        });

    </script>
<?php } ?>


<!-- Mirrored from demos.creative-tim.com/material-dashboard-pro/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 Mar 2017 21:32:16 GMT -->
</html>
