<!doctype html>
<html lang="en">
   <!-- Mirrored from demos.creative-tim.com/material-dashboard-pro/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 Mar 2017 21:29:18 GMT -->
   <head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- App favicon -->
      <link rel="icon" href="<?php echo base_url();?>assets/backend/img/favicon.png" type="image/x-icon">
      <link rel="icon" href="<?php echo base_url();?>assets/backend/img/favicon.png">
      <!-- App title -->
      <title>LeggingsPro – Buy Clothes Online</title>
      <!-- Bootstrap core CSS     -->
      <link href="<?php echo base_url(); ?>assets/backend/css/bootstrap.min.css" rel="stylesheet" />
      <?php 
         if($this->uri->segment(1) == 'cms')
         {
         ?>
      <!--  Material Dashboard CSS    -->
      <link href="<?php echo base_url(); ?>assets/backend/css/material-dashboard.css" rel="stylesheet" />
      <?php 
         }
         ?>
      <!--  CSS for Demo Purpose, don't include it in your project     -->
      <link href="<?php echo base_url(); ?>assets/backend/css/demo.css" rel="stylesheet" />
      <!--     Fonts and icons     -->
      <link href="<?php echo base_url(); ?>assets/backend/css/font-awesome.css" rel="stylesheet" />
      <link href="<?php echo base_url(); ?>assets/backend/css/google-roboto-300-700.css" rel="stylesheet" />
      <link href="<?php echo base_url(); ?>assets/backend/css/nestable.css" rel="stylesheet" />
      <link href="<?php echo base_url(); ?>assets/backend/plugins/jquery.filer/css/jquery.filer.css" rel="stylesheet" />
      <link href="<?php echo base_url(); ?>assets/backend/plugins/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css" rel="stylesheet" />
      <link href="<?php echo base_url(); ?>assets/backend/plugins/clockpicker/css/bootstrap-clockpicker.min.css" rel="stylesheet" />
      <link href="<?php echo base_url(); ?>assets/backend/css/owl.carousel.min.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>assets/backend/css/style.css" rel="stylesheet" />
      <link href="<?php echo base_url(); ?>assets/backend/css/easyzoom.css" rel="stylesheet" />
      <!--   Core JS Files   -->
      <script src="<?php echo base_url(); ?>assets/backend/js/jquery-3.1.1.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>assets/backend/js/jquery-ui.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>assets/backend/js/bootstrap.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>assets/backend/js/material.min.js" type="text/javascript"></script>
      <!-- <script src="<?php //echo base_url();?>assets/backend/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script> -->
      <script src="<?php echo base_url(); ?>assets/backend/js/angular.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>assets/backend/js/jquery.nestable.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>assets/backend/js/custom-angular.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>assets/backend/plugins/clockpicker/js/bootstrap-clockpicker.min.js" type="text/javascript"></script>
   </head>
   <script>
      var base_url = '<?php echo base_url(); ?>';
      var delete_msg = '<?php echo lang('are_you_sure');?>';
      var module_section = '<?php echo $this->uri->segment(1);?>';
   </script>
   <style>
      .validate_error{
      border : 1px solid red;
      }
      #validation-msg {
      visibility: hidden;
      min-width: 250px;
      margin-left: -125px;
      text-align: center;
      border-radius: 2px;
      padding: 16px;
      position: fixed;
      z-index: 9999;
      left: 50%;
      top: 80px;
      font-size: 17px;
      }
      #validation-msg.show {
      visibility: visible;
      -webkit-animation: fadein 0.5s, fadeout 0.5s 4.5s;
      animation: fadein 0.5s, fadeout 0.5s 4.5s;
      }
   </style>
   <body ng-App="App">
      <header class="header">
         <!-- Header top bar -->
         <!--   <div class="top-bar">
            <div class="container">
               <?php 
               if(!$this->session->userdata('admin')){ ?>
               <div class="top-bar-left">
                  <ul class="list-inline">
                     <li class="icon-user"><a href="<?php echo base_url('account/register');?>">Please login to see <button style="background-color:orange;">Discounted</button> price</a></li>
                     <li class="icon-user"><a href="<?php echo base_url('account/customer_register');?>">Register for the Customer account</a></li>
                  </ul>
               </div>
               <?php 
               } 
               ?>               
            </div>
            </div> -->
         <div class="header-black">
            <div class="container">
               <div class="col-md-12">
                  <div class="col-md-2">
                     <div class="logo-new">
                        <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/backend/img/logo-new.png" alt="Legging Pro"/></a>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="header-search">
                        <form action="<?php echo base_url('products/search');?>" method="get">
                           <input class="searchbar" name="keyword" type="text" value="<?php echo (!empty($categoryDet) && !is_array($categoryDet) ? $categoryDet : '');?>" placeholder="Search">
                           <button type="submit"><img src="<?php echo base_url(); ?>assets/backend/img/search.png" alt=""></button>
                        </form>
                     </div>
                  </div>
                 
                  <div class="col-md-6">
                     <?php 
                        if (isset($this->session->userdata['admin']['UserID'])) {
                        
                            $user_id = $this->session->userdata['admin']['UserID'];
                        
                        } else {
                            if (!get_cookie('temp_order_key')) {
                                $user_id = 0;
                                
                            } else {
                                $user_id = get_cookie('temp_order_key');
                            }
                        }
                        if($user_id > 0)
                        {
                            $productCount = getTotalProduct($user_id)[0]->products_count;
                        }
                        else
                        {
                            $productCount = 0;
                        }
                        ?>
                      <div class="currency">
                        <?php 
                           $currencies = getActiveCurrencies();
                           ?>
                        <select class="form-control currency" onchange="ChangeCurrency(this.value);">
                           <?php foreach($currencies as $currency){?>
                           <option <?php echo ($CurrencyID == $currency->DollerSystemID ? 'selected="selected"' : '');?> value="<?php echo $currency->DollerSystemID;?>"><?php echo $currency->Code;?></option>
                           <?php }?>
                        </select>
                     </div>
                     <div class="my-account">
                           <li class="dropdown">
                              <?php if($this->session->userdata('admin')){ ?>
                              <a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo base_url('cms/myorder'); ?>"> <i class="fa fa-user"> </i><span> MY ACCOUNT</span></a>
                              <ul class="dropdown-menu" role="menu">
                             <li> <a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo base_url('cms/myorder'); ?>"> <i class="fa fa-user"> </i><span> MY ORDER</span></a></li>
                                 <li class="icon-heart"><a href="<?php echo base_url('products/myWishlist'); ?>"><i class="fa fa-heart"></i> <span> MY WISHLIST</span></a></li>
                                 <li class="icon-form"><a href="<?php echo ($this->session->userdata['admin']['RoleID'] == '4' ? base_url('account/logout') : base_url('cms/account/logout'));?>"><i class="fa fa-lock"></i> <span> LOGOUT</span></a></li>
                              </ul>
                              <?php }
                                 else
                                 { ?>
                           <li class="icon-form"><a href="#" data-toggle="modal" data-target="#modalLoginForm"><i class="fa fa-lock"></i> <span>LOGIN</span></a></li>
                           <?php 
                              } 
                              ?>         
                           </li>
                     </div>
                     <div class="cart-wrapper">
                        <a href="#" class="cart-icon" data-toggle="modal" data-target="#popup-cart"><i class="fa fa-shopping-cart"></i> Cart</a>
                     </div>

                     <div class="contact-info">
                       <div class="contact"><span>Need help!</span>
                        +123 456 7890
                     </div>
                     </div>
                     

                     <div class="pro-count"> Items in cart: <span><?php echo $productCount;?></span> | Total: <span>$20,00</span></div>
                  </div>
               </div>
            </div>
         </div>
         <div class="menu">
            <div class="container">
               <div class="col-md-12">
                  <div class="navigation-wrapper">
                                      <nav role="navigation" class="navbar navbar-default mainmenu">
                <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <!-- Collection of nav links and other content for toggling -->
                        <div id="navbarCollapse" class="collapse navbar-collapse">
                            <ul id="fresponsive" class="nav navbar-nav dropdown">
                                <li><a href="<?php echo base_url(); ?>home">Home</a></li>
                                <li><a href="<?php echo base_url(); ?>AboutUs">About Us</a></li>
                                   <li class="dropdown"><a data-toggle="dropdown" class="dropdown-toggle">Women<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li>
                      <a href="#" data-toggle="dropdown" class="dropdown-toggle">Legging<span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url(); ?>products/index/33">WINTER FURLINE LEGGING</a></li>
                        <li><a href="<?php echo base_url(); ?>products/index/34">WINTER FLEECE JEGGINGS</a></li>
                        <li><a href="<?php echo base_url(); ?>products/index/35">YOGA ACTIVE LEGGINGS</a></li>
                        <li><a href="<?php echo base_url(); ?>products/index/36">GENERAL</a></li>
                        <li><a href="<?php echo base_url(); ?>products/index/38">AVH MILKY LEGGINGS</a></li>


                      </ul>
                    </li>

                     <li>
                      <a href="#" data-toggle="dropdown" class="dropdown-toggle">Clothing<span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url(); ?>products/index/20">JACKET</a></li>
                        <li><a href="<?php echo base_url(); ?>products/index/22">PANTIES</a></li>
                        <li><a href="<?php echo base_url(); ?>products/index/21">SWEATSHIRT</a></li>
                        <li><a href="<?php echo base_url(); ?>products/index/23">TOPS</a></li>
                        <li><a href="<?php echo base_url(); ?>products/index/24">TSHIRT</a></li>
                      </ul>
                    </li>

                     <li>
                      <a href="#" data-toggle="dropdown" class="dropdown-toggle">Home Textiles<span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url(); ?>products/index/26">BED SETS</a></li>
                        <li><a href="<?php echo base_url(); ?>products/index/27">PILLOW</a></li>
                        <li><a href="<?php echo base_url(); ?>products/index/28">BEACH TOWEL</a></li>
                      </ul> 
                    </li>
                     <li>
                      <a href="#" data-toggle="dropdown" class="dropdown-toggle">Ligeries<span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url(); ?>products/index/40">SEXY LINGIRIES</a></li>
                      </ul>
                    </li>
                  </ul>
                </li>
                                <li><a href="<?php echo base_url(); ?>category/detail/32">Legging</a></li>
                             
                                <li><a href="<?php echo base_url(); ?>category/detail/29">Accessories</a></li>
                                <li><a href="<?php echo base_url(); ?>Contact">Contact Us</a></li>
                            </ul>
                        </div>
                    </nav>

                  </div>
      
               </div>
            </div>
         </div>
         <!-- <div class="row header-new">
            <div class="col-md-6 left-header-btn">
               <a href="#">
                  NEW STYLES ADDED! UP TO 80% OFF
                  <div class="sub-line">SHOP NOW <span>*see details</span></div>
               </a>
            </div>
            <div class="col-md-6 right-header-btn">
               <a href="#">
                  NEW STYLES ADDED! UP TO 80% OFF
                  <div class="sub-line">SHOP NOW <span>*see details</span></div>
               </a>
            </div>
            </div> -->
      </header>
      <div class="wrapper">