<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('add').' '.lang($ControllerName);?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">


                            <div class="row">
								<div class="col-md-6">
                                    <div class="form-group label-floating">
                                <label class="control-label" for="menu_position"><?php echo lang('menu_position'); ?> <span class="red">*</span></label>
								<select id="menu_position" class="selectpicker" data-style="select-with-transition" required name="menu_position">
									<?php 
									$menu = MenuPosition();
									if(!empty($menu)){ 
											foreach($menu as $key => $result){ ?>
												<option value="<?php echo $key; ?>"><?php echo $result; ?> </option>
									<?php } } ?>
								</select>                                    
									</div>
                                </div>
								
								<div class="col-md-6">
                                    <div class="form-group label-floating">
                                <label class="control-label" for="menu_show_on_selected_page"><?php echo lang('menu_show_on_selected_page'); ?> <span class="red">*</span></label>
								<select id="menu_show_on_selected_page" class="selectpicker" data-style="select-with-transition" required name="show_page">
									<option value="0"><?php echo lang('all'); ?></option>
									<?php 
									$pages = getPages();
									foreach($pages as $val){
										?>
										<option value="<?php echo $val->PageID; ?>"><?php echo $val->Title; ?> </option>
										<?php 
									}

									?>
								</select>                                    
									</div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"><?php echo lang('title'); ?> <span class="red">*</span></label>
                                        <input type="text" name="Title" required  class="form-control" id="Title">
                                    </div>
                                </div>
                            </div>
							<div class="row">
							<div class="col-md-6">
								<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                    <h4 class="panel-title">
													<strong><?php echo lang('pages'); ?></strong>                                                      
														<i class="material-icons">keyboard_arrow_down</i>
                                                    </h4>
                                                </a>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in menu-scroll" role="tabpanel" aria-labelledby="headingOne">
                                               <ul class="page-list">
											<?php 
												$pages = getPages();
												foreach($pages as $val){
													?>
													<li class="draggable"  data-menu-type="1"  data-id="<?php echo $val->PageID; ?>"><span class="menu-remove pull-right">x</span><div><?php echo $val->Title; ?></div></li>
													<?php 
												}

												?>
											</ul>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingTwo">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                    <h4 class="panel-title">
                                                       <strong><?php echo lang('categorys'); ?></strong> 
                                                        <i class="material-icons">keyboard_arrow_down</i>
                                                    </h4>
                                                </a>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse menu-scroll" role="tabpanel" aria-labelledby="headingTwo">
                                                 <ul class="page-list">
											<?php 
												$category = getCategory();
												foreach($category as $cat){
													?>
													<li class="draggable" data-menu-type="2"  data-id="<?php echo $cat->CategoryTextID; ?>"><span class="menu-remove pull-right">x</span><div><?php echo $cat->Title; ?></div></li>
													<?php 
												}

												?>
											</ul>
                                            </div>
                                        </div>
                                    </div>							
							</div>
               				 <div class="col-md-6 dd" id="nestable">
								<h6><strong><?php echo lang('menu_structure'); ?>  <span class="red">*</span></strong></h6>
							<p>Drag each item into the order you prefer.</p>	
								<ol class="dd-list menu-list">
									</ol>
							
							</div>
								</div>
                            <div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group text-right m-b-0">
								<input type="hidden" name="menu" required class="top_menu" value="">
								<input type="hidden" name="menu_type" class="menu_type" value="">

                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>