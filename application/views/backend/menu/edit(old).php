<?php

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if(!empty($languages)){
    foreach($languages as $key => $language){
        $common_fields = $common_fields2 = $menu1= $menu2 = $show1 = $show2 = '';
		if($result[0]->MenuType == 1){
		  $menu1 = 'in';
		  $show1 = 'true';	
		}else{
		  $menu2 = 'in';
		  $show2 = 'true';	
	
		}
        if($key == 0){
        $common_fields2 = '<div class="row">
							<div class="col-md-6">
								<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="'.$show1.'" aria-controls="collapseOne">
                                                    <h4 class="panel-title">
													<strong>'.lang('pages').'</strong>                                                      
														<i class="material-icons">keyboard_arrow_down</i>
                                                    </h4>
                                                </a>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse '.$menu1.'  menu-scroll" data-menutype="1" role="tabpanel" aria-labelledby="headingOne">
                                               <ul class="page-list">';
												$pages = getPages();
												foreach($pages as $val){
													  $common_fields2 .= '<li class="draggable" data-menu-type="1"  data-id="'.$val->PageID.'"><span class="menu-remove pull-right">x</span><div>'.$val->Title.'</div></li>';
												}

											  $common_fields2 .= '</ul>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingTwo">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="'.$show2.'" aria-controls="collapseTwo">
                                                    <h4 class="panel-title">
                                                       <strong>'.lang('categorys').'</strong> 
                                                        <i class="material-icons">keyboard_arrow_down</i>
                                                    </h4>
                                                </a>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse '.$menu2.' menu-scroll" data-menutype="2" role="tabpanel" aria-labelledby="headingTwo">
                                                 <ul class="page-list">';
												$category = getCategory();
												foreach($category as $cat){
													  $common_fields2 .= '<li class="draggable" data-menu-type="2"  data-id="'.$cat->CategoryTextID.'"><span class="menu-remove pull-right">x</span><div>'.$cat->Title.'</div></li>';
												}
											  $common_fields2 .= '</ul>
                                            </div>
                                        </div>
                                    </div>
							</div>
               				 <div class="col-md-6 dd" id="nestable">
								<h6><strong>'.lang('menu_structure').'<span class="red">*</span></strong></h6>
							<p>Drag each item into the order you prefer.</p>
							<ol class="dd-list menu-list">';
								$menu_pages = array();
			                    $menu =  json_decode($result[0]->Menu);
                                $testOl = '';
                                //print_rm($menu);
								foreach ($menu as $li) {
                                    array_push($menu_pages,$li->id);
                                    $testOl = $common_fields2  .=AdminMenu($menu,$li->id,$li->menutype,0);
                            	}
			

									$common_fields2  .='</ol>
								</div>
                                <!-- Here Menu Start:'.$testOl.'-->
								</div>
		<div class="row">
		<div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').'/> '.lang('is_active').'
                                            </label>
                                        </div>
                                    </div>
                                </div>
								</div>
								';
	$common_fields = '<div class="col-md-6">
                                    <div class="form-group label-floating">
                                <label class="control-label" for="menu_position">'.lang('menu_position').' <span class="red">*</span></label>
								<select id="menu_position" class="selectpicker" data-style="select-with-transition" required name="menu_position">';
									
									$menu = MenuPosition();
									if(!empty($menu)){ 
											foreach($menu as $k => $rr){
											$common_fields .= '	<option value="'.$k.'" '.getSelected($k,$result[0]->MenuPosition).'>'.$rr.'</option>';
									 } } 
								$common_fields .= '</select>                                    
									</div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group label-floating">
                                <label class="control-label" for="menu_show_on_selected_page">'.lang('menu_show_on_selected_page').' <span class="red">*</span></label>
								<select id="menu_show_on_selected_page" class="selectpicker" data-style="select-with-transition" required name="show_page">
									<option value="0">'.lang('all').'</option>';
									$pages = getPages();
									foreach($pages as $val){
									$common_fields .= '<option value="'.$val->PageID.'" '.getSelected($val->PageID,$result[0]->ShowOn).'>'.$val->Title.'</option>';
									}

								$common_fields .= '</select>                                    
									</div>
                                </div>';		
        }


        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'">
                                        <a href="#'.$language->SystemLanguageTitle.'" data-toggle="tab">
                                            '.$language->SystemLanguageTitle.'
                                        </a>
                                  </li>';


        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageTitle.'">
                      <form action="'.base_url().'cms/'.$ControllerName.'/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="'.base64_encode($language->SystemLanguageID).'">
                                                    <input type="hidden" name="'.$TableKey.'" value="'.base64_encode($result[0]->$TableKey).'">
                                                    <input type="hidden" name="IsDefault" value="'.$language->IsDefault.'">';
													if($key == 0){
													 $lang_data .= '<input type="hidden" name="menu" required class="top_menu" value=>
													<input type="hidden" name="menu_type" class="menu_type" value="'.$result[0]->MenuType.'">';
													}

                                                  $lang_data .= '<div class="row">
                                                        	'.$common_fields.'

                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title'.$key.'">'.lang('title').'  <span class="red">*</span></label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title'.$key.'" value="'.((isset($result[$key]->Title)) ? $result[$key]->Title : '').'">
                                                               
                                                            </div>
                                                        </div>
														</div>
                                                         '.$common_fields2.'                                                    
                                                    <div class="form-group text-right m-b-0">
                                                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            '.lang('submit').'
                                                        </button>
                                                        <a href="'.base_url().'cms/'.$ControllerName.'">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         '.lang('back').'
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>

                        </div>';

    }
}


?>



<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    
                    <div class="card-header">
                        <h5 class="card-title"><?php echo lang('edit').' '.lang($ControllerName);?></h5>
						<h6 class="card-sub-title"><?php echo lang('card_sub_title'); ?></h6>
                    </div>
                    
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                               
                            </div>
                                <div class="col-md-10">
                                    <div class="tab-content">
                                        <?php echo $lang_data; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>