<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
<!-- Start content -->
<div class="content">
    <div class="container">


        <div class="row">
			<div class="col-xs-12">
				<div class="page-title-box">
                    <h4 class="page-title">Order Detail </h4>
                    <ol class="breadcrumb p-0 m-0">
                        
                    </ol>
                    <div class="clearfix"></div>
                </div>
			</div>
		</div>
        <!-- end row -->


        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <!-- <div class="panel-heading">
                        <h4>Invoice</h4>
                    </div> -->
                    <div class="panel-body">
                        <div class="clearfix">
                            <div class="pull-left">
                                <h3> <?php echo $order_items[0]['first_name'].' '.$order_items[0]['last_name'];?></h3>
                            </div>
                            <div class="pull-right">
                                <h4>Track ID # <br>
                                    <strong><?php echo $order_items[0]['order_track_id'];?></strong>
                                </h4>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">

                                <div class="pull-left m-t-30">
                                    <h3> Shipping Address</h3>
                                    <address>
                                      <strong><?php echo $order_items[0]['shipping_city'];?>, <?php echo $order_items[0]['shipping_state'];?>, <?php echo $order_items[0]['shipping_country'];?></strong><br>
                                      <b>Zip</b> : <?php echo $order_items[0]['shipping_zip_code'];?><br>
                                     <b>Address 1:</b><?php echo $order_items[0]['shipping_address_1'];?><br>
                                     <b>Address 2:</b><?php echo $order_items[0]['shipping_address_1'];?>
                                      </address>
                                </div>
                                <div class="pull-left m-t-30">
                                    <h3> Billing Address</h3>
                                    <address>
                                      <strong><?php echo $order_items[0]['billing_city'];?>, <?php echo $order_items[0]['billing_state'];?>, <?php echo $order_items[0]['billing_country'];?></strong><br>
                                      <b>Zip</b> : <?php echo $order_items[0]['billing_zip_code'];?><br>
                                     <b>Address 1:</b><?php echo $order_items[0]['billing_address_1'];?><br>
                                     <b>Address 2:</b><?php echo $order_items[0]['billing_address_2'];?>
                                      </address>
                                </div>
                                <div class="pull-right m-t-30">
                                    <p><strong>Order Date: </strong> <?php echo date('d-m-Y',strtotime($order_items[0]['order_date']));?></p>
                                    <p><strong>Order Status: </strong> <span class="label label-danger order_status"><?php echo $order_items[0]['status'];?></span></p>
                                    <p><strong>Order ID: </strong> #<?php echo $order_items[0]['order_id'];?></p>
                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- end row -->

                        <div class="m-h-50"></div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table m-t-30">
                                        <thead>
                                            <tr><th>#</th>
                                            <th>Item</th>
                                            
                                            <th>Quantity</th>
                                            <th>Unit Cost</th>
                                            <th>Total</th>
                                        </tr></thead>
                                        <tbody>
                            <?php 
                                            $total = 0;
                                            $ordered_itmes = getOrderItems($order_items[0]['order_id']);
                                            foreach($ordered_itmes as $key => $value){
                                                $total = $total + ($value['palce_order_price']  * $value['quantity']);?>
                                            <tr>
                                                <td><?php echo $key +1 ; ?></td>
                                                <td><a href="<?php echo base_url('cms/product/edit/'.$value['product_id']);?>" target="_blank"><?php echo $value['product_title_en'];?></a></td>
                                                
                                                <td><?php echo $value['quantity']; ?></td>
                                                <td>SAR <?php echo $value['palce_order_price'];?></td>
                                                <td>SAR <?php echo ($value['palce_order_price']  * $value['quantity']);?></td>
                                            </tr>
                            <?php } ?>                    
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="clearfix m-t-40">
                                    <h5 class="small text-inverse font-600">PAYMENT TERMS AND POLICIES</h5>

                                    <small>
                                        All accounts are to be paid within 7 days from receipt of
                                        invoice. To be paid by cheque or credit card or direct payment
                                        online. If account is not paid within 7 days the credits details
                                        supplied as confirmation of work undertaken will be charged the
                                        agreed quoted fee noted above.
                                    </small>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-6 col-md-offset-3">
                                <p class="text-right"><b>Sub Total:</b> <?php echo $total; ?> SAR</p>
                                <!--<p class="text-right">Discout: 12.9%</p>
                                <p class="text-right">VAT: 12.9%</p>
                                <hr>
                                <h3 class="text-right">USD 2930.00</h3> -->
                            </div>
                        </div>
                        <hr>
                        <div class="hidden-print">
                            <div class="pull-right">
                                <?php
                                if ($this->session->userdata['admin']['role_id'] != 2 && $order_items[0]['status'] == 'Dispatched') { ?>
                                    <button type="button" class="btn btn-success waves-effect" onclick="markAsDelivered(<?php echo $order_items[0]['order_id'];?>);">Order delivered to customer</button>
                                <?php } ?>
                                <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i></a>
                                <!--<a href="#" class="btn btn-primary waves-effect waves-light">Submit</a>-->
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <!-- end row -->



    </div> <!-- container -->

</div> <!-- content -->