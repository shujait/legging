<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="card">
                    <div class="card-header card-header-text" data-background-color="orange">
                        <h4 class="card-title">Order Track ID: <?php echo $order_items[0]['order_track_id'];?></h4>
                        <p class="category">Total Amount: <?php echo getSelectedCurrencies($order_items[0]['currency_id'], $order_items[0]['amount_deposit']);?></p>
                    </div>
                    <div class="card-content table-responsive">
                        <table class="table table-hover">
                            <thead class="text-warning">
                                <th>Product Name</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Detail</th>
                            </thead>
                            <tbody>
                                <?php 
                                foreach ($order_items as $key => $value) {
                                ?>
                                    <tr>
                                        <td><?php echo $value['Title'];?></td>
                                        <td><?php echo $value['quantity'];?></td>
                                        <td><?php echo getSelectedCurrencies($value['currency_id'], $value['price']);?></td>
                                        <td><a href="javascript:void(0);" data-toggle="modal" data-target="#orderDetail<?php echo $value['order_item_id'] ?>" class="btn btn-simple btn-warning btn-icon show_detail_modal"><i class="material-icons" title="Detail">visibility</i><div class="ripple-container"></div></a></td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="card">
                    <div class="card-header card-header-tabs" data-background-color="rose">
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">
                                <span class="nav-tabs-title">Billing Details</span>
                                <ul class="nav nav-tabs" data-tabs="tabs">
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>Address 1:</th>
                                    <td>
                                        <?php echo $order_items[0]['billing_address_1'];?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Address 2:</th>
                                    <td>
                                        <?php echo $order_items[0]['billing_address_2'];?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Country:</th>
                                    <td>
                                        <?php 
                                        $Country = CountryList($order_items[0]['billing_country']);
                                        echo $Country->Title;
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>State:</th>
                                    <td>
                                        <?php 
                                        $State = GetState($order_items[0]['billing_state']);
                                        echo $State->Title;
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>City:</th>
                                    <td>
                                        <?php 
                                        $City = GetCity($order_items[0]['billing_city']);
                                        echo $City->Title;?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Zip Code:</th>
                                    <td>
                                        <?php echo $order_items[0]['billing_zip_code'];?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-12">
                <div class="card">
                    <div class="card-header card-header-tabs" data-background-color="rose">
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">
                                <span class="nav-tabs-title">Other Details</span>
                                <ul class="nav nav-tabs" data-tabs="tabs">
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>Order Status:</th>
                                    <td>
                                        <?php 
                                        if($this->session->userdata['admin']['RoleID'] == 1 || $this->session->userdata['admin']['RoleID'] == 2)
                                        {
                                        ?>
                                            <select class="order_status" data-order-id = "<?php echo $order_items[0]['order_id'];?>">
                                                <option value="Received" <?php echo ($order_items[0]['status'] == 'Received' ? 'selected' : ''); ?>>Received</option>
                                                <option value="Dispatched" <?php echo ($order_items[0]['status'] == 'Dispatched' ? 'selected' : ''); ?>>Dispatched</option>
                                                <option value="Delivered" <?php echo ($order_items[0]['status'] == 'Delivered' ? 'selected' : ''); ?>>Delivered</option>
                                            </select>
                                        <?php 
                                        }
                                        else{
                                            echo $order_items[0]['status'];
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Order Date:</th>
                                    <td>
                                        <?php echo date('d/m/Y', strtotime($order_items[0]['created_at']));?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Payment Method:</th>
                                    <td>
                                        <?php echo $order_items[0]['payment_method'];?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Invoice Id:</th>
                                    <td>
                                        <?php echo $order_items[0]['invoice_no'];?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Transaction Id:</th>
                                    <td>
                                        <?php echo $order_items[0]['transaction_id'];?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Amount Deposit:</th>
                                    <td>
                                        $<?php echo $order_items[0]['amount_deposit'];?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="card">
                    <div class="card-header card-header-tabs" data-background-color="rose">
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">
                                <span class="nav-tabs-title">Shipping Details</span>
                                <ul class="nav nav-tabs" data-tabs="tabs">
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>Address 1:</th>
                                    <td>
                                        <?php echo $order_items[0]['shipping_address_1'];?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Address 2:</th>
                                    <td>
                                        <?php echo $order_items[0]['shipping_address_2'];?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Country:</th>
                                    <td>
                                        <?php 
                                        $Country = CountryList($order_items[0]['shipping_country']);
                                        echo $Country->Title;
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>State:</th>
                                    <td>
                                        <?php 
                                        $State = GetState($order_items[0]['shipping_state']);
                                        echo $State->Title;
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>City:</th>
                                    <td>
                                        <?php 
                                        $City = GetCity($order_items[0]['shipping_city']);
                                        echo $City->Title;?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Zip Code:</th>
                                    <td>
                                        <?php echo $order_items[0]['shipping_zip_code'];?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php 
foreach ($order_items as $key => $value) {
?>

<div class="modal fade" id="orderDetail<?php echo $value['order_item_id'] ?>" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                <h4 class="modal-title custom_align" id="Heading"><?php echo $value['Title'];?></h4>
            </div>
            <div class="modal-body">
                <?php if($value['colour'] == '' && $value['size'] == ''){?>
                    <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr><th>Size</th><th>Color</th><th>Pieces</th></tr>
                        </thead>
                        <tbody>
                        <?php 
                        $getArray = json_decode($value['PacketData']);
                        foreach ($getArray as $singPack) {

                            $sizeDet = getSizeDetail($singPack->size);
                            $colorDet = getColorDetail($singPack->color);
                        ?>
                           <tr><td><?php echo $sizeDet['Title']; ?></td><td><?php echo $colorDet['Title'];?></td><td><?php echo $singPack->piece; ?></td></tr>
                        <?php 
                        }
                        ?>
                        </tbody>
                    </table>
                <?php 
                }else{
                ?>
                    <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr><th>Size</th><th>Color</th></tr>
                        </thead>
                        <tbody>
                            <tr><td><?php echo $value['size']; ?></td><td><?php echo $value['colour'];?></td></tr>
                        </tbody>
                    </table>
                <?php 
                }
                ?>
            </div>
            <div class="modal-footer ">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog --> 
</div>
<?php 
}
?>

<script>
    $( document ).ready(function() {
            $('.order_status').on('change',function(){
                    if (confirm("Are you sure you want to change status ?")) {
                        $.blockUI({
                            css: {
                                border: 'none',
                                padding: '15px',
                                backgroundColor: '#000',
                                '-webkit-border-radius': '10px',
                                '-moz-border-radius': '10px',
                                opacity: .5,
                                color: '#fff'
                            }
                        });

                        $.ajax({
                            type: "POST",
                            url: base_url + 'cms/myorder/action',
                            data: {
                                'id': $(this).attr('data-order-id'),
                                'form_type': 'change_status',
                                'value' : $(this).val()
                            },
                            dataType: "json",
                            cache: false,
                            //async:false,
                            success: function (result) {

                                if (result.error != false) {
                                    showError(result.error);
                                } else {
                                    showSuccess(result.success);
                                    
                                }


                                

                            },
                            complete: function () {
                                $.unblockUI();
                            }
                        });
                        return true;
                    } else {
                        return false;
                    }
            });
    });
</script>