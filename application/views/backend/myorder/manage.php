<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang($ControllerName.'s'); ?></h4>
                        <?php if(checkUserRightAccess(54,$this->session->userdata['admin']['UserID'],'CanAdd')){?>
                            <div class="toolbar">
                                
                            </div>
                        <?php 
                        }
                        ?>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                    <th>Order Track ID</th>

                                    <th>Name</th>

                                    <th>Email</th>

                                    <th>Shipping Address</th>

                                    <th>Order Status</th>

                                    <th><?php echo lang('actions');?></th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                    foreach($results as $value){ ?>
                                        <tr id="<?php echo $value['order_id'];?>">

                                            <td><?php echo $value['order_track_id']; ?></td>

                                            <td><?php echo $value['first_name'].' '.$value['last_name']; ?></td>

                                            <td><?php echo $value['email']; ?></td>

                                            <td><?php echo $value['shipping_address_1']; ?></td>

                                            <td>
                                                <?php 
                                                if($this->session->userdata['admin']['RoleID'] == 1 || $this->session->userdata['admin']['RoleID'] == 2)
                                                {
                                                ?>
                                                    <select class="order_status" data-order-id = "<?php echo $value['order_id'];?>">
                                                        <option value="Received" <?php echo ($value['status'] == 'Received' ? 'selected' : ''); ?>>Received</option>
                                                        <option value="Dispatched" <?php echo ($value['status'] == 'Dispatched' ? 'selected' : ''); ?>>Dispatched</option>
                                                        <option value="Delivered" <?php echo ($value['status'] == 'Delivered' ? 'selected' : ''); ?>>Delivered</option>
                                                    </select>
                                                <?php 
                                                }
                                                else{
                                                    echo $value['status'];
                                                }
                                                ?>
                                            </td>



                                            <td><a href="<?php echo base_url('cms/'.$ControllerName.'/view/'.$value['order_id']);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Detail">visibility</i><div class="ripple-container"></div></a></td>

                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>
<script>
    $( document ).ready(function() {
            $('.order_status').on('change',function(){
                    if (confirm("Are you sure you want to change status ?")) {
                        $.blockUI({
                            css: {
                                border: 'none',
                                padding: '15px',
                                backgroundColor: '#000',
                                '-webkit-border-radius': '10px',
                                '-moz-border-radius': '10px',
                                opacity: .5,
                                color: '#fff'
                            }
                        });

                        $.ajax({
                            type: "POST",
                            url: base_url + 'cms/myorder/action',
                            data: {
                                'id': $(this).attr('data-order-id'),
                                'form_type': 'change_status',
                                'value' : $(this).val()
                            },
                            dataType: "json",
                            cache: false,
                            //async:false,
                            success: function (result) {

                                if (result.error != false) {
                                    showError(result.error);
                                } else {
                                    showSuccess(result.success);
                                    
                                }


                                

                            },
                            complete: function () {
                                $.unblockUI();
                            }
                        });
                        return true;
                    } else {
                        return false;
                    }
            });
    });
</script>