<div class="content" ng-controller="CustomController">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
						<form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
 						<input type="hidden" name="form_type" value="save">
                            <div class="col-md-5">
                            <h4 class="card-title"><?php echo lang('add').' '.lang($ControllerName);?></h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"><?php echo lang('title'); ?> <span class="red">*</span></label>
                                        <input type="text" name="Title" required ng-model="title"  class="form-control" id="Title">
                                    </div>
                                </div>
                            </div>
							<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="icon"><?php echo lang('icon'); ?></label>
                                        <input type="text" name="icon"  class="form-control" id="icon">
                                    </div>
                                </div>
                            </div>
							<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="url"><?php echo lang('url'); ?> <span class="red">*</span></label>
                                        <input type="text" name="url" required class="form-control" id="url" value="{{title}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
								 </div>
							<div class="col-md-5">
								  <h4 class="card-title"><?php echo lang('seo_setting');?></h4>
								<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="seo_title"><?php echo lang('seo_title'); ?></label>
                                        <input type="text" name="seo_title" class="form-control" id="seo_title">
                                    </div>
                                </div>
                            </div>
								<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="keywords"><?php echo lang('keywords'); ?></label>
                                        <input type="text" name="keywords" class="form-control" id="keywords">
                                    </div>
                                </div>
                            </div>
								<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="meta_desc"><?php echo lang('meta_desc'); ?></label>
                                        <input type="text" name="meta_desc" class="form-control" id="meta_desc">
                                    </div>
                                </div>
                            </div>
								<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="crawl_after_day"><?php echo lang('crawl_after_day'); ?></label>
                                        <input type="text" name="crawl_after_day" class="form-control" id="crawl_after_day">
                                    </div>
                                </div>
                            </div>
								<div class="row">
                                <div class="col-md-12">
									  <label class="col-sm-2 margin-top-10 padding-left-0"><?php echo lang('follow'); ?></label>
									<div class="col-sm-10">
										<div class="radio">
                                                    <label>
                                                        <input type="radio" name="follow" value="follow" checked="true"> <?php echo lang('yes'); ?>
                                                    </label>
                                                    <label>
                                                        <input type="radio" name="follow" value="nofollow"> <?php echo lang('no'); ?>
                                                    </label>
                                                </div> 
										</div>
                                </div>
                            </div>
								<div class="row">
                                <div class="col-md-12">
                                        <label class="col-sm-2 margin-top-10 padding-left-0"><?php echo lang('index'); ?></label>
									<div class="col-sm-10">
										<div class="radio">
                                                    <label>
                                                        <input type="radio" name="index" value="index" checked="true"> <?php echo lang('yes'); ?>
                                                    </label>
                                                    <label>
                                                        <input type="radio" name="index" value="noindex"> <?php echo lang('no'); ?>
                                                    </label>
                                                </div> 
										</div>
                                </div>
                            </div>
								</div>
							<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
										<a href="javascript:void(0)" onClick="show_popup();"><?php echo lang('add_media'); ?></a>
                                    </div>
                                </div>
                            </div>
						    <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="content"><?php echo lang('content'); ?> <span class="red">*</span></label>
										<br />
										<br />
										<textarea class="form-control" required id="content" name="content"></textarea>
                                    </div>
                                </div>
                            </div>
								 <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
						<div class="clearfix"></div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>