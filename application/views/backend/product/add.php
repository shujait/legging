<style type="text/css">
    .cal_val{
        font-size: 16px;
        font-weight: 900;
        float: right;
        color: #333;
    }
</style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('add').' '.lang($ControllerName);?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">

                            
                            <div class="row">
                                
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"><?php echo lang('title'); ?></label>
                                        <input type="text" name="Title" required  class="form-control" id="Title">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="InitialCost">Initial Cost</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" name="InitialCost" required  class="form-control" id="InitialCost">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">                              
                                 <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Price"><?php echo lang('price'); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" name="Price" required  class="form-control" id="Price">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="PriceForWholeSaler"><?php echo lang('price_whole_saler'); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" name="PriceForWholeSaler" required  class="form-control" id="PriceForWholeSaler">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Discount">Discount (in percentage)</label>
                                        <div class="input-group">
                                            <input type="text" name="Discount" class="form-control" id="Discount">
                                            <span class="input-group-addon">%</span>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="WholeSaleDiscount">Discount For Whole saler (in percentage)</label>
                                        <div class="input-group">
                                            <input type="text" name="WholeSaleDiscount" class="form-control" id="WholeSaleDiscount">
                                            <span class="input-group-addon">%</span>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="InStock">Number of pieces in Stock</label>
                                        <input type="text" name="InStock" required  class="form-control" id="InStock">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="CategoryID"  data-level = "1" class="form-control selectpicker CategoryID" id="CategoryID" required>
                                            <option value=""><?php echo lang('choose_category');?></option>
                                            <?php foreach ($categories as $key => $value) { ?>
                                                <option value="<?php echo $value->CategoryID; ?>"><?php echo $value->Title; ?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="SubCategoryID" class="form-control selectpicker CategoryID" data-level = "2" id="SubCategoryID" required>
                                            <option value=""><?php echo lang('choose_sub_category');?></option>
                                        </select>
                                    </div>
                                </div>
                                 <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="SubCategoryIDLevel2" class="form-control selectpicker"  id="SubCategoryIDLevel2" required>
                                            <option value=""><?php echo lang('choose_sub_category');?> Level 2</option>
                                        </select>
                                    </div>
                                </div>
                                
                            </div>
                            
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group m-b-20">
                                        <label><?php echo lang('Image'); ?> (Press Ctrl key to select multiple)</label><br>
                                        <input type="file" name="Image[]" id="filer_input1" required>
                                    </div>
                                </div>
                            </div>
                           

 


                           <div class="row">
                           <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">&nbsp;</label>
                                    <select name="Sizes[]" class="form-control selectpicker" multiple required>
                                        <option value="">Select Sizes</option>
                                        <?php foreach ($sizes as $key => $value) { ?>
                                            <option  value="<?php echo $value->SizeID; ?>"><?php echo $value->Title; ?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div> 
                               
                            <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="Colors[]" class="form-control selectpicker" multiple required>
                                            <option value="">Select Colors</option>
                                            <?php foreach ($colours as $key => $value) { ?>
                                                <option  value="<?php echo $value->ColourID; ?>"><?php echo $value->Title; ?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div> 
                             </div>              
                                      
                           
                        
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="ShortDescription"><?php echo lang('short_description'); ?></label>
                                        <textarea class="form-control" name="ShortDescription" id="ShortDescription"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="Description"><?php echo lang('description'); ?></label>
                                        <textarea class="form-control" name="Description" id="Description" style="height: 100px;"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="OnlyForWholeSale">
                                                <input name="OnlyForWholeSale" value="1" type="checkbox" id="OnlyForWholeSale"/> Only for Whole saler
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="FeaturedProduct">
                                                <input name="FeaturedProduct" value="1" type="checkbox" id="FeaturedProduct"/> <?php echo lang('feature_product'); ?>?
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                               
                            </div>


                            <h4 class="card-title">Products Packets
                                <div>
                                    <a href="javascript:void()" onclick="add()">
                                        <i class="material-icons">add_circle</i>
                                    </a>
                                </div>
                            </h4>
                            
                            <div class="morePackets">
                            </div>
                            
                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<div id="newpackets" style="display: none;">
    <div class="row newPacketDet">
        <div class="col-md-3">
            <div class="form-group label-floating">
                <select name="PacketIDs[]"  data-level = "1" class="form-control PacketID" required>
                    <option value=""><?php echo lang('select').' '.lang('packets');?></option>
                    <?php foreach ($packets as $key => $value) { ?>
                        <option value="<?php echo $value->Pieces; ?>"><?php echo $value->Title; ?></option>
                    <?php }?>
                </select>
            </div>
        </div>
        <div class="col-md-1">
            <a href="javascript:void()" class="delRow">
                <i class="material-icons">remove_circle</i>
            </a>
        </div>
        <div class="packetVariations">
        </div>
    </div>
</div>
<div class="variationfield" style="display: none;">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group label-floating">
                <select class="form-control packetSizes">
                    <?php foreach ($sizes as $key => $value) { ?>
                        <option  value="<?php echo $value->SizeID.'-'.$value->Title; ?>"><?php echo $value->Title; ?></option>
                    <?php }?>
                </select>
            </div>
        </div> 
           
        <div class="col-md-4">
            <div class="form-group label-floating">
                <select class="form-control packetColors">
                    <?php foreach ($colours as $key => $value) { ?>
                        <option  value="<?php echo $value->ColourID.'-'.$value->Title; ?>"><?php echo $value->Title; ?></option>
                    <?php }?>
                </select>
            </div>
        </div> 
        <div class="col-md-3">
            <div class="form-group label-floating">
                <select class="form-control noofpieces packetPieces" onchange="recalculatePiece(this.value);">
                </select>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="packetVariationModal" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button> -->
                <h4 class="modal-title custom_align" id="Heading">Packet Variations</h4>
            </div>
            <div class="modal-body">
                
            </div>
            <div class="modal-footer ">
                <input type="hidden" id="totalPieces" value="">
                <button type="button" class="btn btn-default makePackets">Done</button>
            </div>
        </div>
        <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog --> 
</div>