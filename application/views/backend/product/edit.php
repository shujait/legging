<?php

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
$images = '';
$common_size_file = '';
$options = '';
$options2 = '';
$product_packet_fields = '';
$slected_color = array();
$selected_size = array();
if($result[0]->Colors != ''){
    $slected_color = explode(',',$result[0]->Colors);
}
if($result[0]->Sizes != ''){
    $selected_size = explode(',',$result[0]->Sizes);
}
if($colours){
        foreach ($colours as $key => $value) 
        {
            $options .= '<option  value="'.$value->ColourID.'" '.(in_array($value->ColourID, $slected_color) ? 'selected' : '').'>'.$value->Title.'</option>';
        }                                        

}
if($sizes){

        foreach ($sizes as $key => $size) 
        {
            $options2 .= '<option  value="'.$size->SizeID.'" '.(in_array($size->SizeID, $selected_size) ? 'selected' : '').'>'.$size->Title.'</option>';
        }                                        

}

if(!empty($product_packets))
{
    foreach($product_packets as $packs)
    {
        $product_packet_fields .= '<div class="row newPacketDet" id="'.$packs['ProductPacketID'].'">
        <div class="col-md-3">
            <div class="form-group label-floating">
                <select name="PacketIDs[]" data-level="1" class="form-control PacketID" required="">
                    <option value="">Select Packets</option>';

            foreach ($packets as $key => $value) {

                $product_packet_fields .= '<option '.($value->Pieces == $packs['TotalPackets'] ? 'selected="selected"' : '').' value="'.$value->Pieces.'">'.$value->Title.'</option>';
            }
        $product_packet_fields .= '</select>
            <span class="material-input"></span></div>
        </div>
        <div class="col-md-1">
            <a href="unsafe:javascript:void()" class="delRow">
                <i class="material-icons">remove_circle</i>
            </a>
        </div>
        <div class="packetVariations">
        </div>
    <div class="col-md-8 tableDiv">
    <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
        <thead>
            <tr><th>Size</th><th>Color</th><th>Pieces</th></tr>
        </thead>
        <tbody>';
        $getArray = json_decode($packs['PacketsData']);
        foreach ($getArray as $singPack) {

            $sizeDet = getSizeDetail($singPack->size);
            $colorDet = getColorDetail($singPack->color);

            $product_packet_fields .= '<tr><td>'.$sizeDet['Title'].'</td><td>'.$colorDet['Title'].'</td><td>'.$singPack->piece.'</td></tr>';
        }
    $product_packet_fields .= '</tbody>
    </table>';
    $product_packet_fields .= "<input type='hidden' name='packetsJson[]' value='".$packs['PacketsData']."'></div></div>";
    }
}



if (!empty($site_images)) {

    $images .= '<div class="form-group clearfix">
       <div class="col-sm-12 padding-left-0 padding-right-0">
          <div class="jFiler jFiler-theme-dragdropbox">
             <div class="jFiler-items jFiler-row">
                <ul class="jFiler-items-list jFiler-items-grid">';
                    foreach($site_images as $img){
                   $images .= '<li class="jFiler-item ArticleImage" data-jfiler-index="1" style="" id="img-'.$img['SiteImageID'].'">
                      <div class="jFiler-item-container">
                         <div class="jFiler-item-inner">
                            <div class="jFiler-item-thumb">
                               <div class="jFiler-item-status"></div>
                               <div class="jFiler-item-info"></div>
                               <div class="jFiler-item-thumb-image"><img src="' . (file_exists($img['ImageName']) ? base_url() . $img['ImageName'] : base_url('uploads/no_image.png')) . '" draggable="false"></div>
                            </div>
                            <div class="jFiler-item-assets jFiler-row">
                               <ul class="list-inline pull-left">
                                  <li><span class="jFiler-item-others"><i class="icon-jfi-file-image jfi-file-ext-jpg"></i></span></li>
                               </ul>
                               <ul class="list-inline pull-right">
                                  <li><a class="icon-jfi-trash jFiler-item-trash-action remove_image" data-image-id="'. $img['SiteImageID'] .'" data-image-path="'. $img['ImageName'] .'"></a></li>
                               </ul>
                            </div>
                         </div>
                      </div>
                   </li>';
                     } 
                   
$images .= '</ul>
             </div>
          </div>
       </div>
    </div>';

    
}


if(!empty($languages)){
    foreach($languages as $key => $language){

        
        $common_fields = '';
        $common_fields2 = '';
        $category_dropdown = '';
        $sub_category_dropdown = '';
        $sub_category_level2_dropdown = '';
        
        $common_no_fields = '';
        $common_check_fields = '';
        $common_fields_whole = '';
        $common_field_price = '';
        if($key == 0){
            
            foreach($categories as $category)
            {
                $category_dropdown .= '<option '.($category->CategoryID == $result[$key]->CategoryID ? 'selected="selected"' : '').' value="'.$category->CategoryID.'">'.$category->Title.'</option>';
            }
            foreach($subcategories as $SubCategory)
            {
                $sub_category_dropdown .= '<option '.($SubCategory->CategoryID == $result[$key]->SubCategoryID ? 'selected="selected"' : '').' value="'.$SubCategory->CategoryID.'">'.$SubCategory->Title.'</option>';
            }
            foreach($subcategorieslevel2 as $SubCategoryLevel2)
            {
                $sub_category_level2_dropdown .= '<option '.($SubCategoryLevel2->CategoryID == $result[$key]->SubCategoryIDLevel2 ? 'selected="selected"' : '').' value="'.$SubCategoryLevel2->CategoryID.'">'.$SubCategoryLevel2->Title.'</option>';
            }

            
        $common_fields2 = '<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">'.lang('choose_category').'</label>
                                        <select name="CategoryID" class="form-control selectpicker CategoryID" data-level = "1" id="CategoryID" required>
                                            <option value="">'.lang('choose_category').'</option>
                                            '.$category_dropdown.'
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">'.lang('choose_sub_category').'</label>
                                        <select name="SubCategoryID" class="form-control selectpicker CategoryID" data-level = "2" id="SubCategoryID" required>
                                            <option value="">'.lang('choose_sub_category').'</option>
                                            '.$sub_category_dropdown.'
                                        </select>
                                    </div>
                                </div>
                                 <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">'.lang('choose_sub_category').' Level 2</label>
                                        <select name="SubCategoryIDLevel2" class="form-control selectpicker"  id="SubCategoryIDLevel2" required>
                                            <option value="">'.lang('choose_sub_category').' Level 2</option>
                                            '.$sub_category_level2_dropdown.'
                                        </select>
                                    </div>
                                </div>
                            </div>';


          $common_check_fields = '<div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="OnlyForWholeSale">
                                                <input name="OnlyForWholeSale" value="1" type="checkbox" id="OnlyForWholeSale" '.((isset($result[$key]->OnlyForWholeSale) && $result[$key]->OnlyForWholeSale == 1) ? 'checked' : '').'/> Only for Whole saler?
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').'/> '.lang('is_active').'
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="FeaturedProduct">
                                                <input name="FeaturedProduct" value="1" type="checkbox" id="FeaturedProduct" '.((isset($result[$key]->FeaturedProduct) && $result[$key]->FeaturedProduct == 1) ? 'checked' : '').'/> '.lang('feature_product').'?
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>'; 


            $common_size_file = '<div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Sizes</label>
                                            <select name="Sizes[]" class="form-control selectpicker" multiple required>
                                                '.$options2.'
                                            </select>
                                        </div>
                                    </div>  
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Colors</label>
                                            <select name="Colors[]" class="form-control selectpicker" multiple required>
                                                '.$options.'
                                            </select>
                                        </div>
                                    </div>                    
                                </div>'; 
            $common_fields_whole = '<div class="row">
            
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Price">'.lang('price').'</label>
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">$</span>
                                                                    <input type="text" name="Price" required  class="form-control" id="Price" value="'.((isset($result[$key]->Price)) ? $result[$key]->Price : '').'">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                                <div class="form-group label-floating">
                                                                    <label class="control-label" for="PriceForWholeSaler">'.lang('price_whole_saler').'</label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        <input type="text" name="PriceForWholeSaler" required  class="form-control" id="PriceForWholeSaler" value="'.((isset($result[$key]->PriceForWholeSaler)) ? $result[$key]->PriceForWholeSaler : '').'">
                                                                    </div>
                                                                </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Discount">Discount (in percentage)</label>
                                                                <div class="input-group">
                                                                    <input type="text" name="Discount" class="form-control" id="Discount" value="'.((isset($result[$key]->Discount)) ? $result[$key]->Discount : '').'">
                                                                    <span class="input-group-addon">%</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="WholeSaleDiscount">Discount For Whole saler (in percentage)</label>
                                                                <div class="input-group">
                                                                    <input type="text" name="WholeSaleDiscount" class="form-control" id="WholeSaleDiscount" value="'.((isset($result[$key]->WholeSaleDiscount)) ? $result[$key]->WholeSaleDiscount : '').'">
                                                                    <span class="input-group-addon">%</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="InStock">Number of pieces in Stock</label>
                                                                <input type="text" name="InStock" required  class="form-control" id="InStock" value="'.((isset($result[$key]->InStock)) ? $result[$key]->InStock : '').'">
                                                            </div>
                                                        </div>
                                                    </div>'; 
            $common_field_price = ' <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="InitialCost">Initial Cost</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">$</span>
                                                <input type="text" name="InitialCost" required  class="form-control" id="InitialCost" value="'.((isset($result[$key]->InitialCost)) ? $result[$key]->InitialCost : '').'">
                                            </div>
                                        </div>
                                    </div>';                                                                                           
       

        

       
        }

        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'">
                                        <a href="#'.$language->SystemLanguageTitle.'" data-toggle="tab">
                                            '.$language->SystemLanguageTitle.'
                                        </a>
                                  </li>';


        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageTitle.'">
                      <form action="'.base_url().'cms/'.$ControllerName.'/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="'.base64_encode($language->SystemLanguageID).'">
                                                    <input type="hidden" name="'.$TableKey.'" value="'.base64_encode($result[0]->$TableKey).'">
                                                    <input type="hidden" name="IsDefault" value="'.$language->IsDefault.'">
                                                    
                                                   
                                                    <div class="row">
                                                       
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title'.$key.'">'.lang('title').'</label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title'.$key.'" value="'.((isset($result[$key]->Title)) ? $result[$key]->Title : '').'">
                                                               
                                                            </div>
                                                        </div>

                                                        '.$common_field_price.'
                                                        
                                                    </div>
                                                    '.$common_fields_whole.'
                                                    
                                                         '.$common_fields2.'

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group m-b-20">
                                                                <label>'.lang('Image').'</label><br>
                                                                <input type="file" name="Image[]" id="filer_input1" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        '.$images.'
                                                    </div>
                                                   
                                                    
                                                   '.$common_size_file.'
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="ShortDescription">'.lang('short_description').'</label>
                                                                <textarea class="form-control" name="ShortDescription" id="ShortDescription">'.$result[$key]->ShortDescription.'</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Description">'.lang('description').'</label>
                                                                <textarea class="form-control" name="Description" id="Description" style="height: 100px;">'.$result[$key]->Description.'</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    '.$common_check_fields.'

                                                    <h4 class="card-title">Products Packets
                                                        <div>
                                                            <a href="javascript:void()" onclick="add()">
                                                                <i class="material-icons">add_circle</i>
                                                            </a>
                                                        </div>
                                                    </h4>
                                                    
                                                    <div class="morePackets">
                                                    '.$product_packet_fields.'
                                                    </div>

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            '.lang('submit').'
                                                        </button>
                                                        <a href="'.base_url().'cms/'.$ControllerName.'">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         '.lang('back').'
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';
        
        
        
        
        
    }
}


?>

<div class="modal fade" id="DeleteArticleImage" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                <h4 class="modal-title custom_align" id="Heading"><?php echo lang('delete'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> <?php echo lang('AreYouSureDeleteFile'); ?></div>
            </div>
            <div class="modal-footer ">
                <a type="button" class="btn btn-success delete_url" ><span class="glyphicon glyphicon-ok-sign"></span> <?php echo lang('Yes'); ?></a>
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> <?php echo lang('No'); ?></button>
            </div>
        </div>
        <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog --> 
</div>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    
                    <div class="card-header">
                        <h5 class="card-title"><?php echo lang('edit').' '.lang($ControllerName);?></h5>
                    </div>
                    
                    <div class="card-content">
                        <div class="row">
                             <?php if(count($languages) > 1){?>
                            <div class="col-md-2">
                                
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                               
                            </div>
                            <div class="col-md-12">
                              <?php }else{ ?>
                                <div class="col-md-12">
                                  <?php } ?>
                                    <div class="tab-content">
                                        <?php echo $lang_data; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="newpackets" style="display: none;">
    <div class="row newPacketDet">
        <div class="col-md-3">
            <div class="form-group label-floating">
                <select name="PacketIDs[]"  data-level = "1" class="form-control PacketID" required>
                    <option value=""><?php echo lang('select').' '.lang('packets');?></option>
                    <?php foreach ($packets as $key => $value) { ?>
                        <option value="<?php echo $value->Pieces; ?>"><?php echo $value->Title; ?></option>
                    <?php }?>
                </select>
            </div>
        </div>
        <div class="col-md-1">
            <a href="javascript:void()" class="delRow">
                <i class="material-icons">remove_circle</i>
            </a>
        </div>
        <div class="packetVariations">
        </div>
    </div>
</div>
<div class="variationfield" style="display: none;">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group label-floating">
                <select class="form-control packetSizes">
                    <?php foreach ($sizes as $key => $value) { ?>
                        <option  value="<?php echo $value->SizeID.'-'.$value->Title; ?>"><?php echo $value->Title; ?></option>
                    <?php }?>
                </select>
            </div>
        </div> 
           
        <div class="col-md-4">
            <div class="form-group label-floating">
                <select class="form-control packetColors">
                    <?php foreach ($colours as $key => $value) { ?>
                        <option  value="<?php echo $value->ColourID.'-'.$value->Title; ?>"><?php echo $value->Title; ?></option>
                    <?php }?>
                </select>
            </div>
        </div> 
        <div class="col-md-3">
            <div class="form-group label-floating">
                <select class="form-control noofpieces packetPieces" onchange="recalculatePiece(this.value);">
                </select>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="packetVariationModal" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button> -->
                <h4 class="modal-title custom_align" id="Heading">Packet Variations</h4>
            </div>
            <div class="modal-body">
                
            </div>
            <div class="modal-footer ">
                <input type="hidden" id="totalPieces" value="">
                <button type="button" class="btn btn-default makePackets">Done</button>
            </div>
        </div>
        <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog --> 
</div>

<script type="text/javascript">
    $(document).on('click', '.remove_image', function () {
        var image_id       = $(this).attr('data-image-id');
        var image_path       = $(this).attr('data-image-path');
        $(".delete_url").attr('data-modal-image-id', image_id);
        $(".delete_url").attr('data-modal-image-path', image_path);
        $('#DeleteArticleImage').modal('show');
    });

    $(document).on('click', '.delete_url', function () {
        var image_id       = $(this).attr('data-modal-image-id');
        var image_path       = $(this).attr('data-modal-image-path');
        var $this = $(this);
        $.ajax({
                type: "POST",
                url: '<?php echo base_url() . 'cms/' . $ControllerName . '/DeleteImage'; ?>',
                data: {
                    image_path: image_path,
                    image_id: image_id
                },
                success: function (result) {
                    $.unblockUI;
                    if (result.error != false) {
                        $("#img-"+image_id).remove();
                    }



                },
                complete: function () {
                    $('#DeleteArticleImage').modal('hide');
                    $.unblockUI();
                }
            });
    });
   
</script>