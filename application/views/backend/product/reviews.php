<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Reviews</h4>
                        <div class="toolbar">
                           
                        </div>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                   
                                   
                                    <th>User Name</th>
                                    <th>Description</th>
                                    <th>Review Rating</th>
                                    


                                    <?php if(checkUserRightAccess(45,$this->session->userdata['admin']['UserID'],'CanDelete')){?>  
                                            <th><?php echo lang('actions');?></th>
                                     <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                   
                                    foreach($results as $value){ ?>
                                        <tr id="<?php echo $value['ReviewID'];?>">
                                            
                                           
                                            <td><?php echo $value['FullName']; ?></td>
                                            <td><?php echo $value['Review']; ?></td>
                                            <td> <div class="product-rating">
                                                                        <?php for($i = 1; $i <= 5; $i++) { 
                                                                                if($i <= $value['Rating']){
                                                                            ?>

                                                                             <i class="fa fa-star gold"></i> 
                                                                        <?php }else{ ?>
                                                                                <i class="fa fa-star-o"></i>
                                                                      <?php  } } ?>
                                                                       
                                                                    </div></td>
                                           

                                             <?php if(checkUserRightAccess(45,$this->session->userdata['admin']['UserID'],'CanDelete')){?> 
                                            <td>
                                                <?php if(checkUserRightAccess(45,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                                    <a href="javascript:void(0);" class="btn btn-simple btn-warning btn-icon remove"><i class="material-icons remove_review" title="Delete" data-id="<?php echo $value['ReviewID'];?>">close</i><div class="ripple-container"></div></a>
                                                <?php } ?>
                                               
                                            </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                        
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>
<script>
    $(document).ready(function () {
        $(document).on('click', '.remove_review', function () {
            var id = $(this).attr('data-id');
    if (confirm(delete_msg)) {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $.ajax({
            type: "POST",
            url: base_url + 'cms/product/delete_review',
            data: {
                'id': id,
                'form_type': 'delete'
            },
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {

                if (result.error != false) {
                    showError(result.error);
                } else {
                    showSuccess(result.success);
                    $('#' + id).remove();
                }


               

            },
            complete: function () {
                $.unblockUI();
            }
        });
        return true;
    } else {
        return false;
    }
});
});
</script>