
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">

                        <ul class="nav nav-pills nav-pills-warning">
                            <li class="active">
                                <a href="#pill1" data-toggle="tab" aria-expanded="true">Admins</a>
                            </li>
                            <li class="">
                                <a href="#pill2" data-toggle="tab" aria-expanded="false">Whole Salers</a>
                            </li>
                            <li class="">
                                <a href="#pill3" data-toggle="tab" aria-expanded="false">Customers</a>
                            </li>
                        </ul>

                        <div class="toolbar">
                            <a href="<?php echo base_url('cms/'.$ControllerName.'/add');?>">
                                <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5"><?php echo lang('add_'.$ControllerName); ?></button>
                            </a>
                        </div>
                        <div class="tab-content">
                        <div class="material-datatables tab-pane active" id="pill1">
                            <h4 class="card-title">Admins</h4>
                            <table id="" class="datatable table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                    <th><?php echo lang('title');?></th>
                                    <th><?php echo lang('role');?></th>
                                    <th>Email</th>    
                                    <th><?php echo lang('is_active');?></th>

                                    <?php if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanDelete') || checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanView')){?>
                                        <th><?php echo lang('actions');?></th>
                                    <?php } ?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                    foreach($results as $value){ ?>
                                        <?php if($value->RoleID == 1 || $value->RoleID == 2) { ?>
                                            <tr id="<?php echo $value->UserID; ?>">

                                                <td><?php echo $value->FullName; ?></td>
                                                <td><?php echo $value->RoleTitle; ?></td>
                                                <td><?php echo $value->Email; ?></td>


                                                <td><?php echo($value->IsActive ? lang('yes') : lang('no')); ?></td>

                                                <?php if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanDelete') || checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanView')){?>
                                                <td>
                                                    <a href="<?php echo base_url('cms/'.$ControllerName.'/changePassword/'.base64_encode($value->UserID));?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Change Password">lock</i><div class="ripple-container"></div></a>
                                                    <?php if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanView')){ ?>
                                                        <a href="<?php echo base_url('cms/'.$ControllerName.'/view/'.base64_encode($value->UserID));?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="View">visibility</i><div class="ripple-container"></div></a>
                                                    <?php } ?>
                                                    <?php if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit')){ ?>
                                                        <a href="<?php echo base_url('cms/'.$ControllerName.'/edit/'.$value->UserID);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">dvr</i><div class="ripple-container"></div></a>
                                                    <?php } ?>
                                                    <?php if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanDelete')){ ?>
                                                        <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $value->RoleID;?>','cms/<?php echo $ControllerName; ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                    <?php } ?>
                                                    <?php if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                                        <a href="<?php echo base_url('cms/user/rights/'.$value->UserID);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="User Rights">vertical_split</i><div class="ripple-container"></div></a>
                                                    <?php } ?>
                                                </td>
                                            <?php } ?>
                                            </tr>
                                            <?php
                                        }
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>

                        <div class="material-datatables tab-pane" id="pill2">
                            <h4 class="card-title">Whole Salers</h4>
                            <table id="" class="datatable table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                    <th><?php echo lang('title');?>/ Company</th>
                                    <th><?php echo lang('role');?></th>
                                    <th>Email</th>
                                    <th>Contact</th>
                                    <th>Phone</th>

                                    <th><?php echo lang('is_active');?></th>

                                    <?php if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanDelete') || checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanView')){?>
                                        <th><?php echo lang('actions');?></th>
                                    <?php } ?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                    foreach($results as $value){ ?>
                                        <?php if($value->RoleID == 1 || $value->RoleID == 2 || $value->RoleID == 3) continue;  ?>
                                        <tr id="<?php echo $value->UserID;?>">

                                            <td><?php echo $value->Company; ?></td>
                                            <td><?php echo $value->RoleTitle; ?></td>

                                            <td><?php echo $value->Email; ?></td>
                                            <td><?php echo $value->Contact; ?></td>
                                            <td><?php echo $value->Phone; ?></td>


                                            <td><?php echo ($value->IsActive ? lang('yes') : lang('no')); ?></td>

                                            <?php if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanDelete') || checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanView')){?>
                                                <td>
                                                    <a href="<?php echo base_url('cms/'.$ControllerName.'/changePassword/'.base64_encode($value->UserID));?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Change Password">lock</i><div class="ripple-container"></div></a>
                                                    <?php if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanView')){ ?>
                                                        <a href="<?php echo base_url('cms/'.$ControllerName.'/view/'.base64_encode($value->UserID));?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="View">visibility</i><div class="ripple-container"></div></a>
                                                    <?php } ?>
                                                    <?php if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit')){ ?>
                                                        <a href="<?php echo base_url('cms/'.$ControllerName.'/edit/'.$value->UserID);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">dvr</i><div class="ripple-container"></div></a>
                                                    <?php } ?>
                                                    <?php if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanDelete')){ ?>
                                                        <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $value->RoleID;?>','cms/<?php echo $ControllerName; ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                    <?php } ?>
                                                    <?php if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                                        <a href="<?php echo base_url('cms/user/rights/'.$value->UserID);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="User Rights">vertical_split</i><div class="ripple-container"></div></a>
                                                    <?php } ?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>

                        <div class="material-datatables tab-pane" id="pill3">
                            <h4 class="card-title">Customers</h4>
                            <table id="" class="datatable table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                    <th><?php echo lang('title');?></th>
                                    <th><?php echo lang('role');?></th>
                                    <th>Email</th>    
                                    <th><?php echo lang('is_active');?></th>

                                    <?php if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanDelete') || checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanView')){?>
                                        <th><?php echo lang('actions');?></th>
                                    <?php } ?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                    foreach($results as $value){ ?>
                                        <?php if($value->RoleID == 3) { ?>
                                            <tr id="<?php echo $value->UserID; ?>">

                                                <td><?php echo $value->FullName; ?></td>
                                                <td><?php echo $value->RoleTitle; ?></td>
                                                <td><?php echo $value->Email; ?></td>


                                                <td><?php echo($value->IsActive ? lang('yes') : lang('no')); ?></td>

                                                <?php if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanDelete') || checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanView')){?>
                                                <td>
                                                    <a href="<?php echo base_url('cms/'.$ControllerName.'/changePassword/'.base64_encode($value->UserID));?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Change Password">lock</i><div class="ripple-container"></div></a>
                                                    <?php if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanView')){ ?>
                                                        <a href="<?php echo base_url('cms/'.$ControllerName.'/view/'.base64_encode($value->UserID));?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="View">visibility</i><div class="ripple-container"></div></a>
                                                    <?php } ?>
                                                    <?php if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit')){ ?>
                                                        <a href="<?php echo base_url('cms/'.$ControllerName.'/edit/'.$value->UserID);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">dvr</i><div class="ripple-container"></div></a>
                                                    <?php } ?>
                                                    <?php if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanDelete')){ ?>
                                                        <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $value->RoleID;?>','cms/<?php echo $ControllerName; ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                    <?php } ?>
                                                    <?php if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                                        <a href="<?php echo base_url('cms/user/rights/'.$value->UserID);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="User Rights">vertical_split</i><div class="ripple-container"></div></a>
                                                    <?php } ?>
                                                </td>
                                            <?php } ?>
                                            </tr>
                                            <?php
                                        }
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                        </div><!-- tab-content -->
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script>
    $(document).ready(function() {
        $('table.datatable').DataTable({
            "ordering": false
        });
    } );
</script>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>