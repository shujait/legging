<?php
$option = '';
$option_business_type = '';
$option_prefer_sales = '';
if(!empty($roles)){ 
    foreach($roles as $role){ 
        $option .= '<option value="'.$role->RoleID.'" '.((isset($result[0]->RoleID) && $result[0]->RoleID == $role->RoleID) ? 'selected' : '').'>'.$role->Title.' </option>';
    } }
$option2 = '';
if(!empty($activities)){ 
    foreach($activities as $activity){ 
        $option2 .= '<option value="'.$activity->ActivityID.'" '.((isset($result[0]->ActivityID) && $result[0]->ActivityID == $activity->ActivityID) ? 'selected' : '').'>'.$activity->Title.' </option>';
    } }   

$busine_types = array('','Retail Store','Department/Chain Store','Wholesaler');
 for($i = 1;$i<=3;$i++){ 
        $option_business_type .= '<option value="'.$i.'" '.((isset($result[0]->BusinessType) && $result[0]->BusinessType == $i) ? 'selected' : '').'>'.$busine_types[$i].' </option>';
    }
$prefer_sales = array('N/A' => 'N/A','BLEE' => 'BERNADETTE LEE(CHINESE)','ED' => 'STAR DE LOZA(SPANISH/ENGLISH)','WEB' => 'ONLINE WEB','YELE' => 'YELETE','SL' => 'SEAN LIN','ANDY' => 'ANDY KIM(KOREAN, ENGLISH)','CECI' => 'CECILIA (SPANISH/ENGLISH)','LEA' => 'LEA BYUN(KOREAN, SPANISH)','JUL'=> 'JULIA ZHU(ENGLISH/MANDARIN)','RG'=>'REYNA GARCIA','NY' =>'NY OFFICE','ELI'=> 'ELIANA ANDRADE','LP' => 'LORENA PINEDO(SPANISH/ENGLISH)');
 foreach($prefer_sales as $pre => $prefer){ 
        $option_prefer_sales .= '<option value="'.$pre.'" '.((isset($result[0]->PreferredSale) && $result[0]->PreferredSale == $pre) ? 'selected' : '').'>'.$prefer.' </option>';
    }       
$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if(!empty($languages)){
    foreach($languages as $key => $language){
        $common_fields = '';
        $common_fields2 = '';
        $common_fields3 = '';
        $whole_saler_common_fields = '';
        if($key == 0){
         $common_fields = '<div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="RoleID'.$key.'">'.lang('choose_user_role').'</label>
                                                                <select id="RoleID'.$key.'" class="form-control" required="" disabled>
                                                                    '.$option.'
                                                                </select>
                                                            </div>
                                                        </div>';
        $common_fields2 = ' <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="Email">'.lang('email').'<span class="text-danger">*</span></label>
                                                                <input type="text" disabled  parsley-trigger="change" required  class="form-control" id="Email" value="'.((isset($result[0]->Email)) ? $result[0]->Email : '').'">
                                                            </div>
                                                        </div>'; 
        $common_fields3 = '<div class="row"><div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label class="control-label" for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').'/> '.lang('is_active').'
                                            </label>
                                        </div>
                                    </div>
                                </div></div>';
       
        }

        if($result[0]->RoleID == 4){
            $whole_saler_common_fields .= '<div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="Contact">Contact</label>
                                            <input type="text"  name="Contact" parsley-trigger="change" required  class="form-control" id="Contact" value="'.((isset($result[0]->Contact)) ? $result[0]->Contact : '').'">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="Company">Company</label>
                                            <input type="text" name="Company" parsley-trigger="change" required  class="form-control" id="Company" value="'.((isset($result[0]->Company)) ? $result[0]->Company : '').'">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="CompanyWebsite">Company Website</label>
                                            <input type="text"  name="CompanyWebsite" parsley-trigger="change" required  class="form-control" id="CompanyWebsite" value="'.((isset($result[0]->CompanyWebsite)) ? $result[0]->CompanyWebsite : '').'">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="Phone">Tel</label>
                                            <input type="text" name="Phone" parsley-trigger="change" required  class="form-control" id="Phone" value="'.((isset($result[0]->Phone)) ? $result[0]->Phone : '').'">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="Fax">Fax</label>
                                            <input type="text"  name="Fax" parsley-trigger="change" required  class="form-control" id="Fax" value="'.((isset($result[0]->Fax)) ? $result[0]->Fax : '').'">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="BusinessType">Business Type</label>
                                            <select id="BusinessType" class="selectpicker" data-style="select-with-transition" required name="BusinessType">
                                                '.$option_business_type.'
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="PreferredSale">Prefer Sale</label>
                                            <select id="PreferredSale" class="selectpicker" data-style="select-with-transition" required name="PreferredSale">
                                                '.$option_prefer_sales.'
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="Fax">Resale Perit Number</label>
                                            <input type="text"  name="ResalePeritNumber" parsley-trigger="change" required  class="form-control" id="ResalePeritNumber" value="'.((isset($result[0]->ResalePeritNumber)) ? $result[0]->ResalePeritNumber : '').'">
                                        </div>
                                    </div>
                                </div>';

        }    

        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'">
                                        <a href="#'.$language->SystemLanguageTitle.'" data-toggle="tab">
                                            '.$language->SystemLanguageTitle.'
                                        </a>
                                  </li>';

        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageTitle.'">
                     

                                                   
                                                     <div class="row">
                                                     '.$common_fields.'
                                                     
                                                        
                                                         '.($result[0]->RoleID != 4 ? '<div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title'.$key.'">'.lang('name').'</label>
                                                                <input type="text" name="FullName" parsley-trigger="change" required  class="form-control" id="Title'.$key.'" value="'.((isset($result[$key]->FullName)) ? $result[$key]->FullName : '').'">
                                                               
                                                            </div>
                                                        </div>' : '').'
                                                         '.$common_fields2.'
                                                    </div>

                                                    '.$whole_saler_common_fields.'
                                                    
                                                   '.$common_fields3.'
                                                    

                                                 

                                                


                        </div>';
        
        
        
        
        
    }
}


?>


<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    
                    <div class="card-header">
                        <h5 class="card-title">View <?php echo $result[0]->FullName;?></h5>
                    </div>
                    
                    <div class="card-content">
                        <div class="row">
                             <?php if(count($languages) > 1){?>
                            <div class="col-md-2">
                                
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                               
                            </div>
                            <div class="col-md-12">
                              <?php }else{ ?>
                                <div class="col-md-10">
                                  <?php } ?>
                                    <div class="tab-content">
                                        <?php echo $lang_data; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
