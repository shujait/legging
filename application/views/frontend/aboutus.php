<section class="page-section page-header breadcrumbs">
    <img src="<?php echo base_url('assets/backend');?>/img/aboutbg.jpg">
    <div class="container">
        <h3>About us</h3>
    </div>

</section>
<div class="container-fluid">


<div class="aboutus-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="aboutus">
                        <h2 class="aboutus-title">Who We Are</h2>
                        <p class="aboutus-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
                        <p class="aboutus-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
                         <p class="aboutus-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
                        <p class="aboutus-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
                        <a class="aboutus-more" href="#">read more</a>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 col-xs-12">
                    <div class="aboutus-banner">
                        <img src="<?php echo base_url(); ?>assets/backend/img/girl-about.png">
                    </div>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-12">
                    <div class="feature">
                        <div class="feature-box">
                            <div class="clearfix">
                                <div class="iconset">
                                    <span class="glyphicon glyphicon-cog icon"></span>
                                </div>
                                <div class="feature-content">
                                    <h4>Work with heart</h4>
                                    <p>Lorem ipsum es to dolor porque es to dolor Lorem ipsum es to dolor porque es to dolor Lorem ipsum es to dolor porque es to dolor  </p>
                                </div>
                            </div>
                        </div>
                        <div class="feature-box">
                            <div class="clearfix">
                                <div class="iconset">
                                    <span class="glyphicon glyphicon-cog icon"></span>
                                </div>
                                <div class="feature-content">
                                    <h4>Reliable services</h4>
                                    <p>Lorem ipsum es to dolor porque es to dolor Lorem ipsum es to dolor porque es to dolor Lorem ipsum es to dolor porque es to dolor  </p>
                                </div>
                            </div>
                        </div>
                        <div class="feature-box">
                            <div class="clearfix">
                                <div class="iconset">
                                    <span class="glyphicon glyphicon-cog icon"></span>
                                </div>
                                <div class="feature-content">
                                    <h4>Great support</h4>
                                    <p>Lorem ipsum es to dolor porque es to dolor Lorem ipsum es to dolor porque es to dolor Lorem ipsum es to dolor porque es to dolor  </p>
                                </div>
                            </div>
                        </div>
                        <div class="feature-box">
                            <div class="clearfix">
                                <div class="iconset">
                                    <span class="glyphicon glyphicon-cog icon"></span>
                                </div>
                                <div class="feature-content">
                                    <h4>Great support</h4>
                                    <p>Lorem ipsum es to dolor porque es to dolor Lorem ipsum es to dolor porque es to dolor Lorem ipsum es to dolor porque es to dolor  </p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="shop-images">
        
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                
                <div class="shop-img">
                <img src="<?php echo base_url(); ?>assets/backend/img/about1.jpg">
                </div>

                </div>

                <div class="col-md-3">

                <div class="shop-img">
                <img src="<?php echo base_url(); ?>assets/backend/img/about2.jpg">
                </div>

                </div>

                <div class="col-md-3">
                <div class="shop-img">
                 <img src="<?php echo base_url(); ?>assets/backend/img/about3.jpg">
                 </div>

                </div>

                <div class="col-md-3">
                
                <div class="shop-img">
                <img src="<?php echo base_url(); ?>assets/backend/img/about4.jpg">
                </div>

                </div>
            </div>

        </div>

    </div>

</div>