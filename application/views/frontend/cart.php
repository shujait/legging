<section class="page-section page-header breadcrumbs">
   <img src="<?php echo base_url('assets/backend');?>/img/regbg.jpg">
   <div class="container">
      <h3>Cart</h3>
   </div>
</section>
<div class="container">
   <div class="row">
      <div class="col-md-9">
         <div class="panel panel-info cus-panel">
            <div class="panel-heading">
               <div class="panel-title">
                  <div class="row">
                     <div class="col-md-9 col-sm-8">
                        <h5><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</h5>
                     </div>
                     <div class="col-md-3 col-sm-4">
                        <a href="<?php echo base_url();?>" class="btn btn-primary btn-sm btn-block">
                        <span class="glyphicon glyphicon-share-alt"></span> Continue shopping
                        </a>
                     </div>
                  </div>
               </div>
            </div>

            <div class="panel-body">
               
               <?php 
                    if($products != '')
                    {
                        $totalPrice = 0;
                        foreach ($products as $key => $value) {
                           $product_images = getSiteImages($value['ProductID'], 'ProductImage');
                            $Price = (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? $value['PriceForWholeSaler'] : $value['Price']);
                            $DiscountPer = (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? $value['WholeSaleDiscount'] : $value['Discount']);

                            if($DiscountPer > 0)
                            {
                              $Discount = $Price * ($DiscountPer/100);

                              $Price = $Price - $Discount;
                            }
               ?>
                           <div class="row" id="delete-<?php echo $value['temp_order_id']; ?>">
                              <hr>
                              <div class="col-md-8 col-sm-5">
                                 <div class="col-md-3"><img class="img-responsive cart-img" height="20px" src="<?php echo (file_exists($product_images[0]['ImageName']) ? base_url() . $product_images[0]['ImageName'] : base_url('uploads/no_image.png'));?>">
                                 </div>
                                 <div class="col-md-9">
                                    <a href="<?php echo base_url('products/detail/'.$value['ProductID']);?>"><h4 class="product-name"><strong><?php echo $value['Title'];?></strong></h4></a>

                                    <div class="qty">
                                       <span><input type="button" value="-" id="moins" onclick="minus('<?php echo $value['temp_order_id'];?>')" class="count-btn"></span>
                                       <span><input type="text" size="200" value="<?php echo $value['quantity']; ?>" id="count-<?php echo $value['temp_order_id'];?>" class="quantity"></span>
                                       <span><input type="button" value="+" id="plus" onclick="plus('<?php echo $value['temp_order_id'];?>')" class="count-btn"></span>
                                    </div>
                                 </div>

                              </div>
                              <div class="col-md-4 col-sm-7">
                                 <div class="col-md-6  col-sm-6 text-right">
                                    <h6><strong class="pull-right cart-price"><?php echo getSelectedCurrencies($CurrencyID,$Price);?> <span class="text-muted">x</span> <?php echo ($value['TotalPieces'] > 0 ? $value['TotalPieces'].'pcs <span class="text-muted">x</span>':'');?></strong></h6>
                                 </div>
                                 
                                 <div class="col-md-6 col-sm-6">
                                    <button type="button" class="btn delete_cart_item btn-danger" data-temp-id="<?php echo $value['temp_order_id']; ?>">
                                    <span class="glyphicon glyphicon-trash"> </span> Remove
                                    </button>
                                 </div>
                              </div>
                           </div>
               <?php
                            $totalPrice += ($value['TotalPieces'] > 0 ? $value['TotalPieces'] * $value['quantity'] * $Price : $value['quantity'] * $Price);
                        }
                       }
                       else
                       {
               ?>
                       <div class="media">
                           <div class="media-body">
                               <p class="item-desc">Empty Cart</p>
                           </div>
                       </div>
               <?php 
                       }
                ?>
            </div>
            <div class="panel-footer">
               <div class="row text-center">
                  <div class="col-md-10">
                     <h4 class="text-right">Total <strong><?php echo getSelectedCurrencies($CurrencyID,$totalPrice);?></strong></h4>
                  </div>
                  <div class="col-md-2">
                     <a href="<?php echo base_url('checkout');?>" class="btn btn-success btn-block">
                     Checkout
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>

         <div class="col-md-3 col-xs-12">
                      <img width="100%"  src="<?php echo base_url(); ?>assets/backend/img/banner-new.jpg">
                  </div>
   </div>
</div>
<script type="text/javascript">
   var count = 1;
   function plus(temp_order_id){
     var countEl = document.getElementById("count-"+temp_order_id);
     count = parseInt(countEl.value)+1;
     countEl.value = count;
     changeQuantity(temp_order_id,count);
   }
   function minus(temp_order_id){
     var countEl = document.getElementById("count-"+temp_order_id);
     count = parseInt(countEl.value)-1;
      if (count > 1) {
        countEl.value = count;
         changeQuantity(temp_order_id,count);
      }  
   }
   
</script>