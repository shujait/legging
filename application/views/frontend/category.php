<div class="content-area">

    <section class="page-section page-header breadcrumbs">
        <img src="<?php echo base_url();?>assets/backend/img/banner.jpg">
        <div class="container">
            <h3><?php echo $categoryDet[0]->Title;?></h3>
        </div>

    </section>

    <section class="page-section with-sidebar">
        <div class="container-fluid">
            <div class="row">
                <!-- CONTENT -->
                <div class="col-md-12 content" id="content">

                    <!-- Products grid -->
                    <div class="row products grid">
                        <?php 
                        foreach($categories as $category)
                        {
                        ?>
                            <div class="col-md-3 col-sm-6">
                                <div class="thumbnail no-border no-padding">
                                    <div class="media">
                                        <a class="media-link" data-gal="prettyPhoto" href="<?php echo base_url().(getSubCategory($category->CategoryID) > 0 ? 'category/detail/' : 'products/index/').$category->CategoryID;?>">
                                            <img src="<?php echo base_url($category->Image);?>" alt=""/>
                                        </a>
                                    </div>
                                    <div class="caption text-center">
                                        <h4 class="caption-title"><a href="<?php echo base_url().(getSubCategory($category->CategoryID) > 0 ? 'category/detail/' : 'products/index/').$category->CategoryID;?>"><?php echo $category->Title;?></a></h4>

                                    </div>
                                </div>
                            </div>
                        <?php 
                        }
                        ?>
                    </div>
                    <hr>
                    <!-- /Products grid -->

                    <!-- Pagination -->
                    <!--<div class="pagination-wrapper">
                        <ul class="pagination">
                            <li class="disabled"><a href="#"><i class="fa fa-angle-double-left"></i> Previous</a></li>
                            <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">Next <i class="fa fa-angle-double-right"></i></a></li>
                        </ul>
                    </div>-->
                    <!-- /Pagination -->

                </div>
                <!-- /CONTENT -->

            </div>
        </div>
    </section>

</div>