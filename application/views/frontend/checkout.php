<section class="page-section page-header breadcrumbs">
   <img src="<?php echo base_url('assets/backend');?>/img/regbg.jpg">
   <div class="container">
      <h3>Checkout</h3>
   </div>
</section>
<div class="container-fluid">
   <div class="reg-form">
      <form action="<?php echo base_url();?>checkout/place_order" method="post" class="checkout_form" enctype="multipart/form-data" data-parsley-validate novalidate>
          <input type="hidden" name="OrderPaymentDetailID" id="OrderPaymentDetailID" value="<?php echo (isset($OrderPaymentDetailID) ? $OrderPaymentDetailID : 0); ?>">
         <div class="row">
            <div class="col-md-9">
               <h3>Personal Info</h3>
               <form action="#">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                            <?php 
                              if(isset($userdetail))
                              {
                                $name = explode(' ', $userdetail[0]->FullName);
                                $first_name = '';
                                $last_name = '';
                                if(isset($name[0])){
                                    $first_name = $name[0];
                                }
                                if(isset($name[1])){
                                    $last_name = $name[1];
                                }
                                
                              }  
                            ?>
                           <label for="contact">First Name<span>*</span></label>
                           <input type="text" name="first_name" class="form-control" id="fname" value="<?php echo (isset($userdetail) && $userdetail[0]->FullName != '' ? $first_name : '');?>" required>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label for="company">Last Name <span>*</span></label>
                           <input type="text" name="last_name" class="form-control" id="lname" value="<?php echo (isset($userdetail) && $userdetail[0]->FullName != '' ? $last_name : '');?>" required>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label for="Tel">Tel<span>*</span></label>
                           <input type="tel" name="telephone" class="form-control" id="Tel" value="<?php echo (isset($userdetail) && $userdetail[0]->Phone != '' ? $userdetail[0]->Phone : '');?>" required>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label for="Fax">Email<span>*</span></label>
                           <input type="text" name="email" class="form-control" id="email" value="<?php echo (isset($userdetail) && $userdetail[0]->Email != '' ? $userdetail[0]->Email : '');?>" required>
                        </div>
                     </div>
                  </div>
                  <hr>
                  <h3>Billing Address</h3>
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label for="AddressLine-1">Address Line 1 <span>*</span></label>
                           <input type="text" name="billing_address_1" class="form-control" id="AddressLine-1" value="<?php echo (isset($billing_details->Address1) && $billing_details->Address1 != '' ? $billing_details->Address1 : '');?>" required>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label for="AddressLine-2">Address Line 2</label>
                           <input type="text" name="billing_address_2" class="form-control" id="AddressLine-2" value="<?php echo (isset($billing_details->Address2) && $billing_details->Address2 != '' ? $billing_details->Address2 : '');?>">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-3">
                        <div class="form-group">
                           <label for="BillingCountry">Country <span>*</span></label>
                           <select name="billing_country" id="BillingCountry" data-state-type="BillingState" class="form-control CountryID">
                              <option value=""><?php echo lang("choose_country");?></option>
                              <?php 
                              foreach($countries as $Country)
                              {
                              ?>
                                  <option value="<?php echo $Country->CountryID; ?>" <?php echo (isset($billing_details->CountryID) && $billing_details->CountryID == $Country->CountryID ? 'selected="selected"' : '');?>><?php echo $Country->Title; ?></option>
                              <?php 
                              }
                              ?>
                           </select>
                        </div>
                     </div>
                     <div class="col-md-3">
                        <div class="form-group">
                             <label for="BillingState">State <span>*</span></label>
                             <select name="billing_state" id="BillingState" data-city-type="BillingCity" class="form-control StateID">
                                 <option value=""><?php echo lang("choose_state");?></option>
                                 <?php 
                                 if($billing_states)
                                 {
                                  foreach($billing_states as $State)
                                  {
                                  ?>
                                      <option value="<?php echo $State->StateID; ?>" <?php echo (isset($billing_details->StateID) && $billing_details->StateID == $State->StateID ? 'selected="selected"' : '');?>><?php echo $State->Title; ?></option>
                                  <?php 
                                  }
                                 }
                                  ?>
                             </select>
                         </div>
                     </div>
                     <div class="col-md-3">
                        <div class="form-group">
                             <label for="BillingCity">City <span>*</span></label>
                             <select name="billing_city" id="BillingCity" class="form-control">
                                 <option value=""><?php echo lang("choose_city");?></option>
                                 <?php 
                                 if($billing_cities)
                                 {
                                  foreach($billing_cities as $City)
                                  {
                                  ?>
                                      <option value="<?php echo $City->CityID; ?>" <?php echo (isset($billing_details->CityID) && $billing_details->CityID == $City->CityID ? 'selected="selected"' : '');?>><?php echo $City->Title; ?></option>
                                  <?php 
                                  }
                                 }
                                  ?>
                             </select>
                         </div>
                     </div>
                     <div class="col-md-3">
                        <div class="form-group">
                           <label for="Zip">Zip</label>
                           <input type="text" name="billing_zip_code" class="form-control" id="Zip" value="<?php echo (isset($billing_details->Address2) && $billing_details->Address2 != '' ? $billing_details->Address2 : '');?>">
                        </div>
                     </div>
                  </div>
                  <hr>
                  <h3>Shipping Address</h3>
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label for="ShippingAddressLine-1">Address Line 1 <span>*</span></label>
                           <input type="text" name="shipping_address_1" class="form-control" id="ShippingAddressLine-1" value="<?php echo (isset($shipping_details->Address1) && $shipping_details->Address1 != '' ? $shipping_details->Address1 : '');?>" required>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label for="ShippingAddressLine-2">Address Line 2</label>
                           <input type="text" name="shipping_address_2" class="form-control" id="ShippingAddressLine-2" value="<?php echo (isset($shipping_details->Address2) && $shipping_details->Address2 != '' ? $shipping_details->Address2 : '');?>">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-3">
                        <div class="form-group">
                           <label for="ShippingCountry">Country <span>*</span></label>
                             <select name="shipping_country" id="ShippingCountry" data-state-type="ShippingState" class="form-control CountryID">
                                 <option value=""><?php echo lang("choose_country");?></option>
                                 <?php 
                                 foreach($countries as $Country)
                                 {
                                 ?>
                                     <option value="<?php echo $Country->CountryID; ?>" <?php echo (isset($shipping_details->CountryID) && $shipping_details->CountryID == $Country->CountryID ? 'selected="selected"' : '');?>><?php echo $Country->Title; ?></option>
                                 <?php 
                                 }
                                 ?>
                             </select>
                        </div>
                     </div>
                     <div class="col-md-3">
                        <div class="form-group">
                             <label for="ShippingState">State <span>*</span></label>
                             <select name="shipping_state" id="ShippingState" data-city-type="ShippingCity" class="form-control StateID">
                                 <option value=""><?php echo lang("choose_state");?></option>
                                 <?php 
                                 if($shipping_states)
                                 {
                                  foreach($shipping_states as $State)
                                  {
                                  ?>
                                      <option value="<?php echo $State->StateID; ?>" <?php echo (isset($shipping_details->StateID) && $shipping_details->StateID == $State->StateID ? 'selected="selected"' : '');?>><?php echo $State->Title; ?></option>
                                  <?php 
                                  }
                                 }
                                  ?>
                             </select>
                         </div>
                     </div>
                     <div class="col-md-3">
                        <div class="form-group">
                             <label for="ShippingCity">City <span>*</span></label>
                             <select name="shipping_city" id="ShippingCity" class="form-control">
                                 <option value=""><?php echo lang("choose_city");?></option>
                                 <?php 
                                 if($shipping_cities)
                                 {
                                  foreach($shipping_cities as $City)
                                  {
                                  ?>
                                      <option value="<?php echo $City->CityID; ?>" <?php echo (isset($shipping_details->CityID) && $shipping_details->CityID == $City->CityID ? 'selected="selected"' : '');?>><?php echo $City->Title; ?></option>
                                  <?php 
                                  }
                                 }
                                  ?>
                             </select>
                         </div>
                     </div>
                     <div class="col-md-3">
                        <div class="form-group">
                           <label for="ShippingZip">Zip</label>
                           <input type="text" name="shipping_zip_code" class="form-control" id="ShippingZip" value="<?php echo (isset($shipping_details->Address2) && $shipping_details->Address2 != '' ? $shipping_details->Address2 : '');?>" required>
                        </div>
                     </div>
                  </div>
                  <hr>
                  <!-- <h3>Payment</h3>
                  <div class="row">
                     <div class="col-md-6">
                        <input type="hidden" name="payment_method" id="payment_method" value="<?php echo (isset($payment_method) ? $payment_method : ''); ?>">
                        <label class="check1">Credit Card / Debit Card
                        <input type="radio" class="payment_method" id="stripePaymentBtn" value="Stripe" <?php echo (isset($payment_method) ? 'disabled="disabled"' : ''); ?>>
                        <span class="checkmark"></span>
                        </label>
                        <label class="check1">Paypal
                        <input type="radio" class="payment_method" id="paypalPaymentBtn" value="Paypal" <?php echo (isset($payment_method) ? 'checked="checked" disabled="disabled"' : ''); ?>>
                        <span class="checkmark"></span>
                        </label>               
                     </div>
                  </div> -->
                 <!--  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label for="name-card">Name of Card<span>*</span></label>
                           <input type="text" name="name-card" class="form-control" id="name-card" required>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label for="card-number">Credit card number<span>*</span></label>
                           <input type="text" name="card-number" class="form-control" id="card-number" required>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-3">
                        <div class="form-group">
                           <label for="exp">Expiration<span>*</span></label>
                           <input type="text" name="exp" class="form-control" id="exp" required>
                        </div>
                     </div>
                     <div class="col-md-3">
                        <div class="form-group">
                           <label for="cvv">CVV<span>*</span></label>
                           <input type="text" name="cvv" class="form-control" id="cvv" required>
                        </div>
                     </div>
                  </div> -->
                  <hr>
                 <!--  <div class="row">
                     <div class="col-md-12 text-center">
                        <input type="submit" name="CreateMyAcoount" class="btn btn-success" value="Place Order">
                     </div>
                  </div> -->
               </form>
               <hr>
            </div>
            <aside class="col-md-3">
               <div class="side-cart">
                  <h4 class="cart-heading"> SUMMARY <span class="price1"><i class="fa fa-shopping-cart"></i> <b>4</b></span></h4>
                  <div class="discount">
                    <a href="#">Do you have a Promo code?</a>
                </div>
                  <?php 
                        if (isset($this->session->userdata['admin']['UserID'])) {

                            $user_id = $this->session->userdata['admin']['UserID'];

                        } else {
                            if (!get_cookie('temp_order_key')) {
                                $user_id = 0;
                                
                            } else {
                                $user_id = get_cookie('temp_order_key');
                            }
                        }

                        if($user_id > 0)
                        {
                            $productdet = getUserCartDetail($user_id);
                        }
                        else
                        {
                            $productdet = '';
                        }
                        $productdescription = 'Products';
                        $productsTitleArr = array();


                       if($productdet != '')
                       {
                           $totalPrice = 0;
                           foreach ($productdet as $key => $value) {
                              $productsTitleArr[] = $value['Title'];

                              $Price = (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? $value['PriceForWholeSaler'] : $value['Price']);
                              $DiscountPer = (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? $value['WholeSaleDiscount'] : $value['Discount']);

                              if($DiscountPer > 0)
                              {
                                  $Discount = $Price * ($DiscountPer/100);

                                  $Price = $Price - $Discount;
                              }
                   ?>
                              <p class="cart-list"><a href="#"><?php echo $value['Title'];?></a> <span class="price"><?php echo ($value['TotalPieces'] > 0 ? $value['TotalPieces'] * $value['quantity'] : $value['quantity']) ?> X <?php echo getSelectedCurrencies($CurrencyID,$Price);?></span></p>
                  <?php
                               $totalPrice += ($value['TotalPieces'] > 0 ? $value['TotalPieces'] * $value['quantity'] * $Price : $value['quantity'] * $Price);
                           }
                           $productdescription = implode(',', $productsTitleArr);
                        }
                   ?>
                  
                  <p class="total-price">Total <span class="total"><b><?php echo getSelectedCurrencies($CurrencyID,$totalPrice);?></b></span></p>

                  <div class="payment-setup">
                    
                    <input type="hidden" name="payment_method" id="payment_method" value="<?php echo (isset($payment_method) ? $payment_method : ''); ?>">
                        <label class="check1">Payment with <span class="pull-right"><img src="<?php echo base_url(); ?>assets/backend/img/credit.png"></span>
                        <input type="radio" class="payment_method" id="stripePaymentBtn" value="Stripe" <?php echo (isset($payment_method) ? 'disabled="disabled"' : ''); ?>>
                        <span class="checkmark"></span>
                        </label>
                        <label class="check1">Payment with <span class="pull-right"><img src="<?php echo base_url(); ?>assets/backend/img/paypal.png"></span>
                        <input type="radio" class="payment_method" id="paypalPaymentBtn" value="Paypal" <?php echo (isset($payment_method) ? 'checked="checked" disabled="disabled"' : ''); ?>>
                        <span class="checkmark"></span>
                        </label> 

                  </div>

                  <div class="place-order">
                  <input type="submit" name="CreateMyAcoount" class="place-order-btn" value="Place Order">

                  </div>
               </div>
            </aside>
            <input type="hidden" name="amount" value="<?php echo $totalPrice; ?>">
            <!-- CONTENT -->

            <form method="post" class="form-horizontal" id="paypalForm" role="form" action="<?= base_url() ?>paypal/create_payment_with_paypal">
                <fieldset>
                    <input title="item_name" name="item_name" type="hidden" value="<?php echo (isset($userdetail) && $userdetail[0]->Contact != '' ? $userdetail[0]->Contact : '');?>">
                    <input title="item_number" name="item_number" type="hidden" value="<?php echo $user_id;?>">
                    <input title="item_description" name="item_description" type="hidden" value="<?php echo $productdescription;?>">
                    <input title="item_tax" name="item_tax" type="hidden" value="0">
                    <input title="item_price" name="item_price" type="hidden" value="<?php echo getSelectedCurrencies($CurrencyID,$totalPrice,false);?>">
                    <input type="hidden" name="currency_code" title="currency_code" value="<?php echo getCurrencyCode($CurrencyID);?>">
                    <input title="details_tax" name="details_tax" type="hidden" value="0">
                    <input title="details_subtotal" name="details_subtotal" type="hidden" value="<?php echo getSelectedCurrencies($CurrencyID,$totalPrice,false);?>">
                </fieldset>
            </form>    
         </div>
      </form>
   </div>
   <!-- SIDEBAR -->
</div>

<script src="https://checkout.stripe.com/checkout.js"></script>

<script type="text/javascript">
    
    <?php if(isset($payment_method)){ ?>

    jQuery(document).ready(function () {
        $.blockUI({
              css: {
                  border: 'none',
                  padding: '15px',
                  backgroundColor: '#000',
                  '-webkit-border-radius': '10px',
                  '-moz-border-radius': '10px',
                  opacity: .5,
                  color: '#fff'
              }
          });
        setTimeout(function () {
          $(".checkout_form").submit();
        }, 2000);
    });
    <?php }?>
    /*************** START: Stripe payment widget ***************/
    var handler = StripeCheckout.configure({
      key: '<?php echo STRIPE_PUBLISHABLE_KEY; ?>',
      image: '<?php echo base_url();?>assets/backend/img/logo.jpg',
      locale: 'auto',
      allowRememberMe: false,
      token: function(token) {
        // You can access the token ID with `token.id`.
        // Get the token ID to your server-side code for use.
        
        //console.log(token);
        //$('#paymentModal').modal('hide');


        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $.ajax({
          url: base_url + 'checkout/stripe_single_payment',
          type: 'POST',
          data: {
            firstname: $('form.payment input[name="first_name"]').val(),
            lastname: $('form.payment input[name="last_name"]').val(),
            token: JSON.stringify(token)
          },
          dataType: 'json',
          success: function(res) {
            //console.log(res);
            $('#stripePaymentBtn').prop('checked', 'checked');
            $('.payment_method').attr('disabled','disabled');
            $('#payment_method').val('Stripe');
            $('#OrderPaymentDetailID').val(res.OrderPaymentDetailID);
            showSuccess(res.message);
            setTimeout(function () {
              $(".checkout_form").submit();
            }, 2000);
            //location.reload();
          }
        });
      }
    });

    document.getElementById('stripePaymentBtn').addEventListener('click', function(e) {
      // Open Checkout with further options:   
      if(!CreateTempAccount())
      { 
        $('#stripePaymentBtn').prop('checked', false);
        return false;
      }

      var xyz = "2";
    if(xyz =="2"){
      handler.open({
        name: "LeggingPro",
        description: "Product Payment",
        panelLabel: "Pay",
        zipCode: false,
        /*allowRememberMe: false,*/
        amount: <?php echo getSelectedCurrencies($CurrencyID, $totalPrice*100,false);?>,
        currency: '<?php echo getCurrencyCode($CurrencyID); ?>'
      });
    }else{ //$(".Respondenterror").show();
    }

      e.preventDefault();
    
    });

    // Close Checkout on page navigation:
    window.addEventListener('popstate', function() {
      handler.close();
    });
    /**************** END: Stripe payment widget ****************/

    $('#paypalPaymentBtn').click(function(){
      if(!CreateTempAccount())
      {
        $('#paypalPaymentBtn').prop('checked', false);
        return false;
        
      }
      $.blockUI({
          css: {
              border: 'none',
              padding: '15px',
              backgroundColor: '#000',
              '-webkit-border-radius': '10px',
              '-moz-border-radius': '10px',
              opacity: .5,
              color: '#fff'
          }
      });
      $('#paypalForm').submit();
    });


    function CreateTempAccount()
    {
      var flg = false;
      $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $form = $(".checkout_form");
        $.ajax({
            type: "POST",
            url: '<?php echo base_url();?>checkout/customerRegistrationValidation',
            data: new FormData($('form.checkout_form')[0]),
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            async:false,
            success: function (result) {
                if (result.error != false) {
                    showError(result.error);
                    //return false;
                } else {
                    flg = true;
                    showSuccess(result.success);
                    //return true;
                }
            },
            complete: function () {
                $.unblockUI();
            }
        });
        return flg;
    }
</script>