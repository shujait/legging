

    <div class="map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3353.382847418292!2d-116.96113958490763!3d32.808619580962414!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80d95835e5f44ab3%3A0xe5d307715186d7f3!2sSan+Diego+Metropolitan+Credit+Union!5e0!3m2!1sen!2s!4v1545387926720" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            
    </div>

        <div class="row text-center">
      <div class="col-md-4 box1 pt-4">
        <a href="tel:+123456789"><i class="fa fa-phone"></i>
        <h3>Phone</h3>
        <p>+123456789</p></a>
      </div>
      <div class="col-md-4 box2 pt-4">
        <a href=""><i class="fa fa-home"></i>
        <h3>Address</h3>
        <p>Lorem ipsum es to dolor, 1, 00184 Roma RM</p></a>
      </div>
      <div class="col-md-4 box3 pt-4">
        <a href="mailto:test@test.com"><i class="fa fa-envelope"></i>
        <h3>E-mail</h3>
        <p>test@test.com</p></a>
      </div>
    </div>

<div class="container-fluid">
    <div class="reg-form">
        <div class="row">

        <div class="col-md-6">
              <img width="100%" src="<?php echo base_url(); ?>assets/backend/img/contact.jpg">
            </div>

            <div class="col-md-6">
                <form action="<?php echo base_url();?>contact/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                    <input type="hidden" name="form_type" value="save">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="contact">Name <span>*</span></label>
                                <input type="text" name="Name" class="form-control" id="Name" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="company">Email <span>*</span></label>
                                <input type="text" name="Email" class="form-control" id="Email" required>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="company">Phone <span>*</span></label>
                                <input type="text" name="Phone" class="form-control" id="Phone" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="CompanyWeb">Message <span>*</span></label>
                                <textarea name="Message" class="form-control" style="height: 200px;" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="submit" name="Submit" class="btn btn-success contact" value="Send Message">
                        </div>
                    </div>
                </form>
            </div>
            
           
            <!-- CONTENT -->
        </div>
    </div>
    <!-- SIDEBAR -->


</div>