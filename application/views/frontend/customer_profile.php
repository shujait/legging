<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">person</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">My Profile</h4>
                                    <div class="row">
                    <div class="col-md-12">
                        
                        <h3>General Information</h3>
                        <form action="<?php echo base_url();?>cms/account/updateCustomer" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                        <?php if(file_exists($result->Image)){
                            $profile_image = base_url($result->Image);
                        }else{
                            $profile_image = base_url('assets/backend/img/no_image.png');
                        }
                        ?> 
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <img src="<?php echo $profile_image;?>" style="width:100px;height:100px;">
                            </div>        
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Image (after choose file click update button to change profile image)</label>
                                        <input type="file" name="Image[]">
                                    </div>
                            </div>
                        </div>     
                        <div class="row">
                            
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="contact">FullName <span>*</span></label>
                                    <input type="text" name="FullName" class="form-control" id="contact" value="<?php echo $result->FullName; ?>" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="Tel">Tel <span>*</span></label>
                                    <input type="tel" name="Phone" class="form-control" id="Tel" required value="<?php echo $result->Phone; ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="EMail">Email </label>
                                    <input type="email"  class="form-control" id="EMail" disabled value="<?php echo $result->Email; ?>">
                                </div>
                            </div>

                            
                        </div>
                        
                        
                       
                        <hr>
                        <h3>Billing Address</h3>
                        <div class="row">
                            <div class="col-md-6">
                            <div class="form-group">
                                <label for="BillingAddress1">Address Line 1 <span>*</span></label>
                                <input type="text" name="BillingAddress1" class="form-control" id="BillingAddress1" required value="<?php echo $billing_address['Address1']; ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="BillingAddress2">Address Line 2 <span>*</span></label>
                                <input type="text" name="BillingAddress2" class="form-control" id="BillingAddress2" value="<?php echo $billing_address['Address2']; ?>">
                            </div>
                        </div>
                        </div>
                        <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="BillingCountry">Country <span>*</span></label>
                                <select name="BillingCountry" id="BillingCountry" data-state-type="BillingState" class="form-control CountryID">
                                    <option value=""><?php echo lang("choose_country");?></option>
                                    <?php 
                                    foreach($countries as $Country2)
                                    {
                                    ?>
                                        <option value="<?php echo $Country2->CountryID; ?>" <?php echo ((isset($billing_address['CountryID']) && $billing_address['CountryID'] == $Country2->CountryID) ? 'selected' : '' )?>><?php echo $Country2->Title; ?></option>
                                    <?php 
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="BillingState">State</label>
                                <select name="BillingState" id="BillingState" data-city-type="BillingCity" class="form-control StateID">
                                    <option value=""><?php echo lang("choose_state");?></option>
                                    <?php
                                      if($billing_states){
                                            foreach ($billing_states as $key => $value) {
                                                echo '<option value="'.$value['StateID'].'" '.($billing_address['StateID'] == $value['StateID'] ? 'selected' : '').'>'.$value['Title'].'</option>';
                                            }

                                      }

                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="BillingCity">City</label>
                                <select name="BillingCity" id="BillingCity" class="form-control">
                                    <option value=""><?php echo lang("choose_city");?></option>
                                    <?php
                                      if($billing_cities){
                                            foreach ($billing_cities as $key => $value) {
                                                echo '<option value="'.$value['CityID'].'" '.($billing_address['CityID'] == $value['CityID'] ? 'selected' : '').'>'.$value['Title'].'</option>';
                                            }

                                      }

                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="BillingZipCode">Zip <span>*</span></label>
                                <input type="text" name="BillingZipCode" class="form-control" id="BillingZipCode"  value="<?php echo $billing_address['ZipCode'];?>">
                            </div>
                        </div>
                    </div>
                        <hr>
                        <h3>Shipping Address</h3>
                        <div class="row">
                            <div class="col-md-6">
                            <div class="form-group">
                                <label for="ShippingAddress1">Address Line 1</label>
                                <input type="text" name="ShippingAddress1" class="form-control" id="ShippingAddress1" value="<?php echo $shipping_address['Address1']; ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="ShippingAddress2">Address Line 2 <span>*</span></label>
                                <input type="text" name="ShippingAddress2" class="form-control" id="ShippingAddress2" value="<?php echo $shipping_address['Address2']; ?>">
                            </div>
                        </div>
                        </div>
                        <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="ShippingCountry">Country <span>*</span></label>
                                <select name="ShippingCountry" id="ShippingCountry" data-state-type="ShippingState" class="form-control CountryID">
                                    <option value=""><?php echo lang("choose_country");?></option>
                                    <?php 
                                    foreach($countries as $Country)
                                    {
                                    ?>
                                        <option value="<?php echo $Country->CountryID; ?>" <?php echo ((isset($shipping_address['CountryID']) && $shipping_address['CountryID'] == $Country->CountryID) ? 'selected' : '' )?>><?php echo $Country->Title; ?></option>
                                    <?php 
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="ShippingState">State</label>
                                <select name="ShippingState" id="ShippingState" data-city-type="ShippingCity" class="form-control StateID">
                                    <option value=""><?php echo lang("choose_state");?></option>
                                    <?php
                                      if($shipping_states){
                                            foreach ($shipping_states as $key => $value) {
                                                echo '<option value="'.$value['StateID'].'" '.($shipping_address['StateID'] == $value['StateID'] ? 'selected' : '').'>'.$value['Title'].'</option>';
                                            }

                                      }

                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="ShippingCity">City</label>
                                <select name="ShippingCity" id="ShippingCity" class="form-control">
                                    <option value=""><?php echo lang("choose_city");?></option>
                                    <?php
                                      if($shipping_cities){
                                            foreach ($shipping_cities as $key => $value) {
                                                echo '<option value="'.$value['CityID'].'" '.($shipping_address['CityID'] == $value['CityID'] ? 'selected' : '').'>'.$value['Title'].'</option>';
                                            }

                                      }

                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="ShippingZipCode">Zip <span>*</span></label>
                                <input type="text" name="ShippingZipCode" class="form-control" id="ShippingZipCode" value="<?php echo $shipping_address['ZipCode'];?>">
                            </div>
                        </div>
                    </div>
                        
                        <hr>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <!--<a href="#" class="btn btn-success"><i class="fa fa-2x fa-edit"></i> Edit Profile</a>-->
                                <input type="submit" class="btn btn-success" value="Update">
                            </div>
                        </div>
                        <hr>
                    </form>
                    </div>
                    <!-- CONTENT -->
                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            
