<section class="page-section page-header breadcrumbs">
    <img src="<?php echo base_url('assets/backend');?>/img/regbg.jpg">
    <div class="container">
        <h3>New Account</h3>
    </div>

</section>
<div class="container-fluid">


    <div class="reg-form">
        <div class="row">

            <div class="col-md-8">

                <p>For all our exiting customers, please contact info@leggingspro.com to retrieve your pre-setup userID and password.</p>
            </div>
            <form action="<?php echo base_url();?>account/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                <input type="hidden" name="form_type" value="save_customer">
                <input type="hidden" name="RoleID" value="3">
                <div class="col-md-8">
                    <h3>General Information <span class="h5 text-red">(Your Email will be your login id)</span></h3>
                    <p>(Your Email will be your login id)</p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="contact">First Name <span>*</span></label>
                                <input type="text" name="FirstName" class="form-control" id="FirstName" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="contact">Last Name <span>*</span></label>
                                <input type="text" name="LastName" class="form-control" id="LastName" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Tel">Tel <span>*</span></label>
                                <input type="tel" name="Phone" class="form-control" id="Phone" required>
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Email">Email <span>*</span></label>
                                <input type="email" name="Email" class="form-control" id="Email" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="EmailConfirmation">Email Confirmation <span>*</span></label>
                                <input type="email" name="EmailConfirmation" class="form-control" id="EmailConfirmation" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Password">Password <span>*</span> (Min length 8 characters)</label>
                                <input type="password" name="Password" class="form-control" id="Password" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="PasswordConfirmation">Password Confirmation <span>*</span></label>
                                <input type="password" name="PasswordConfirmation" class="form-control" id="PasswordConfirmation" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Profile">Profile Image</label>
                                <input type="file" name="Image[]" class="form-control" id="Profile">
                            </div>
                        </div>

                    </div>
                    <hr>
                    
                    <h3>Billing Address</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="BillingAddress1">Address Line 1 <span>*</span></label>
                                <input type="text" name="BillingAddress1" class="form-control" id="BillingAddress1" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="BillingAddress2">Address Line 2</label>
                                <input type="text" name="BillingAddress2" class="form-control" id="BillingAddress2">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="BillingCountry">Country <span>*</span></label>
                                <select name="BillingCountry" id="BillingCountry" data-state-type="BillingState" class="form-control CountryID">
                                    <option value=""><?php echo lang("choose_country");?></option>
                                    <?php 
                                    foreach($countries as $Country)
                                    {
                                    ?>
                                        <option value="<?php echo $Country->CountryID; ?>"><?php echo $Country->Title; ?></option>
                                    <?php 
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="BillingState">State <span>*</span></label>
                                <select name="BillingState" id="BillingState" data-city-type="BillingCity" class="form-control StateID">
                                    <option value=""><?php echo lang("choose_state");?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="BillingCity">City <span>*</span></label>
                                <select name="BillingCity" id="BillingCity" class="form-control">
                                    <option value=""><?php echo lang("choose_city");?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="BillingZipCode">Zip</label>
                                <input type="text" name="BillingZipCode" class="form-control" id="BillingZipCode">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h3>Shipping Address</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="ShippingAddress1">Address Line 1<span>*</span></label>
                                <input type="text" name="ShippingAddress1" class="form-control" id="ShippingAddress1" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="ShippingAddress2">Address Line 2 </label>
                                <input type="text" name="ShippingAddress2" class="form-control" id="ShippingAddress2">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="ShippingCountry">Country <span>*</span></label>
                                <select name="ShippingCountry" id="ShippingCountry" data-state-type="ShippingState" class="form-control CountryID" required>
                                    <option value=""><?php echo lang("choose_country");?></option>
                                    <?php 
                                    foreach($countries as $Country)
                                    {
                                    ?>
                                        <option value="<?php echo $Country->CountryID; ?>"><?php echo $Country->Title; ?></option>
                                    <?php 
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="ShippingState">State <span>*</span></label>
                                <select name="ShippingState" id="ShippingState" data-city-type="ShippingCity" class="form-control StateID" required>
                                    <option value=""><?php echo lang("choose_state");?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="ShippingCity">City <span>*</span></label>
                                <select name="ShippingCity" id="ShippingCity" class="form-control" required>
                                    <option value=""><?php echo lang("choose_city");?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="ShippingZipCode">Zip</label>
                                <input type="text" name="ShippingZipCode" class="form-control" id="ShippingZipCode" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p>* You agree to receive newsletters by creating an account with us. If you do not wish to receive newsletters, please e-mail webadmin@leggingspro.com.</p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <input type="submit" name="CreateMyAcoount" class="btn btn-success" value="Create My Account">
                        </div>
                    </div>
                    <hr>
                </div>
            </form>
            <aside class="col-md-4 " >

                <div class="widget">
                    <div class="bannernew1">
                        <img src="<?php echo base_url('assets/backend');?>/img/bannernew1.jpg">

                    </div>

                </div>

                <div class="widget">
                    <div class="bannernew2">
                        <img src="<?php echo base_url('assets/backend');?>/img/bannernew2.jpg">

                    </div>

                </div>

            </aside>
            <!-- CONTENT -->
        </div>
    </div>
    <!-- SIDEBAR -->


</div>