<div class="container-fluid">

    <h3>Update Password</h3>
    <div class="reg-form">
        <div class="row">
            <form action="<?php echo base_url();?>account/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                <input type="hidden" name="form_type" value="updatePassword">
                <input type="hidden" name="PasswordRegenrateKey" value="<?php echo $PasswordRegenrateKey;?>">
                <input type="hidden" name="UserID" value="<?php echo $user['UserID'];?>">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="Password">Password <span>*</span></label>
                                    <input type="password" name="Password" class="form-control" id="Password" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="PasswordConfirmation">Password Confirmation <span>*</span></label>
                                    <input type="password" name="PasswordConfirmation" class="form-control" id="PasswordConfirmation" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <input type="submit" name="ChangePassword" class="btn btn-success" value="Change Password">
                        </div>
                    </div>
                    <hr>
                </div>
            </form>
            <!-- CONTENT -->
        </div>
    </div>
    <!-- SIDEBAR -->


</div>