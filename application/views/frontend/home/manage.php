<?php 
if(!empty($slider))
{
?>
    <section class="page-section no-padding slider">
            <div id="homeSlider" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <?php 
                    foreach ($slider as $key=>$img) {
                    ?>
                        <li data-target="#homeSlider" data-slide-to="<?php echo $key;?>" <?php echo ($key == 0 ? 'class="active"' : '');?>></li>
                    <?php
                    } 
                    ?>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <?php 
                        foreach ($slider as $key=>$img) {
                    ?>
                            <div class="item <?php echo ($key == 0 ? 'active' : '');?>">
                                <img src="<?php echo (file_exists($img['ImageName']) ? base_url() . $img['ImageName'] : base_url('uploads/no_image.png'));?>" alt="Los Angeles" width="1280" height="588" class="img-responsive">
                                <?php 
                                if($img['ImageDescription'] != '')
                                {
                                ?>
                                    <div class="carousel-caption">
                                        <?php echo $img['ImageDescription']; ?>
                                    </div>
                                <?php 
                                }
                                ?>
                                    <!-- <h3>Best<br>
                                        Legging <br>
                                        Collection
                                    </h3>
                                    <p>2018</p> -->
                            </div>
                    <?php 
                        }
                    ?>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#homeSlider" data-slide="prev">
                    <span class="fa fa-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#homeSlider" data-slide="next">
                    <span class="fa fa-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </section>
<?php
}
?>
<?php 
if(!empty($banner_after_slider))
{
?>
    <section class="page-sale">
        <div class="container-fluid">
            <div class="row">
                <?php 
                    foreach ($banner_after_slider as $key=>$img) {
                ?>
                        <div class="col-md-3 mt-20">
                           <a href="<?php echo ($img['RedirectLink'] != '' ? $img['RedirectLink'] : 'javascript:void(0)');?>"> <img src="<?php echo (file_exists($img['ImageName']) ? base_url() . $img['ImageName'] : base_url('uploads/no_image.png'));?>" alt="" width="700" height="307" class="img100 img-responsive"></a>
                           <?php 
                                if($img['ImageDescription'] != '')
                                {
                                ?>
                                    <div class="carousel-caption">
                                        <?php echo $img['ImageDescription']; ?>
                                    </div>
                                <?php 
                                }
                                ?>
                        </div>

                <?php 
                    }
                ?>
            </div>
        </div>
    </section>
<?php
}
?>

<?php 
if(!empty($featured_products))
{
?>
    <section class="page-section">
        <div class="container-fluid">
            <h2 class="section-title1"><span>FEATURED PRODUCTS</span><br>
                <img src="<?php echo base_url('assets/backend');?>/img/line.jpg">

            </h2>

            <div class="top-products-carousel">
                <div class="owl-carousel top-products-carousel">

                
                <?php 
                    foreach ($featured_products as $product) {
                        $product_images = getSiteImages($product->ProductID, 'ProductImage');
                        $RemainingProducts = getRemainingProductQuantity($product->ProductID);
                        $DiscountPer = (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? $product->WholeSaleDiscount : $product->Discount);
                        $DiscountPrice = 0;
                        if($DiscountPer > 0)
                        {
                            $Price = (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? $product->PriceForWholeSaler : $product->Price);

                            $Discount = $Price * ($DiscountPer/100);

                            $DiscountPrice = $Price - $Discount;
                        }
                ?>
                        <div class="thumbnail no-border no-padding">
                            <div class="media">
                                <a class="media-link" data-gal="prettyPhoto" href="<?php echo ($RemainingProducts <= 0 ? 'javascript:void(0);' : base_url('products/detail/'.$product->ProductID));?>">
                                    <img src="<?php echo (file_exists($product_images[0]['ImageName']) ? base_url() . $product_images[0]['ImageName'] : base_url('uploads/no_image.png'));?>" alt=""/>
                                </a>
                            </div>
                            <div class="caption text-center">
                                <h4 class="caption-title"><a href="<?php echo ($RemainingProducts <= 0 ? 'javascript:void(0);' : base_url('products/detail/'.$product->ProductID));?>"><?php echo $product->Title;?></a></h4>

                                <div class="product-description">
                                                    Lorem ipsum es to dolor porque lorem ipsum es to dolor porque lorem ipsum.
                                                </div>

                                <div class="price">
                                    <ins class="discount" <?php echo ($DiscountPrice>0 ? 'style="text-decoration: line-through; color:red"' : ''); ?>><?php echo (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? getSelectedCurrencies($CurrencyID,$product->PriceForWholeSaler) : getSelectedCurrencies($CurrencyID,$product->Price));?></ins>

                                    <?php 
                                    if($DiscountPrice>0)
                                    {
                                    ?>
                                      
                                        <ins style="font-size: 18px;"><?php echo getSelectedCurrencies($CurrencyID,$DiscountPrice)?></ins>
                                    <?php
                                    }
                                    ?>
                                    <?php 
                                    /*if($RemainingProducts <= 0)
                                    {
                                    ?>
                                        <p class="out-of-stock">Out of Stock</p>
                                    <?php 
                                    }*/
                                    ?>
                                </div>
                                <div class="buttons">
                                    <a class="btn btn-grey" <?php echo ($RemainingProducts <= 0 ? 'disabled="disabled"' : '');?> href="<?php echo ($RemainingProducts <= 0 ? 'javascript:void(0);' : base_url('products/detail/'.$product->ProductID));?>"><?php echo ($RemainingProducts <= 0 ? 'Out of stock' : 'Add to Cart');?></a>
                                    <!--
                                -->
                                </div>
                            </div>
                        </div>
                <?php 
                    }
                ?>
                </div>
            </div>
        </div>
    </section>
<?php
}
?>

<?php 
if(!empty($banner_after_featur_prod))
{
?>
    <section class="page-section">

        <div class="container-fluid">
            <div class="row girl">
                <div class="col-md-12 no-padding">
                    <a href="<?php echo ($img['RedirectLink'] != '' ? $img['RedirectLink'] : 'javascript:void(0)');?>"><img width="100%" src="<?php echo (file_exists($banner_after_featur_prod[0]['ImageName']) ? base_url() . $banner_after_featur_prod[0]['ImageName'] : base_url('uploads/no_image.png'));?>"></a>
                    <?php 
                                if($banner_after_featur_prod[0]['ImageDescription'] != '')
                                {
                                ?>
                                    <div class="carousel-caption">
                                        <?php echo $banner_after_featur_prod[0]['ImageDescription']; ?>
                                    </div>
                                <?php 
                                }
                                ?>

                </div>
                <!-- <div class="col-md-7 arrival ">
                    <div class="new-arrival text-center">
                        <h1>NEW</h1>
                        <h2>ARRIVALS</h2>
                        <h3>2018</h3>
                    </div>
                </div> -->
            </div>
        </div>

    </section>
<?php
}
?>

<?php 
if(!empty($latest_products))
{
?>
    <section class="page-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="section-title1">
                        <span>LATEST PRODUCTS</span><br>
                        <img src="<?php echo base_url('assets/backend');?>/img/line.jpg">
                    </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="top-products-carousel">
                        <div class="owl-carousel top-products-carousel">
                            <?php 
                                foreach ($latest_products as $product) {
                                    $product_images = getSiteImages($product->ProductID, 'ProductImage');
                                    $RemainingProducts = getRemainingProductQuantity($product->ProductID);
                                    $DiscountPer = (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? $product->WholeSaleDiscount : $product->Discount);
                                    $DiscountPrice = 0;
                                    if($DiscountPer > 0)
                                    {
                                        $Price = (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? $product->PriceForWholeSaler : $product->Price);

                                        $Discount = $Price * ($DiscountPer/100);

                                        $DiscountPrice = $Price - $Discount;
                                    }
                            ?>
                                    <div class="thumbnail no-border no-padding">
                                        <div class="media">
                                            <a class="media-link" data-gal="prettyPhoto" href="<?php echo ($RemainingProducts <= 0 ? 'javascript:void(0);' : base_url('products/detail/'.$product->ProductID));?>">
                                                <img src="<?php echo (file_exists($product_images[0]['ImageName']) ? base_url() . $product_images[0]['ImageName'] : base_url('uploads/no_image.png'));?>" alt=""/>
                                            </a>
                                        </div>
                                        <div class="caption text-center">
                                            <h4 class="caption-title"><a href="<?php echo ($RemainingProducts <= 0 ? 'javascript:void(0);' : base_url('products/detail/'.$product->ProductID));?>"><?php echo $product->Title;?></a></h4>

                                            <div class="product-description">
                                                    Lorem ipsum es to dolor porque lorem ipsum es to dolor porque lorem ipsum.
                                                </div>

                                            <div class="price">
                                                <ins class="discount" <?php echo ($DiscountPrice>0 ? 'style="text-decoration: line-through; color:red"' : ''); ?>><?php echo (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? getSelectedCurrencies($CurrencyID,$product->PriceForWholeSaler) : getSelectedCurrencies($CurrencyID,$product->Price));?></ins>

                                                <?php 
                                                if($DiscountPrice>0)
                                                {
                                                ?>
                                                    
                                                    <ins style="font-size: 18px;"><?php echo getSelectedCurrencies($CurrencyID,$DiscountPrice)?></ins>
                                                <?php
                                                }
                                                ?>
                                                <?php 

                                                /*if($RemainingProducts <= 0)
                                                {
                                                ?>
                                                    <p class="out-of-stock">Out of Stock</p>
                                                <?php 
                                                }*/
                                                ?>
                                            </div>
                                            <div class="buttons">
                                                <a class="btn btn-grey" <?php echo ($RemainingProducts <= 0 ? 'disabled="disabled"' : '');?> href="<?php echo ($RemainingProducts <= 0 ? 'javascript:void(0);' : base_url('products/detail/'.$product->ProductID));?>"><?php echo ($RemainingProducts <= 0 ? 'Out of stock' : 'BUY');?></a>
                                            </div>
                                        </div>
                                    </div>
                            <?php 
                                }
                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
}
?>

<?php 
if(!empty($home_page_cat_images))
{
?>
    <section class="page-section">
        <div class="container-fluid">
            <div class="row">
                <?php 
                    foreach ($home_page_cat_images as $key=>$img) {
                ?>
                        <div class="col-md-4">
                            <div class="container-images">
                                <a href="<?php echo ($img['RedirectLink'] != '' ? $img['RedirectLink'] : 'javascript:void(0)');?>"><img src="<?php echo (file_exists($img['ImageName']) ? base_url() . $img['ImageName'] : base_url('uploads/no_image.png'));?>">
                            </div>
                             <?php 
                                if($img['ImageDescription'] != '')
                                {
                                ?>
                                    <div class="carousel-caption">
                                        <?php echo $img['ImageDescription']; ?>
                                    </div>
                                <?php 
                                }
                                ?>
                        </div>
                <?php 
                    }
                ?>
            </div>
        </div>
    </section>
<?php
}
?>

<?php 
if(!empty($banner_below_cat))
{
?>
    <section class="page-bannrelong">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <a href="<?php echo ($img['RedirectLink'] != '' ? $img['RedirectLink'] : 'javascript:void(0)');?>"><img class="img100 img-responsive" src="<?php echo (file_exists($banner_below_cat[0]['ImageName']) ? base_url() . $banner_below_cat[0]['ImageName'] : base_url('uploads/no_image.png'));?>"></a>
                     <?php 
                                if($banner_below_cat[0]['ImageDescription'] != '')
                                {
                                ?>
                                    <div class="carousel-caption">
                                        <?php echo $banner_below_cat[0]['ImageDescription']; ?>
                                    </div>
                                <?php 
                                }
                                ?>
                </div>
            </div>
        </div>
    </section>
<?php
}
?>

<?php 
if(!empty($banner_above_foot))
{
?>
    <section class="page-section">
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Carousel indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>   
                <!-- Wrapper for carousel items -->
                <div class="carousel-inner">
                    <div class="item carousel-item active">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="testimonial-wrapper">
                                    <div class="testimonial">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante, commodo iacul viverra.</div>
                                    <div class="media">
                                        <div class="media-left d-flex mr-3">
                                            <img src="<?php echo base_url('assets/backend');?>/img/testi.jpg" alt="">                                       
                                        </div>
                                        <div class="media-body">
                                            <div class="overview">
                                                <div class="name"><b>Paula Wilson</b></div>
                                                <div class="details">Customer</div>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-half-o"></i></li>
                                                    </ul>
                                                </div>
                                            </div>                                      
                                        </div>
                                    </div>
                                </div>                              
                            </div>
                            <div class="col-sm-6">
                                <div class="testimonial-wrapper">
                                    <div class="testimonial">Vestibulum quis quam ut magna consequat faucibus. Pellentesque eget mi suscipit tincidunt. Utmtc tempus dictum. Pellentesque virra. Quis quam ut magna consequat faucibus, metus id mi gravida.</div>
                                    <div class="media">
                                        <div class="media-left d-flex mr-3">
                                            <img src="<?php echo base_url('assets/backend');?>/img/testi.jpg" alt="">                                       
                                        </div>
                                        <div class="media-body">
                                            <div class="overview">
                                                <div class="name"><b>Antonio Moreno</b></div>
                                                <div class="details">Customer</div>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                              
                            </div>
                        </div>          
                    </div>
                    <div class="item carousel-item">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="testimonial-wrapper">
                                    <div class="testimonial">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante, commodo iacul viverra.</div>
                                    <div class="media">
                                        <div class="media-left d-flex mr-3">
                                            <img src="<?php echo base_url('assets/backend');?>/img/testi.jpg" alt="">                                        
                                        </div>
                                        <div class="media-body">
                                            <div class="overview">
                                                <div class="name"><b>Michael Holz</b></div>
                                                <div class="details">Customer</div>                                          
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                              
                            </div>
                            <div class="col-sm-6">
                                <div class="testimonial-wrapper">
                                    <div class="testimonial">Vestibulum quis quam ut magna consequat faucibus. Pellentesque eget mi suscipit tincidunt. Utmtc tempus dictum. Pellentesque virra. Quis quam ut magna consequat faucibus, metus id mi gravida.</div>
                                    <div class="media">
                                        <div class="media-left d-flex mr-3">
                                            <img src="<?php echo base_url('assets/backend');?>/img/testi.jpg" alt="">                                         
                                        </div>
                                        <div class="media-body">
                                            <div class="overview">
                                                <div class="name"><b>Mary Saveley</b></div>
                                                <div class="details">Customer</div>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                            </div>                                      
                                        </div>
                                    </div>
                                </div>                              
                            </div>
                        </div>          
                    </div>
                    <div class="item carousel-item">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="testimonial-wrapper">
                                    <div class="testimonial">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante, commodo iacul viverra.</div>
                                    <div class="media">
                                        <div class="media-left d-flex mr-3">
                                            <img src="<?php echo base_url('assets/backend');?>/img/testi.jpg" alt="">                                         
                                        </div>
                                        <div class="media-body">
                                            <div class="overview">
                                                <div class="name"><b>Martin Sommer</b></div>
                                                <div class="details">Customer</div>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                            </div>                                      
                                        </div>
                                    </div>
                                </div>                              
                            </div>
                            <div class="col-sm-6">
                                <div class="testimonial-wrapper">
                                    <div class="testimonial">Vestibulum quis quam ut magna consequat faucibus. Pellentesque eget mi suscipit tincidunt. Utmtc tempus dictum. Pellentesque virra. Quis quam ut magna consequat faucibus, metus id mi gravida.</div>
                                    <div class="media">
                                        <div class="media-left d-flex mr-3">
                                            <img src="<?php echo base_url('assets/backend');?>/img/testi.jpg" alt="">                                      
                                        </div>
                                        <div class="media-body">
                                            <div class="overview">
                                                <div class="name"><b>John Williams</b></div>
                                                <div class="details">Customer</div>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                            </div>                                      
                                        </div>
                                    </div>
                                </div>                              
                            </div>
                        </div>          
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    </section>
<?php
}
?>
    <div>
        <div>