<div class="content-area">
   <section class="product-detail">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12">
               <div class="row">
                  <div class="col-md-4 col-xs-12">
                     <div class="carousel slide productSlider" id="productImgSlider">
                        <div class="product-img">
                           <div class="carousel-inner cont-slider">
                              <?php 
                                 $RemainingProducts = getRemainingProductQuantity($product[0]->ProductID);
                                 $DiscountPer = (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? $product[0]->WholeSaleDiscount : $product[0]->Discount);
                                 $DiscountPrice = 0;
                                 if($DiscountPer > 0)
                                 {
                                     $Price = (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? $product[0]->PriceForWholeSaler : $product[0]->Price);
                                 
                                     $Discount = $Price * ($DiscountPer/100);
                                 
                                     $DiscountPrice = $Price - $Discount;
                                 }
                                 $product_images = getSiteImages($product[0]->ProductID, 'ProductImage');
                                 foreach ($product_images as $key=>$img) {
                                     
                                 ?>
                              <div class="item easyzoom easyzoom--overlay <?php echo ($key == 0 ? 'active' : '');?>">
                                 <a href="<?php echo (file_exists($img['ImageName']) ? base_url() . $img['ImageName'] : base_url('uploads/no_image.png'));?>">
                                 <img src="<?php echo (file_exists($img['ImageName']) ? base_url() . $img['ImageName'] : base_url('uploads/no_image.png'));?>" alt="" >
                                 </a>
                              </div>
                              <?php 
                                 }
                                 ?>
                           </div>
                           <a class="carousel-arrow left" href="#productImgSlider" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
                           <a class="carousel-arrow right" href="#productImgSlider" data-slide="next"><i class="fa fa-chevron-right"></i></a>
                        </div>
                        <!-- Indicators -->
                        <ol class="carousel-indicators visible-lg visible-md">
                           <?php 
                              foreach ($product_images as $key=>$img) {
                                  
                              ?>
                           <li class="<?php echo ($key == 0 ? 'active' : '');?>" data-slide-to="<?php echo $key;?>" data-target="#productImgSlider">
                              <img src="<?php echo (file_exists($img['ImageName']) ? base_url() . $img['ImageName'] : base_url('uploads/no_image.png'));?>" alt="" width="360" height="692" class="img-responsive">
                           </li>
                           <?php 
                              }
                              ?>
                        </ol>
                     </div>
                  </div>
                  <div class="col-md-6 col-xs-12">
                     <div class="row">
                        <div class="col-md-11 col-xs-11">
                           <div class="product-title"><?php echo $product[0]->Title;?></div>
                        </div>
                        <?php
                           if($this->session->userdata('admin')){ 
                               
                           
                               $already = checkWishlist($product[0]->ProductID);
                               if(!$already){
                               ?>
                        <div class="col-md-1 col-xs-1">
                           <a href="javascript:void(0);" class="add_to_wishlist btn btn-default  pull-right" data-product="<?php echo $product[0]->ProductID;?>"><i class="fa fa-heart"></i></a>
                        </div>
                        <?php }else{ ?>
                        <a href="<?php echo base_url('products/myWishlist');?>">Your wishlist product</a>
                        <?php } }?>
                     </div>
                     <?php if($reviews){
                        $avg_rating = getProductAvgRating($product[0]->ProductID); 
                        
                           ?>
                     <div class="row">
                        <div class="col-md-12 col-xs-12">
                           <div class="product-rating">
                              <?php for($i = 1; $i <= 5; $i++) { 
                                 if($i <= $avg_rating){
                                 ?>
                              <i class="fa fa-star gold"></i> 
                              <?php }else{ ?>
                              <i class="fa fa-star-o"></i>
                              <?php  } } ?>
                           </div>
                        </div>
                     </div>
                     <?php } ?>
                     <div class="row">
                        <div class="col-md-12 col-xs-12">
                           <div class="price">
                              <h3 class="discount" <?php echo ($DiscountPrice>0 ? 'style="text-decoration: line-through; color:red; margin-right: 40px;"' : ''); ?>><b><?php echo (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? getSelectedCurrencies($CurrencyID, $product[0]->PriceForWholeSaler) : getSelectedCurrencies($CurrencyID,$product[0]->Price));?></b></h3>
                              <?php 
                                 if($DiscountPrice>0)
                                 {
                                 ?>
                              <h3 class="discount" style="color: #111111;"><b><?php echo getSelectedCurrencies($CurrencyID,$DiscountPrice)?></b></h3>
                              <?php
                                 }
                                 ?>
                              <?php 
                                 if($RemainingProducts <= 0)
                                 {
                                 ?>
                              <p class="out-of-stock">Out of Stock</p>
                              <?php 
                                 }
                                 ?>
                           </div>
                        </div>
                     </div>
                     <hr>
                     <div class="row">
                        <div class="col-md-12 col-xs-12">
                           <div class="productView-options">
                              <form class="form" method="post" action="" data-cart-item-add="">
                                 <input type="hidden" name="action" value="">
                                 <input type="hidden" name="product_id" value="">
                                 <div data-product-option-change="" style="">
                                    <?php 
                                       if(isset($this->session->userdata['admin']['RoleID']) && $this->session->userdata['admin']['RoleID'] == 4 && !empty($product_packets)){ 
                                       ?>
                                    <div class="form-field" data-product-attribute="set-rectangle">
                                       <label class="form-label form-label--alternate form-label--inlineSmall">
                                          Packets:
                                          <small>Required</small>
                                          <div>
                                             <select class="form-control" id="product_packets" onchange="showPackDetail(this.value);">
                                                <?php 
                                                   foreach($product_packets as $packs)
                                                   {
                                                   ?>
                                                <option value="<?php echo $packs['ProductPacketID']?>"><?php echo $packs['TotalPackets']?> Pieces</option>
                                                <?php 
                                                   }
                                                   ?>
                                             </select>
                                          </div>
                                    </div>
                                    <?php 
                                       }
                                       else
                                       {
                                       ?>
                                    <div class="form-field" data-product-attribute="set-rectangle">
                                    <label class="form-label form-label--alternate form-label--inlineSmall">
                                    Size:
                                    <small>Required</small>
                                    </label>
                                    <div>
                                    <?php 
                                       $Sizes = explode(',', $product[0]->Sizes);
                                       foreach($Sizes as $key=>$size)
                                       {
                                           $sizedDet = getSizeDetail($size);
                                       ?>
                                    <label class="cus-size">
                                    <input type="radio" <?php echo ($key == 0 ? 'checked="checked"' : '');?> name="size" value="<?php echo $sizedDet['Title'];?>">
                                    <span class="checkmark-2"><?php echo $sizedDet['Title'];?></span>
                                    </label>
                                    <?php 
                                       }
                                       ?>
                                    </div>
                                    </div>
                                    <div class="form-field" data-product-attribute="swatch">
                                       <label class="form-label form-label--alternate form-label--inlineSmall">
                                       Color:
                                       <small>Required</small>
                                       </label>
                                       <?php 
                                          $Colors = explode(',', $product[0]->Colors);
                                          foreach($Colors as $key=>$color)
                                          {
                                              $colorDet = getColorDetail($color);
                                          ?>
                                       <label class="colors">
                                       <input type="radio" <?php echo ($key == 0 ? 'checked="checked"' : '');?> name="color" value="<?php echo $colorDet['Title'];?>">
                                       <span class="checkmark-1" style="background: <?php echo strtolower($colorDet['Title']);?>;"></span>
                                       </label>
                                       <?php 
                                          }
                                          ?>
                                    </div>
                                    <?php
                                       }
                                       ?>
                                 </div>
                              </form>
                              <div class="product-quantity">
                                 <div class="form-field form-field--increments">
                                    <label class="form-label form-label--alternate">Quantity:</label>
                                    <span><input type="button" value="-" id="moins" onclick="minus()" class="count-btn"></span>
                                    <span><input type="text" size="25" value="1" id="quantity_field" class="quantity" min="1"></span>
                                    <span><input type="button" value="+" id="plus" onclick="plus()" class="count-btn"></span>
                                 </div>
                              </div>
                              <?php 
                                 if(isset($this->session->userdata['admin']['RoleID']) && $this->session->userdata['admin']['RoleID'] == 4 && !empty($product_packets)){ 
                                 ?>
                              <div class="clearfix">
                              </div>
                              <div class="form-field" data-product-attribute="set-rectangle" style="width: 47%;height: 180px;">
                                 <label class="form-label form-label--alternate form-label--inlineSmall">
                                 Packet Detail:
                                 </label>
                                 <?php 
                                    foreach($product_packets as $key=>$packs)
                                    {
                                        $getArray = json_decode($packs['PacketsData']);
                                    ?>
                                 <div class="packetDetail" id="packetDetail-<?php echo $packs['ProductPacketID']; ?>" <?php echo ($key>0 ? 'style="display: none"' : '');?>>
                                    <h6><strong><?php echo $packs['TotalPackets']?> Pieces</strong></h6>
                                    <?php 
                                       foreach ($getArray as $singPack) {
                                           $sizeDet = getSizeDetail($singPack->size);
                                           $colorDet = getColorDetail($singPack->color);
                                       ?>
                                    <p style="font-size: 10px;"><?php echo $singPack->piece; ?> Pieces of <b>Size: </b><?php echo $sizeDet['Title']; ?> and <b>Color: </b><?php echo $colorDet['Title']; ?></p>
                                    <?php
                                       }
                                       ?>
                                 </div>
                                 <?php 
                                    }   
                                    ?>
                              </div>
                              <?php
                                 }
                                 ?>
                           </div>
                        </div>
                     </div>
                     <?php 
                        if (isset($this->session->userdata['admin']['UserID'])) {
                        
                            $user_id = $this->session->userdata['admin']['UserID'];
                        
                        } else {
                            if (!get_cookie('temp_order_key')) {
                                $user_id = 0;
                                
                            } else {
                                $user_id = get_cookie('temp_order_key');
                            }
                        }
                        if($user_id > 0)
                        {
                            $checkProd = checkUserProductCart($user_id, $product[0]->ProductID);
                        }
                        else
                        {
                            $checkProd = 0;
                        }
                        ?>
                     <div class="row">
                        <div class="col-md-4 col-xs-12">
                           <div class="btn-group cart">
                              <button type="button" data-pro-id="<?php echo $product[0]->ProductID;?>" class="btn btn-success add_to_cart btnstyle">
                              ADD TO CART
                              </button>
                           </div>
                        </div>
                        <?php
                           if($checkProd > 0)
                           {
                               $parent_category = 0;
                               if($product[0]->SubCategoryIDLevel2 != 0)
                               {
                                   $parent_category = $product[0]->SubCategoryIDLevel2;
                               }
                               elseif($product[0]->SubCategoryID != 0)
                               {
                                   $parent_category = $product[0]->SubCategoryID;
                               }
                               else
                               {
                                   $parent_category = $product[0]->CategoryID;
                               }
                           ?> 
                     
                        <div class="col-md-8 col-xs-12">
                        <div class="btn-group cart">
                              <a href="<?php echo base_url('products/index/'.$parent_category);?>">
                              <button type="button" class="btn btn-default pull-right">
                              Continue Shopping
                              </button>
                              </a>
                           </div>
                           <div class="btn-group cart">
                              <a href="<?php echo base_url('checkout');?>">
                              <button type="button" class="btn btn-default  pull-right">
                              Checkout
                              </button>
                              </a>
                           </div>
                        </div>
                        <?php
                           }
                           ?>
                     </div>
                     <div class="row">
                        <div class="col-md-12 col-xs-12">
                           <div class="product-info">
                              <ul class="nav nav-tabs nav_tabs">
                                 <li class=" description active"><a href="#proTab-one" data-toggle="tab">DESCRIPTION</a>
                                 </li>
                                 <li><a href="#proTab-two" data-toggle="tab">PRODUCT INFO</a></li>
                                 <li><a href="#proTab-three" data-toggle="tab">REVIEWS (<?php if($reviews) { echo count($reviews); }else{ echo '0'; } ?>)</a></li>
                              </ul>
                              <div class="tab-content">
                                 <div class="tab-pane fade in active" id="proTab-one">
                                    <div class="product-desc ">
                                       <?php echo $product[0]->Description;?>
                                    </div>
                                 </div>
                                 <div class="tab-pane fade" id="proTab-two">
                                    <div class="product-desc product-info">
                                       <?php echo $product[0]->Description;?>
                                    </div>
                                 </div>
                                 <div class="tab-pane fade" id="proTab-three">
                                    <div class="product-desc product-review">
                                       <?php if($reviews) { ?>
                                       <div class="row">
                                          <?php foreach($reviews as $review) { ?>
                                          <div class="col-sm-12">
                                             <div class="user-review">
                                                <div class="user-avatar clearfix">
                                                   <div class="user-img">
                                                      <?php if(file_exists($review['Image'])){
                                                         $image = base_url($review['Image']);
                                                         }else{
                                                         $image = base_url('assets/backend/img/no_img.png');
                                                         }
                                                         ?>
                                                      <img src="<?php echo $image ?>">
                                                   </div>
                                                   <div class="user-avatar-right">
                                                      <div class="user-name"><?php echo $review['FullName'] ?></div>
                                                      <div class="product-rating">
                                                         <?php for($i = 1; $i <= 5; $i++) { 
                                                            if($i <= $review['Rating']){
                                                            ?>
                                                         <i class="fa fa-star gold"></i> 
                                                         <?php }else{ ?>
                                                         <i class="fa fa-star-o"></i>
                                                         <?php  } } ?>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="review-timestamp"><?php echo date('F d, Y', strtotime($review['CreatedAt'])); ?></div>
                                                <div class="review-text"><?php echo $review['Review'] ?></div>
                                             </div>
                                          </div>
                                          <?php } ?>
                                       </div>
                                       <?php } ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-12">
                           <?php if($this->session->userdata('admin')){ ?>
                           <?php if(canWriteReview($this->session->userdata('admin')['UserID'],$product[0]->ProductID)){ ?>
                           <form method="post" class="form_data" action="<?php echo base_url("/products/addreview") ?>">
                              <input type="hidden" name="ProductID" value="<?php echo $product[0]->ProductID?>">
                              <h3>Leave a Comment</h3>
                              <input type="text" class="form-control" placeholder="Write a Comment here" name="review"></textarea>
                              <h3>Rate This Product</h3>
                              <div class="rating">
                                 <input type="radio" id="star5" name="rating" value="5" /><label for="star5"></label>
                                 <input type="radio" id="star4" name="rating" value="4" /><label for="star4"></label>
                                 <input type="radio" id="star3" name="rating" value="3" /><label for="star3"></label>
                                 <input type="radio" id="star2" name="rating" value="2" /><label for="star2"></label>
                                 <input type="radio" id="star1" name="rating" value="1" /><label for="star1"></label>
                              </div>
                              <button class="btn btn-success pull-right" type="submit">Save</button>
                           </form>
                           <?php } ?>
                           <?php } ?>
                        </div>
                     </div>
                     <!--<div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="product-shrt-desc">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                            </div>
                        </div>
                        </div>-->
                  </div>
                  <div class="col-md-2 col-xs-12">
                      <img width="100%" src="<?php echo base_url(); ?>assets/backend/img/banner-new.jpg">
                  </div>
               </div>
               <hr>
            </div>
         </div>
      </div>
   </section>
</div>
<script type="text/javascript">
   var count  = 1;
   var countEl = document.getElementById("quantity_field");
   
   $( document ).ready(function() {
       
         count = $("#quantity_field").val();
   
   
         $(".add_to_wishlist").on('click',function(){
               $.ajax({
               type: "POST",
               url: base_url + 'products/addToWishlist',
               data: {
                   'ProductID': $(this).attr('data-product')
               },
               dataType: "json",
               cache: false,
               //async:false,
               success: function (result) {
   
                   if (result.error != false) {
                       showError(result.error);
                   } else {
                       showSuccess(result.success);
                       
                   }
   
   
                  setTimeout(function () {
                       window.location.reload();
                   }, 1000);
   
               },
               complete: function () {
                   $.unblockUI();
               }
           });
         });
         
      
   });
   function plus() {
       count++;
       countEl.value = count;
   }
   
   function minus() {
       if (count > 1) {
           count--;
           if(count >= $("#quantity_field").attr('min') ){
                countEl.value = count;
   
           }else{
               alert('You can not buy less than' + $("#quantity_field").attr('min') +' items');
           }
          
       }
   }
   
   function showPackDetail(ProductPacketID)
   {
       $('.packetDetail').hide();
   
       $('#packetDetail-'+ProductPacketID).show();
   }
</script>