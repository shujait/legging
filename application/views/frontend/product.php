<div class="content-area">

    <section class="page-section page-header breadcrumbs">
        <img src="<?php echo base_url();?>assets/backend/img/banner.jpg">
        <div class="container">
            <h3><?php echo (is_array($categoryDet) ? $categoryDet[0]->Title : 'Search Result: '.$categoryDet);?></h3>
        </div>

    </section>

    <section class="page-section with-sidebar">
        <div class="container-fluid">
            <div class="row">
                <!-- CONTENT -->
                <div class="col-md-12 content" id="content">
                    <?php 
                        if($products)
                        {
                    ?>      <input type="hidden" id="CategoryID" value="<?php echo (count($TotalProducts) <= 8 && is_array($categoryDet) ? $categoryDet[0]->CategoryID : (is_array($categoryDet) ? $categoryDet[0]->CategoryID : $categoryDet));?>">
                            <!-- shop-sorting -->
                            <div class="shop-sorting">
                                <div class="row">
                                    <?php $sizes = getAllSizes(); 
                                    if($sizes)
                                    {
                                    ?>
                                        <div class="col-md-4 col-xs-12 text-right">

                                            <div class="ButtonFilter">
                                                <?php 
                                                
                                                foreach($sizes as $size)
                                                {
                                                ?>

                                                    <a data-size="<?php echo $size->SizeID;?>" class="SelectSize" href=""><?php echo $size->Title;?></a>

                                                <?php 
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    <?php 
                                    }
                                    ?>


                                    <div class="col-md-2 col-xs-12 text-right">
                                        <form class="form-inline" action="">
                                            <div class="form-group selectpicker-wrapper">
                                                <label for="sort" class="hide"> </label>

                                                <select class="form-control" onchange="GetFilteredProducts();" id="sortOrder" title="sorting">
                                                    <option value="">Sort: Recommended</option>
                                                    <option value="(Low-High)">Sort: Price (Low-High)</option>
                                                    <option value="(High-Low)">Sort: Price (High-Low)</option>
                                                    <option value="(A-Z)">Sort: Name (A-Z)</option>
                                                    <option value="(Z-A)">Sort: Name (Z-A)</option>

                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                    <?php 
                                        $currencies = getActiveCurrencies();
                                        if($currencies)
                                        {
                                    ?>
                                            <div class="col-md-3 col-xs-12 text-right">
                                                <div class="currency">

                                                    <label for="currency" class="hide"> </label>
                                                    
                                                    <select class="form-control" onchange="GetFilteredProducts();" id="currency">
                                                        <?php foreach($currencies as $currency){?>
                                                            <option <?php echo ($CurrencyID == $currency->DollerSystemID ? 'selected="selected"' : '');?> value="<?php echo $currency->DollerSystemID;?>"><?php echo $currency->Title . ' ('.$currency->Code.')';?></option>
                                                        <?php }?>
                                                    </select>
                                                </div>

                                            </div>
                                    <?php 
                                        }
                                    ?>
                                    
                                </div>
                            </div>
                            <!-- /shop-sorting -->
                    <?php 
                    }
                    ?>


                    <!-- Products grid -->
                    <div class="row products grid">
                        <div id="MoreProducts">
                            <?php 
                            if($products)
                            {
                                foreach ($products as $product) {
                                    $product_images = getSiteImages($product->ProductID, 'ProductImage');
                                    $RemainingProducts = getRemainingProductQuantity($product->ProductID);
                                    $DiscountPer = (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? $product->WholeSaleDiscount : $product->Discount);
                                    $DiscountPrice = 0;
                                    if($DiscountPer > 0)
                                    {
                                        $Price = (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? $product->PriceForWholeSaler : $product->Price);

                                        $Discount = $Price * ($DiscountPer/100);

                                        $DiscountPrice = $Price - $Discount;
                                    }
                            
                            ?>
                                    <div class="col-md-3 col-sm-6 product-list">
                                        <div class="thumbnail no-border no-padding">
                                            <div class="media">
                                                <a class="media-link" data-gal="prettyPhoto" href="<?php echo ($RemainingProducts <= 0 ? 'javascript:void(0);' : base_url('products/detail/'.$product->ProductID));?>">
                                                    <img src="<?php echo (file_exists($product_images[0]['ImageName']) ? base_url() . $product_images[0]['ImageName'] : base_url('uploads/no_image.png'));?>" alt=""/>
                                                </a>
                                            </div>
                                            <div class="caption text-center" style="min-height: 250px">
                                                <h4 class="caption-title"><a href="<?php echo ($RemainingProducts <= 0 ? 'javascript:void(0);' : base_url('products/detail/'.$product->ProductID));?>"><?php echo $product->Title;?></a></h4>

                                                <div class="product-description">
                                                    Lorem ipsum es to dolor porque lorem ipsum es to dolor porque lorem ipsum.
                                                </div>

                                                <div class="price">
                                                    <ins class="discount" <?php echo ($DiscountPrice>0 ? 'style="text-decoration: line-through; color:red"' : ''); ?>><?php echo (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? getSelectedCurrencies($CurrencyID, $product->PriceForWholeSaler) : getSelectedCurrencies($CurrencyID,$product->Price));?></ins>

                                                    <?php 
                                                    if($DiscountPrice>0)
                                                    {
                                                    ?>
                                                        <ins style="font-size: 18px;"><?php echo getSelectedCurrencies($CurrencyID,$DiscountPrice)?></ins>
                                                    <?php
                                                    }
                                                    ?>
                                                </div>
                                                <div class="buttons">
                                                    <a class="btn btn-grey" <?php echo ($RemainingProducts <= 0 ? 'disabled="disabled"' : '');?> href="<?php echo ($RemainingProducts <= 0 ? 'javascript:void(0);' : base_url('products/detail/'.$product->ProductID));?>"><?php echo ($RemainingProducts <= 0 ? 'Out of stock' : 'Add to Cart');?></a>
                                                    <!--
                                                -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                            <?php 
                                }
                            ?>
                        
                        </div>
                        <?php
                            if(count($TotalProducts) > 8)
                            {
                            
                        ?>
                                <div class="col-md-12 col-xs-12 text-center mt-20 parentLoadMoreDiv">
                                    <input type="hidden" id="TotalCount" value="<?php echo count($TotalProducts);?>">
                                    <input type="hidden" id="StartRange" value="8">
                                    <a href="javascript:void(0);" data-category-id="<?php echo (count($TotalProducts) <= 8 && is_array($categoryDet) ? $categoryDet[0]->CategoryID : (is_array($categoryDet) ? $categoryDet[0]->CategoryID : $categoryDet));?>" class="btn btn-success btn-lg LoadMoreProducts">Show More</a>
                                </div>
                        <?php 
                            }
                        }else {
                        ?>
                        <div class="not-found-product">
                        <img src="<?php echo base_url();?>assets/backend/img/notfound.png">

                           <p>Products not found!</p>
                           </div>
                        <?php 
                        } 
                        ?>

                    </div>
                    <hr>
                    <!-- /Products grid -->

                    <!-- Pagination -->
                    <!--<div class="pagination-wrapper">
                        <ul class="pagination">
                            <li class="disabled"><a href="#"><i class="fa fa-angle-double-left"></i> Previous</a></li>
                            <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">Next <i class="fa fa-angle-double-right"></i></a></li>
                        </ul>
                    </div>-->
                    <!-- /Pagination -->

                </div>
                <!-- /CONTENT -->

            </div>
        </div>
    </section>

</div>