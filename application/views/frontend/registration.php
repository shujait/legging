<section class="page-section page-header breadcrumbs">
    <img src="<?php echo base_url('assets/backend');?>/img/regbg.jpg">
    <div class="container">
        <h3>New Account</h3>
    </div>

</section>
<div class="container-fluid">


    <div class="reg-form">
        <div class="row">

            <div class="col-md-8">

                <p>For all our exiting customers, please contact info@leggingspro.com to retrieve your pre-setup userID and password.</p>
            </div>
            <form action="<?php echo base_url();?>account/action" method="post" onsubmit="return false;" class="form_data subscription_plan" enctype="multipart/form-data" data-parsley-validate novalidate>
                <input type="hidden" name="form_type" value="save">
                <input type="hidden" name="RoleID" value="4">
                <input type="hidden" name="CustomerID" value="0" id="CustomerID">
                <input type="hidden" name="ChargeID" value="0" id="ChargeID">
                <div class="col-md-8">
                    <h3>General Information <span class="h5 text-red">(Your Email will be your login id)</span></h3>
                    <!-- <p>(Your Email will be your login id)</p> -->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="contact">First Name <span>*</span></label>
                                <input type="text" name="FirstName" class="form-control" id="FirstName" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="contact">Last Name <span>*</span></label>
                                <input type="text" name="LastName" class="form-control" id="LastName" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="contact">Surname<span>*</span></label>
                                <input type="text" name="surname" class="form-control" id="surname" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="contact">Contact <span>*</span></label>
                                <input type="text" name="Contact" class="form-control" id="Contact" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="company">Company <span>*</span></label>
                                <input type="text" name="Company" class="form-control" id="Company" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="CompanyWeb">Company Web Site <span>*</span></label>
                                <input type="text" name="CompanyWebsite" class="form-control" id="CompanyWebsite" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="Tel">Tel <span>*</span></label>
                                <input type="tel" name="Phone" class="form-control" id="Phone" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="Fax">Fax <span>*</span></label>
                                <input type="text" name="Fax" class="form-control" id="Fax" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="BusinessType">Business type</label>
                                <select name="BusinessType" id="BusinessType" class="form-control">
                                    <option selected="selected" value="1">Retail Store</option>
                                    <option value="2">Department/Chain Store</option>
                                    <option value="3">Wholesaler</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="PreferredSale">Preferred Sales</label>
                                <select name="PreferredSale" id="PreferredSale" class="form-control">
                                    <option value="">N/A</option>
                                    <option value="BLEE">BERNADETTE LEE(CHINESE)</option>
                                    <option value="ED">STAR DE LOZA(SPANISH/ENGLISH)</option>
                                    <option value="WEB">ONLINE WEB</option>
                                    <option value="YELE">YELETE</option>
                                    <option value="SL">SEAN LIN</option>
                                    <option value="ANDY">ANDY KIM(KOREAN, ENGLISH)</option>
                                    <option value="CECI">CECILIA (SPANISH/ENGLISH)</option>
                                    <option value="LEA">LEA BYUN(KOREAN, SPANISH)</option>
                                    <option value="JUL">JULIA ZHU(ENGLISH/MANDARIN)</option>
                                    <option value="RG">REYNA GARCIA</option>
                                    <option value="NY">NY OFFICE</option>
                                    <option value="ELI">ELIANA ANDRADE</option>
                                    <option value="LP">LORENA PINEDO(SPANISH/ENGLISH)</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="Email">Email <span>*</span></label>
                                <input type="email" name="Email" class="form-control" id="Email" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="EmailConfirmation">Email Confirmation <span>*</span></label>
                                <input type="email" name="EmailConfirmation" class="form-control" id="EmailConfirmation" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="Password">Password <span>*</span> (Min length 8 characters)</label>
                                <input type="password" name="Password" class="form-control" id="Password" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="PasswordConfirmation">Password Confirmation <span>*</span></label>
                                <input type="password" name="PasswordConfirmation" class="form-control" id="PasswordConfirmation" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="Profile">Profile Image</label>
                                <input type="file" name="Image[]" class="form-control" id="Profile" >
                            </div>
                        </div>

                    </div>
                    <hr>
                    <h3>Sales Tax Information</h3>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="ResalePeritNumber">Resale Permit Number <span>*</span></label>
                                <input type="text" name="ResalePeritNumber" class="form-control" id="ResalePeritNumber" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <p>Please fax in (323)201-3771 or email your copy of Resale Permit to webadmin@leggingspro.com to finish the new account setup.</p>
                        </div>
                    </div>
                    <hr>
                    <h3>Billing Address</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="BillingAddress1">Address Line 1 <span>*</span></label>
                                <input type="text" name="BillingAddress1" class="form-control" id="BillingAddress1" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="BillingAddress2">Address Line 2 <span>*</span></label>
                                <input type="text" name="BillingAddress2" class="form-control" id="BillingAddress2" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="BillingCountry">Country <span>*</span></label>
                                <select name="BillingCountry" id="BillingCountry" data-state-type="BillingState" class="form-control CountryID">
                                    <option value=""><?php echo lang("choose_country");?></option>
                                    <?php 
                                    foreach($countries as $Country)
                                    {
                                    ?>
                                        <option value="<?php echo $Country->CountryID; ?>"><?php echo $Country->Title; ?></option>
                                    <?php 
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="BillingState">State</label>
                                <select name="BillingState" id="BillingState" data-city-type="BillingCity" class="form-control StateID">
                                    <option value=""><?php echo lang("choose_state");?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="BillingCity">City</label>
                                <select name="BillingCity" id="BillingCity" class="form-control">
                                    <option value=""><?php echo lang("choose_city");?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="BillingZipCode">Zip <span>*</span></label>
                                <input type="text" name="BillingZipCode" class="form-control" id="BillingZipCode" required>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h3>Shipping Address</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="ShippingAddress1">Address Line 1</label>
                                <input type="text" name="ShippingAddress1" class="form-control" id="ShippingAddress1">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="ShippingAddress2">Address Line 2 </label>
                                <input type="text" name="ShippingAddress2" class="form-control" id="ShippingAddress2">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="ShippingCountry">Country <span>*</span></label>
                                <select name="ShippingCountry" id="ShippingCountry" data-state-type="ShippingState" class="form-control CountryID">
                                    <option value=""><?php echo lang("choose_country");?></option>
                                    <?php 
                                    foreach($countries as $Country)
                                    {
                                    ?>
                                        <option value="<?php echo $Country->CountryID; ?>"><?php echo $Country->Title; ?></option>
                                    <?php 
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="ShippingState">State</label>
                                <select name="ShippingState" id="ShippingState" data-city-type="ShippingCity" class="form-control StateID">
                                    <option value=""><?php echo lang("choose_state");?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="ShippingCity">City</label>
                                <select name="ShippingCity" id="ShippingCity" class="form-control">
                                    <option value=""><?php echo lang("choose_city");?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="ShippingZipCode">Zip <span>*</span></label>
                                <input type="text" name="ShippingZipCode" class="form-control" id="ShippingZipCode" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p>* You agree to receive newsletters by creating an account with us. If you do not wish to receive newsletters, please e-mail webadmin@leggingspro.com.</p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <input type="button" name="CreateMyAcoount" class="btn btn-success" value="Create My Account" id="stripePaymentBtn">
                        </div>
                    </div>
                    <hr>
                </div>
            </form>
            <aside class="col-md-4 " >

                <div class="widget">
                    <div class="bannernew1">
                        <img src="<?php echo base_url('assets/backend');?>/img/bannernew1.jpg">

                    </div>

                </div>

                <div class="widget">
                    <div class="bannernew2">
                        <img src="<?php echo base_url('assets/backend');?>/img/bannernew2.jpg">

                    </div>

                </div>

            </aside>
            <!-- CONTENT -->
        </div>
    </div>
    <!-- SIDEBAR -->


</div>
<script src="https://checkout.stripe.com/checkout.js"></script>

<script type="text/javascript">
    
/*************** START: Stripe payment widget ***************/
    var handler = StripeCheckout.configure({
      key: '<?php echo STRIPE_PUBLISHABLE_KEY; ?>',
      image: '<?php echo base_url();?>assets/backend/img/logo.jpg',
      locale: 'auto',
      allowRememberMe: false,
      token: function(token) {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $.ajax({
          url: base_url + 'account/stripe_single_payment_for_subscription',
          type: 'POST',
          data: {
            firstname: $('form.payment input[name="first_name"]').val(),
            lastname: $('form.payment input[name="last_name"]').val(),
            token: JSON.stringify(token)
          },
          dataType: 'json',
          success: function(res) {
            
            
            $('#CustomerID').val(res.CustomerID);
            $('#ChargeID').val(res.ChargeID);
            showSuccess(res.message);
            setTimeout(function () {
              $(".subscription_plan").submit();
            }, 2000);
            //location.reload();
          }
        });
      }
    });

    document.getElementById('stripePaymentBtn').addEventListener('click', function(e) {
        if($('#CustomerID').val() != 0){
            $(".subscription_plan").submit();
            return true;
        }
        handler.open({
        name: "LeggingPro",
        description: "Product Payment",
        panelLabel: "Pay",
        zipCode: false,
        /*allowRememberMe: false,*/
        amount: '<?php echo $PlanAmount;?>',
        currency: '<?php echo getCurrencyCode($CurrencyID); ?>'
      });

      e.preventDefault();
    
    });

    // Close Checkout on page navigation:
    window.addEventListener('popstate', function() {
      handler.close();
    });
    /**************** END: Stripe payment widget ****************/


</script>