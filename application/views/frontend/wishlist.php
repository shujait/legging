<section class="page-section page-header breadcrumbs">
   <img src="<?php echo base_url('assets/backend');?>/img/regbg.jpg">
   <div class="container">
      <h3>Wishlist</h3>
   </div>
</section>
<div class="container-fluid">
   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-info cus-panel">
            <div class="panel-heading">
               <div class="panel-title">
                  <div class="row">
                     <div class="col-md-12 col-sm-12">
                        <h5><span class="glyphicon glyphicon-shopping-cart"></span> WishList</h5>
                     </div>
                  </div>
               </div>
            </div>
            <div class="panel-body">
               <hr>

               <?php if($products){
                  foreach ($products as $key => $value) { 

                    $RemainingProducts = getRemainingProductQuantity($value['ProductID']);
                    $Price = (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? $value['PriceForWholeSaler'] : $value['Price']);
                    $DiscountPer = (isset($this->session->userdata['admin']) && $this->session->userdata['admin']['RoleID'] == 4 ? $value['WholeSaleDiscount'] : $value['Discount']);

                    if($DiscountPer > 0)
                    {
                        $Discount = $Price * ($DiscountPer/100);

                        $Price = $Price - $Discount;
                    }
                    ?>
                     
                  
               <div class="row wish-list" id="<?php echo $value['ProductID'];?>">
                  <div class="col-md-7 col-sm-12">
                       <div class="col-md-1 col-sm-12">
                        <button type="button" class="btn  btn-danger btn-xs pull-right delete-pro" data-id="<?php echo $value['ProductID'];?>">
                        <span class="glyphicon glyphicon-trash"> </span>
                        </button>
                     </div>
                     <div class="col-md-2"><img class="img-responsive" src="<?php echo base_url($value['ImageName']); ?>">
                     </div>
                     <div class="col-md-4">
                        <h4 class="product-name"><strong><?php echo $value['Title']; ?></strong></h4>
                        <h4><small><?php echo $value['ShortDescription']; ?></small></h4>
                     </div>
                  </div>
                  <div class="col-md-4 col-sm-12">

                     
                     <div class="col-md-3  col-sm-12">
                        <h6><strong class="pull-right cart-price">$ <?php echo number_format($Price, 2);?></strong></h6>
                     </div>
                   

                     <div class="col-md-2 col-sm-12 pull-right">
                        <a <?php echo ($RemainingProducts <= 0 ? 'disabled="disabled"' : '');?> href="<?php echo ($RemainingProducts <= 0 ? 'javascript:void(0);' : base_url('products/detail/'.$value['ProductID']));?>"><button type="button" class=" btn  btn-primary btn-md ">
                           More Detail
                        </button></a>
                     </div>
                  </div>
               </div>

            <?php }  }else{ ?>

               
                        <div class="not-found-product">

                           <p>Wishlist Empty!</p>
                           </div> 


             <?php  
            } ?>

            </div>
         </div>
      </div>
   </div>
</div>


<script>
    $( document ).ready(function() {

      $(".delete-pro").on('click',function(){
            if (confirm("Are you sure you want to delete ?")) {
               var id = $(this).attr("data-id");
           $.blockUI({
               css: {
                   border: 'none',
                   padding: '15px',
                   backgroundColor: '#000',
                   '-webkit-border-radius': '10px',
                   '-moz-border-radius': '10px',
                   opacity: .5,
                   color: '#fff'
               }
           });

           $.ajax({
               type: "POST",
               url: base_url + 'products/removeItemFromWishlist',
               data: {
                   'id': $(this).attr("data-id")
                   
               },
               dataType: "json",
               cache: false,
               //async:false,
               success: function (result) {

                   if (result.error != false) {
                       showError(result.error);
                   } else {
                       showSuccess(result.success);
                       $('#' + id).remove();
                   }


                  

               },
               complete: function () {
                   $.unblockUI();
               }
           });
           return true;
       }
      })
   
 });

</script>