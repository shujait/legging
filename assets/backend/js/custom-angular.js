 var getUrl = window.location;
if(getUrl.host == "localhost"){
 var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1]+'/';
}else{
 var baseUrl = getUrl .protocol + "//" + getUrl.host + "/";
}
var app = angular.module('App', []);
app.controller('CustomController', function ($scope, $log,$http,$filter) {
	 $scope.title;
	 $scope.date = $filter('date')(new Date(), 'dd-MM-yyyy');
	 $scope.items = [];
	 $scope.country  = 0;
     $scope.getState = function () {
       
        var url = baseUrl + 'cms/index/getState/';
        $http({
            method: 'POST',
            url: url,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: "country=" + $scope.country
        }).then(function successCallback(response) {
            $scope.state = response.data.States;
			
    }, function errorCallback(response) {
       // called asynchronously if an error occurs
       // or server returns response with an error status.

			});
    };
	 $scope.getRate = function () {
      
        var url = baseUrl + 'get-rate/';
        $http({
            method: 'POST',
            url: url,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: "sub_id=" + $scope.sub_id
        }).then(function successCallback(response) {
            $scope.rate = response.data.rate;
			$scope.variation = response.data.var;
			
    }, function errorCallback(response) {
       // called asynchronously if an error occurs
       // or server returns response with an error status.

			});
    };
	 $scope.RawMaterial = function (){
		 
		 var url = baseUrl + 'report/raw-material-report';
        $http({
            method: 'POST',
            url: url,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: "cat_id=" + $scope.cat_id+"&sub_id=" + $scope.sub_id
        }).then(function successCallback(response) {
           console.log(response.data);
			
    }, function errorCallback(response) {
       // called asynchronously if an error occurs
       // or server returns response with an error status.

			});
		 
	 };
	
	
	
	 $scope.add = function() {
        $scope.items.push($scope.items.length+1);
    };
	 $scope.del = function(i){
    $scope.items.splice(i,1);
  	};
	 $scope.del_row = function(i){
	angular.element( document.querySelector( "#row-"+i ) ).remove();
  	};
});
