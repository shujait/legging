$(document).ready(function () {
    if(module_section == 'cms')
    {
        tinymce.init({
            forced_root_block : "",
            selector: 'textarea',
            plugins: "code,textcolor",
            theme : "modern",
            mode: "exact",
            toolbar: "forecolor,backcolor, undo,redo,cleanup,bold,italic,underline,strikethrough,separator,"
            + "justifyleft,justifycenter,justifyright,justifyfull,formatselect,"
            + "bullist,numlist,outdent,indent",
            theme_advanced_toolbar_location : "top",
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,"
            + "justifyleft,justifycenter,justifyright,justifyfull,formatselect,"
            + "bullist,numlist,outdent,indent",
            theme_advanced_buttons2 : "link,unlink,anchor,image,separator,"
            +"undo,redo,cleanup,code,separator,sub,sup,charmap",
            theme_advanced_buttons3 : "",
            height:"250px",
            setup: function (editor) {
                editor.on('change', function () {
                    tinymce.triggerSave();
                });
            }
        });
    }
$(document).on('click', '.remove_image', function () {
        var image_id       = $(this).attr('data-image-id');
        var image_path       = $(this).attr('data-image-path');
        $(".delete_url").attr('data-modal-image-id', image_id);
        $(".delete_url").attr('data-modal-image-path', image_path);
        $('#DeleteArticleImage').modal('show');
    }); 
 /*$("#filer_input1").filer({
      limit: null,
        maxSize: null,
        extensions: null,
        changeInput: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><a class="jFiler-input-choose-btn blue">Browse Files</a></div></div>',
        showThumbs: true
     
 });*/    
$(window).bind("load", function() {
        $(".menu-list li > .menu-remove").show();    
      var  page = window.location.pathname.split('/')[4];
        if(page != 'add'){
            updateOutput();
        }
        });

    $(document).on("click",".draggable",function(){
        /*if(jQuery(".collapse.in.menu-scroll").data("menutype") !=  jQuery(".menu_type").val()){
            jQuery(".menu-list").html('');
        }*/
        $(".menu-list").append($(this)[0].outerHTML);
        $(".menu-list li").removeClass("draggable").addClass('dd-item');
        $(".menu-list li > div").addClass('dd-handle');
        $(".menu-list li > .menu-remove").show();
        updateOutput();
    });
    
    $(document).on("click",".menu-remove",function(){
        $($(this).parent()).remove();
         updateOutput();
    });
    var updateOutput = function(e)

    {
         var data = [];

        jQuery('.dd-item').each(function(){

            var id      = jQuery(this).attr('data-id');

            var menu_type = jQuery(this).attr('data-menu-type');

            var parent  = jQuery(this).parent().parent().attr('data-id');

            if(typeof parent == 'undefined')

                parent = 0;

            var menu = {'id':id,'parent':parent,'menutype':menu_type};

            data.push(menu);

        }); 

        jQuery('.top_menu').val(JSON.stringify(data));
        //jQuery(".menu_type").val(jQuery(".collapse.in.menu-scroll").data("menutype"));

    };

    

    // activate Nestable for list 1

    jQuery('#nestable').nestable({

        group: 1

    }).on('change', updateOutput);

    $(".form_data").submit(function (e) {
        e.preventDefault();

        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $form = $(this);
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: new FormData(this),
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            //async:false,
            success: function (result) {
                if (result.error != false) {
                    showError(result.error);
                } else {
                    showSuccess(result.success);
                }
                if (result.reset) {
                    $form[0].reset();
                }
                if (result.reload) {
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                }
                if (result.redirect) {
                    setTimeout(function () {
                        window.location.href = base_url + result.url;
                    }, 1000);
                }
            },
            complete: function () {
                $.unblockUI();
            }
        });
    });


    if ($('.jFiler-input-text').length > 0) {

        $('.jFiler-input-text').hide();
    }


});

$(document).ready(function () {
    //$('.dataTables_length').children('label').children('select').addClass('selectpicker').data('style','select-with-transition');
});
// .dataTables_length

function showSuccess(message_text) {
    type = ['','info','success','warning','danger','rose','primary'];
    $.notify({
        icon: "notifications",
        message: '<span style="font-size: 17px;">'+message_text+'</span>'

    },{
        type: type[2], // 2nd index from type array above
        timer: 3000,
        placement: {
            from: 'bottom',
            align: 'right'
        }
    });
}

function showError(message_text) {
    type = ['','info','success','warning','danger','rose','primary'];
    $.notify({
        icon: "notifications",
        message: '<span style="font-size: 17px;">'+message_text+'</span>'

    },{
        type: type[4],// 4th index from type array above
        timer: 3000,
        placement: {
            from: 'bottom',
            align: 'right'
        }
    });
}

function deleteRecord(id, actionUrl, reloadUrl) {

    //id can contain comma separated ids too.

    if (confirm(delete_msg)) {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $.ajax({
            type: "POST",
            url: base_url + '' + actionUrl,
            data: {
                'id': id,
                'form_type': 'delete'
            },
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {

                if (result.error != false) {
                    showError(result.error);
                } else {
                    showSuccess(result.success);
                    $('#' + id).remove();
                }


                if (reloadUrl != "") setTimeout(function () {
                    document.location.href = reloadUrl;
                }, 1000);

            },
            complete: function () {
                $.unblockUI();
            }
        });
        return true;
    } else {
        return false;
    }

}


function deleteImage(id, actionUrl) {

    //id can contain comma separated ids too.

    if (confirm("Are you sure you want to delete?")) {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $.ajax({
            type: "POST",
            url: base_url + '' + actionUrl,
            data: {
                'id': id,
                'form_type': 'delete_image'
            },
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {


                if (result.error != 'false') {
                    alert('There is something went wrong');

                } else {
                    $('#img-' + id).remove();
                    alert('Deleted Successfully');
                }


            },
            complete: function () {
                $.unblockUI();
            }
        });
        return true;
    } else {
        return false;
    }

}


function redirect(redirect_to) {
    redirect_to = base_url + redirect_to;
    window.location.href = redirect_to;
}